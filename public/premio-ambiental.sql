--- PRODUCAO ---
--- criar nessa ordem ---

CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `admin` tinyint(1) NOT NULL DEFAULT '1',
  `ultimo_acesso` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_login_unique` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `modalidade` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_modalidade_login_cadastro` (`login_cadastro`),
  KEY `fk_modalidade_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_modalidade_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_modalidade_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `porte` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `descricao` text NOT NULL,
  `minimo_empregado` int(4) unsigned DEFAULT NULL,
  `maximo_empregado` int(4) unsigned DEFAULT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_porte_login_cadastro` (`login_cadastro`),
  KEY `fk_porte_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_porte_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_porte_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `evento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `data_inicio` datetime NOT NULL,
  `data_fim` datetime NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fim` time NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `token` text,
  PRIMARY KEY (`id`),
  KEY `fk_evento_login_cadastro` (`login_cadastro`),
  KEY `fk_evento_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_evento_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_evento_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `evento_modalidade` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `evento_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_evento_modalidade_login_cadastro` (`login_cadastro`),
  KEY `fk_evento_modalidade_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_evento_modalidade_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_evento_modalidade_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `avaliador` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `evento_id` int(11) unsigned NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `nome_fantasia` varchar(255) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `cep_empresa` varchar(9) NOT NULL,
  `endereco_empresa` varchar(255) NOT NULL,
  `estado_empresa` varchar(255) NOT NULL,
  `municipio_empresa` varchar(255) NOT NULL,
  `bairro_empresa` varchar(255) NOT NULL,
  `complemento_empresa` varchar(255) NULL,
  `login` varchar(255) NULL,
  `senha` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `login_alteracao_avaliador` int(11) unsigned NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_avaliador_evento_id` (`evento_id`),
  KEY `fk_avaliador_login_cadastro` (`login_cadastro`),
  KEY `fk_avaliador_login_alteracao` (`login_alteracao`),
  KEY `fk_avaliador_login_alteracao_avaliador` (`login_alteracao_avaliador`),
  CONSTRAINT `fk_avaliador_evento_id` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
  CONSTRAINT `fk_avaliador_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_avaliador_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_avaliador_login_alteracao_avaliador` FOREIGN KEY (`login_alteracao_avaliador`) REFERENCES `avaliador` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `inscricao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_alternativo` varchar(255) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `celular` varchar(20) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `nome_fantasia` varchar(255) NOT NULL,
  `razao_social` varchar(255) NOT NULL,
  `representante` varchar(255) NOT NULL,
  `representante_cargo` varchar(255) NOT NULL,
  `cep_empresa` varchar(9) NOT NULL,
  `endereco_empresa` varchar(255) NOT NULL,
  `estado_empresa` varchar(255) NOT NULL,
  `municipio_empresa` varchar(255) NOT NULL,
  `bairro_empresa` varchar(255) NOT NULL,
  `complemento_empresa` varchar(255) NULL,
  `porte_id` int(11) unsigned NOT NULL,
  `evento_id` int(11) unsigned NOT NULL,
  `login` varchar(255) NULL,
  `senha` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `login_alteracao` int(11) unsigned NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inscricao_unique_evento_login` (`evento_id`,`login`),
  KEY `fk_inscricao_evento_id` (`evento_id`),
  KEY `fk_inscricao_porte_id` (`porte_id`),
  KEY `fk_inscricao_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_inscricao_evento_id` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
  CONSTRAINT `fk_inscricao_porte_id` FOREIGN KEY (`porte_id`) REFERENCES `porte` (`id`),
  CONSTRAINT `fk_inscricao_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `inscricao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `inscricao_modalidade` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `inscricao_id` int(11) unsigned NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inscricao_modalidade_inscricao_id` (`inscricao_id`),
  KEY `fk_inscricao_modalidade_modalidade_id` (`modalidade_id`),
  CONSTRAINT `fk_inscricao_modalidade_inscricao_id` FOREIGN KEY (`inscricao_id`) REFERENCES `inscricao` (`id`),
  CONSTRAINT `fk_inscricao_modalidade_modalidade_id` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `projeto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `inscricao_id` int(11) unsigned NOT NULL,
  `nome` varchar(255) NOT NULL,
  `modalidade_id` int(11) unsigned NOT NULL,
  `responsavel_nome` varchar(255) NOT NULL,
  `responsavel_telefone` varchar(255) NOT NULL,
  `responsavel_email` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `evento_id` int(11) unsigned NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projeto_inscricao_id` (`inscricao_id`),
  KEY `fk_projeto_evento_id` (`evento_id`),
  KEY `fk_projeto_modalidade_id` (`modalidade_id`),
  KEY `fk_projeto_login_cadastro` (`login_cadastro`),
  CONSTRAINT `fk_projeto_inscricao_id` FOREIGN KEY (`inscricao_id`) REFERENCES `inscricao` (`id`),
  CONSTRAINT `fk_projeto_evento_id` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
  CONSTRAINT `fk_projeto_modalidade_id` FOREIGN KEY (`modalidade_id`) REFERENCES `modalidade` (`id`),
  CONSTRAINT `fk_projeto_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `inscricao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `avaliador_projeto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) unsigned NOT NULL,
  `avaliador_id` int(11) unsigned NOT NULL,
  `nota` decimal(4,2) unsigned NOT NULL,
  `comentario` varchar(255) NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_avaliador_projeto_projeto_id` (`projeto_id`),
  KEY `fk_avaliador_projeto_avaliador_id` (`avaliador_id`),
  CONSTRAINT `fk_avaliador_projeto_projeto_id` FOREIGN KEY (`projeto_id`) REFERENCES `projeto` (`id`),
  CONSTRAINT `fk_avaliador_projeto_avaliador_id` FOREIGN KEY (`avaliador_id`) REFERENCES `avaliador` (`id`),
  CONSTRAINT `fk_avaliador_projeto_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `avaliador` (`id`),
  CONSTRAINT `fk_avaliador_projeto_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8



CREATE TABLE `documento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `descricao` text NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documento_login_cadastro` (`login_cadastro`),
  KEY `fk_documento_login_alteracao` (`login_alteracao`),
  CONSTRAINT `fk_documento_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`),
  CONSTRAINT `fk_documento_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `inscricao_documento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `inscricao_id` int(11) unsigned NOT NULL,
  `evento_id` int(11) unsigned NOT NULL,
  `documento_id` int(11) unsigned NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inscricao_documento_inscricao_id` (`inscricao_id`),
  KEY `fk_inscricao_documento_evento_id` (`evento_id`),
  KEY `fk_inscricao_documento_documento_id` (`documento_id`),
  KEY `fk_inscricao_documento_login_cadastro` (`login_cadastro`),
  CONSTRAINT `fk_inscricao_documento_inscricao_id` FOREIGN KEY (`inscricao_id`) REFERENCES `inscricao` (`id`),
  CONSTRAINT `fk_inscricao_documento_evento_id` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
  CONSTRAINT `fk_inscricao_documento_documento_id` FOREIGN KEY (`documento_id`) REFERENCES `documento` (`id`),
  CONSTRAINT `fk_inscricao_documento_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `inscricao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `inscricao_documento` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `inscricao_id` int(11) unsigned NOT NULL,
  `evento_id` int(11) unsigned NOT NULL,
  `documento_id` int(11) unsigned NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inscricao_documento_inscricao_id` (`inscricao_id`),
  KEY `fk_inscricao_documento_evento_id` (`evento_id`),
  KEY `fk_inscricao_documento_documento_id` (`documento_id`),
  KEY `fk_inscricao_documento_login_cadastro` (`login_cadastro`),
  CONSTRAINT `fk_inscricao_documento_documento_id` FOREIGN KEY (`documento_id`) REFERENCES `documento` (`id`),
  CONSTRAINT `fk_inscricao_documento_evento_id` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
  CONSTRAINT `fk_inscricao_documento_inscricao_id` FOREIGN KEY (`inscricao_id`) REFERENCES `inscricao` (`id`),
  CONSTRAINT `fk_inscricao_documento_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `inscricao` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8






/*
CREATE TABLE `avaliador_projeto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) unsigned NOT NULL,
  `avaliador_id` int(11) unsigned NOT NULL,
  `nota` decimal(4,2) unsigned NOT NULL,
  `login_cadastro` int(11) unsigned NOT NULL,
  `login_alteracao` int(11) unsigned NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_avaliador_projeto_projeto_id` (`projeto_id`),
  KEY `fk_avaliador_projeto_avaliador_id` (`avaliador_id`),
  CONSTRAINT `fk_avaliador_projeto_projeto_id` FOREIGN KEY (`projeto_id`) REFERENCES `projeto` (`id`),
  CONSTRAINT `fk_avaliador_projeto_avaliador_id` FOREIGN KEY (`avaliador_id`) REFERENCES `avaliador` (`id`),
  CONSTRAINT `fk_avaliador_projeto_login_cadastro` FOREIGN KEY (`login_cadastro`) REFERENCES `avaliador` (`id`),
  CONSTRAINT `fk_avaliador_projeto_login_alteracao` FOREIGN KEY (`login_alteracao`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

select * from projeto*/