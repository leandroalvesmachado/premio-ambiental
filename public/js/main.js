$(document).ready(function() {

	$(".data").mask("99/99/9999");
    $(".fone").mask("(99) 9999-9999");
    $(".celular").mask("(99) 9999-9999?9", {
        placeholder: " "
    });
    $(".rg").mask("99999?9999999999");
    $(".cpf").mask("999999999-99");
    $(".cnpj").mask("99.999.999/9999-99");
    $(".cep").mask("99999-999");
    $(".decimal").mask("9999.99");
    $('.hora').mask("99:99");
    $(".numerico").mask('9?999999999');

    $('.dropify').dropify({
        messages: {
            'default': 'Arraste e solte um arquivo aqui ou clique',
            'replace': 'Arraste e solte ou clique para substituir',
            'remove':  'Remover',
            'error':   'Ooops, Aconteceu algo de errado.'
        },
        error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'O formato da imagem não é permitido ({{ value }} apenas).'
        }
    });
});