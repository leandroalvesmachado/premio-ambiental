<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Prêmio FIEC por Desempenho Ambiental</title>
	<base href="{{ url('') }}/">
    <link rel="apple-touch-icon" href="img/icon.ico">
    <link rel="shortcut icon" type="image/x-icon" href="img/icon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/pace.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/app.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/colors.min.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="stack/assets/css/style.css">
    <!-- END Custom CSS-->
</head>

<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column   menu-expanded blank-page blank-page">
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                            <div class="card-header no-border pb-0">
                                <div class="card-title text-xs-center">
                                    <img src="img/login-logo.png" class="img-responsive" width="80%">
                                </div>
                                <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                                	<span>Recuperar Senha</span>
                                </h6>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    @if(Session::get('message'))
                                    <div class="alert alert-{{ Session::get('message')['color'] }} alert-dismissible fade in mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <strong>{{ Session::get('message')['msg'] }}</strong> 
                                    </div>
                                    @endif
                                    {!! Form::open(['route'=>'recover.store','class'=>'form-horizontal']) !!}
                                        <fieldset class="form-group position-relative has-icon-left {{ $errors->has('login') ? 'has-danger' : '' }}">
                                            {!! Form::text('login',null,['id'=>'login','class'=>'form-control form-control-lg input-lg','placeholder'=>'Login']) !!}
                                            <div class="form-control-position">
                                                <i class="ft-user"></i>
                                            </div>
                                            <p class="text-danger">{{ $errors->first('login') }}</p>
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left {{ $errors->has('email') ? 'has-danger' : '' }}">
                                            {!! Form::text('email',null,['id'=>'email','class'=>'form-control form-control-lg input-lg','placeholder'=>'E-mail']) !!}
                                            <div class="form-control-position">
                                                <i class="ft-mail"></i>
                                            </div>
                                            <p class="text-danger">{{ $errors->first('email') }}</p>
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left {{ $errors->has('perfil') ? 'has-danger' : '' }}">
                                            {!! Form::select('perfil',$perfil, null,['id'=>'perfil','class'=>'form-control form-control-lg input-lg']) !!}
                                            <!-- <div class="form-control-position">
                                                <i class="fa fa-user-circle"></i>
                                            </div> -->
                                            <p class="text-danger">{{ $errors->first('perfil') }}</p>
                                        </fieldset>
                                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Recuperar</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="card-footer no-border">
                                <p class="float-sm-left text-xs-center"><a href="{{ route('login.create') }}" class="card-link">Voltar para o Login</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    
    <!-- BEGIN VENDOR JS-->
    <script src="stack/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="stack/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="stack/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="stack/app-assets/js/scripts/forms/form-login-register.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
</body>
</html>