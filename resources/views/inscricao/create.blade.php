<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>Prêmio FIEC por Desempenho Ambiental</title>
<base href="{{ url('') }}/">
<link rel="apple-touch-icon" href="img/icon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/icon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/feather/style.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap-extended.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/app.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/colors.min.css">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="stack/assets/css/style.css">
<!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
    
    
    <!-- content -->
    <div class="container">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-body text-md-center">
                                    <img class="card-img-top img-fluid" src="img/login-logo.png" alt="Card image cap">
                                    <div class="card-block">
                                        <h4 class="card-title">{{ $evento->nome }}</h4>
                                        <p class="card-text">{{ $evento->descricao }}</p>
                                        <p class="card-text">{{ $evento->data_inicio }} à {{ $evento->data_fim }}</p>
                                        <p class="card-text"><small class="text-muted"></small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body collapse in">
                                    <div class="card-block">
                                        @if ( strtotime(date('Y-m-d H:i:s')) >= strtotime($evento->getOriginal('data_inicio')) &&  strtotime(date('Y-m-d H:i:s')) <=  strtotime($evento->getOriginal('data_fim')) )
                                        
                                        {!! Form::open(['route'=>['inscricao.store',$evento_token],'class'=>'form']) !!}
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-user"></i>Participante</h4>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                                            <label>* CPF</label>
                                                            {!! Form::text('cpf',null,['id'=>'cpf','class'=>'form-control cpf']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('cpf') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                                                            <label>* Nome</label>
                                                            {!! Form::text('nome',null,['id'=>'nome','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('endereco') ? ' has-danger' : '' }}">
                                                            <label>* Endereço</label>
                                                            {!! Form::text('endereco',null,['id'=>'endereco','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('endereco') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                                            <label>* E-mail</label>
                                                            {!! Form::text('email',null,['id'=>'email','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('email_alternativo') ? ' has-danger' : '' }}">
                                                            <label>E-mail Alternativo</label>
                                                            {!! Form::text('email_alternativo',null,['id'=>'email_alternativo','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('email_alternativo') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('telefone') ? ' has-danger' : '' }}">
                                                            <label>* Telefone</label>
                                                            {!! Form::text('telefone',null,['id'=>'telefone','class'=>'form-control fone']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('telefone') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('celular') ? ' has-danger' : '' }}">
                                                            <label>* Celular</label>
                                                            {!! Form::text('celular',null,['id'=>'celular','class'=>'form-control celular']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('celular') }}</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="ft-briefcase"></i>Empresa</h4>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('cnpj') ? ' has-danger' : '' }}">
                                                            <label>* CNPJ</label>
                                                            {!! Form::text('cnpj',null,['id'=>'cnpj','class'=>'form-control cnpj']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('cnpj') }}</span>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('representante') ? ' has-danger' : '' }}">
                                                            <label>* Representante</label>
                                                            {!! Form::text('representante',null,['id'=>'representante','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('representante') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('representante_cargo') ? ' has-danger' : '' }}">
                                                            <label>* Cargo do Representante</label>
                                                            {!! Form::text('representante_cargo',null,['id'=>'representante_cargo','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('representante_cargo') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('nome_fantasia') ? ' has-danger' : '' }}">
                                                            <label>* Nome Fantasia</label>
                                                            {!! Form::text('nome_fantasia',null,['id'=>'nome_fantasia','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('nome_fantasia') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('razao_social') ? ' has-danger' : '' }}">
                                                            <label>* Razão Social</label>
                                                            {!! Form::text('razao_social',null,['id'=>'razao_social','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('razao_social') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('cep_empresa') ? ' has-danger' : '' }}">
                                                            <label>* CEP</label>
                                                            {!! Form::text('cep_empresa',null,['id'=>'cep_empresa','class'=>'form-control cep']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('cep_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('endereco_empresa') ? ' has-danger' : '' }}">
                                                            <label>* Endereço</label>
                                                            {!! Form::text('endereco_empresa',null,['id'=>'endereco_empresa','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('endereco_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('estado_empresa') ? ' has-danger' : '' }}">
                                                            <label>* Estado</label>
                                                            {!! Form::text('estado_empresa',null,['id'=>'estado_empresa','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('estado_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('municipio_empresa') ? ' has-danger' : '' }}">
                                                            <label>* Município</label>
                                                            {!! Form::text('municipio_empresa',null,['id'=>'municipio_empresa','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('municipio_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group {{ $errors->has('bairro_empresa') ? ' has-danger' : '' }}">
                                                            <label>* Bairro</label>
                                                            {!! Form::text('bairro_empresa',null,['id'=>'bairro_empresa','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('bairro_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('complemento_empresa') ? ' has-danger' : '' }}">
                                                            <label>Complemento</label>
                                                            {!! Form::text('complemento_empresa',null,['id'=>'complemento_empresa','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('complemento_empresa') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('porte_id') ? ' has-danger' : '' }}">
                                                            <label>* Porte</label>
                                                            {!! Form::select('porte_id',$portes, null,['id'=>'porte_id','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('porte_id') }}</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="ft-layers"></i>Modalidades</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group {{ $errors->has('modalidade_id') ? ' has-danger' : '' }}">
                                                            <label>* Modalidade</label>
                                                            <br>
                                                            <input type="hidden" name="modalidade_id[]" value="0" checked>
                                                            @forelse ($modalidades as $modalidade)
                                                                {!! Form::checkbox('modalidade_id[]',$modalidade->id) !!} {{ $modalidade->nome }}
                                                                <br>
                                                            @empty
                                                                Nenhuma modalidade cadastrada
                                                            @endforelse
                                                            <span class="help-block text-danger">{{ $errors->first('modalidade_id') }}</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h4 class="form-section"><i class="ft-lock"></i>Acesso</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('senha') ? ' has-danger' : '' }}">
                                                            <label>* Senha</label>
                                                            {!! Form::password('senha',['id'=>'senha','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('senha') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group {{ $errors->has('senha_confirmar') ? ' has-danger' : '' }}">
                                                            <label>* Confirmar Senha</label>
                                                            {!! Form::password('senha_confirmar',['id'=>'senha_confirmar','class'=>'form-control']) !!}
                                                            <span class="help-block text-danger">{{ $errors->first('senha_confirmar') }}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary block-page">
                                                    <i class="fa fa-check-square-o"></i> Salvar
                                                </button>
                                            </div>
                                        {!! Form::close() !!}
                                        @else
                                            Inscrições encerradas      
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- content end -->

    

    <!-- BEGIN VENDOR JS-->
    <script src="stack/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="stack/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="stack/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="stack/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="js/dropify/dist/js/dropify.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>

    @if(Session::get('message'))
    <script type="text/javascript">
        $(document).ready(function() {
            swal({
                title: "{{ Session::get('message')['title'] }}!",
                text: "{!! Session::get('message')['msg'] !!}",
                type: "{{ Session::get('message')['color'] }}"
            });
        });
    </script>
    @endif

    @yield('script')
</body>
</html>