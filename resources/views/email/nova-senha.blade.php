<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style type="text/css" media="screen">
        * {
            font-family: arial;
        }
        .geral {
            width: 960px;
            margin: 0 auto;
            border: 1px solid #d6d6d6;
            background: #f7f7f7;
        }
        .header img {
            margin: 0 auto;
            display: block
        }
        .header {
            box-shadow: 0px 3px 2px rgba(0, 0, 0, 0.18);
            background: #ffffff;
        }
        /*.barra-top {
            background: rgba(1, 107, 173, 1);
            background: -moz-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(1, 107, 173, 1)), color-stop(99%, rgba(0, 102, 164, 1)), color-stop(100%, rgba(0, 102, 164, 1)));
            background: -webkit-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -o-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -ms-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: linear-gradient(to right, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#016bad', endColorstr='#0066a4', GradientType=1);
            height: 29px;
            box-shadow: 2px 3px 4px rgba(0, 0, 0, 0.45);
            border-bottom: 1px solid #d6d6d6;
            border-top: 1px solid #d6d6d6;
            position: relative;
        }*/
        .barra-top {
        	background: rgba(1, 107, 173, 1);
        	background: -moz-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -webkit-gradient(left top, right top, color-stop(0%, rgba(1, 107, 173, 1)), color-stop(99%, rgba(0, 102, 164, 1)), color-stop(100%, rgba(0, 102, 164, 1)));
            background: -webkit-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -o-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: -ms-linear-gradient(left, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
            background: linear-gradient(to right, rgba(1, 107, 173, 1) 0%, rgba(0, 102, 164, 1) 99%, rgba(0, 102, 164, 1) 100%);
        	height: 29px;
        	box-shadow: 2px 3px 4px rgba(0, 0, 0, 0.45);
            border-bottom: 1px solid #d6d6d6;
            border-top: 1px solid #d6d6d6;
            position: relative;
        }
        
        .rodape {
            margin: 0 auto;
            display: block;
            text-align: center;
            background: #535353;
            padding: 1em;
            line-height: 20px;
            color: #ffffff;
            font-size: 14px;
        }
        .header {
            padding: 1em;
        }
        .conteudo {
            padding: 4em;
            line-height: 22px;
        }
        .evento {
        	text-align: center;
        }
    </style>
</head>

<body>
    <div class="geral">
        <div class="barra-top">

        </div>
        <div class="header">
            <img src="{{ $message->embed(public_path('img/login-logo.png')) }}">
        </div>
        <div class="conteudo">
        	<div class="evento">

        	</div>
            <h3>NOVA SENHA</h3>
            <p class="mensagem">
            	<b>{{ $input['perfil']}}</b>
            	<br>
            	Login: {{ $object['login'] }}
            	<br>
            	Nome: {{ $object['nome'] }}
            	<br>
            	Nova Senha: {{ $senha }}
            </p>
            <p class="mensagem">
                Link para acessar o sistema: <a href="http://premioambiental.sfiec.org.br">http://premioambiental.sfiec.org.br</a>
            </p>
            <p class="mensagem">
                <b>Esse é um e-mail automático. Por favor não responda esta mensagem.</b>
            </p>
        </div>
        <div class="bar"></div>
        <div class="rodape">
            <strong>NUMA - Núcleo do Meio Ambriente</strong>
            <br>
            (85) 3421.5916 / Av. Barão de Studart, 1980, Aldeota - Fortaleza-CE
        </div>
    </div>
</body>

</html>