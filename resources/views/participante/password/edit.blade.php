@extends('participante.template.layout')

@section('content')

<div class="row match-height">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body collapse in">
				<div class="card-block">
                    {!! Form::open(['route'=>'participante.password','class'=>'form']) !!}
						<div class="form-body">
							<h4 class="form-section">Alterar Senha</h4>
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
						        	<div class="form-group {{ $errors->has('senha') ? ' has-danger' : '' }}">
						            	<label>* Nova Senha</label>
						                {!! Form::password('senha',['id'=>'senha','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('senha') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-4"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
						        	<div class="form-group {{ $errors->has('senha_confirmar') ? ' has-danger' : '' }}">
						            	<label>* Confirmar Senha</label>
						                {!! Form::password('senha_confirmar',['id'=>'senha_confirmar','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('senha_confirmar') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-4"></div>
						   	</div>
						</div>
						<div class="form-actions right">
							<button type="submit" class="btn btn-primary block-page">
								<i class="fa fa-check-square-o"></i> Salvar
							</button>
						</div>
                   	{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@stop