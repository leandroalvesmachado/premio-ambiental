@extends('participante.template.layout')

@section('content')

@if (count($projetos) > 0)

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Projetos</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			</div>
			<div class="card-body collapse in">
				<div class="table-responsive">
					<table class="table mb-0 table-bordered table-striped">
						<thead>
							<tr class="text-uppercase">
								<th class="text-md-center">NOME</th>
								<th class="text-md-center">MODALIDADE</th>
								<th class="text-md-center">Responsável</th>
								<th class="text-md-center">AÇÕES</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($projetos as $projeto)
							<tr>
								<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->nome }}</td>
								<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->modalidade->nome }}</td>
								<td class="text-md-center" style="vertical-align: middle;">
									{{ $projeto->responsavel_nome }}
									<br>
									{{ $projeto->responsavel_telefone }}
									<br>
									{{ $projeto->responsavel_email }}
								</td>
								<td class="text-md-center" style="vertical-align: middle;">
									@if ( strtotime(date('Y-m-d H:i:s')) >= strtotime($evento->getOriginal('data_inicio')) &&  strtotime(date('Y-m-d H:i:s')) <=  strtotime($evento->getOriginal('data_fim')) )

									<a href="#" data-toggle="modal" data-target="#{{ $projeto->id }}">
										<i class="ft-x-circle"></i>&nbsp;<span class="">Remover</span>
									</a>
									@else
										#
									@endif
								</td>

								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $projeto->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="myModalLabel1">Projeto: {{ $projeto->nome }}</h4>
											</div>
											<div class="modal-body text-md-center">
												<h4 class="modal-title">
													<p style="margin-bottom: 4px;"></p>
												</h4>
												<h4 class="modal-title">
													Têm certeza que deseja remover o projeto?
												</h4>
												<br>
												<div class="row">
													<div class="col-md-3">&nbsp;</div>
													<div class="col-md-3">
														<a href="{{ route('participante.destroy',$projeto) }}">
															<button class="btn btn-block btn-success">Sim</button>
														</a>
													</div>
													<div class="col-md-3">
														<button class="btn btn-block btn-danger" data-dismiss="modal">Não</button>
													</div>
													<div class="col-md-3">&nbsp;</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
											</div>
										</div>
									</div>
								</div>

							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endif

<div class="row match-height">
    <div class="col-md-12">
    	<div class="card">
			<div class="card-body collapse in">
				<div class="card-block">
					@if ( strtotime(date('Y-m-d H:i:s')) >= strtotime($evento->getOriginal('data_inicio')) &&  strtotime(date('Y-m-d H:i:s')) <=  strtotime($evento->getOriginal('data_fim')) )

                    {!! Form::open(['route'=>'participante.projeto','class'=>'form','files'=>true]) !!}
						<div class="form-body">
							<h4 class="form-section">Adicionar Projeto</h4>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
						            	<label>* Nome do Projeto</label>
						                {!! Form::text('nome',null,['id'=>'nome','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('modalidade_id') ? ' has-danger' : '' }}">
						            	<label>* Modalidade</label>
						               	{!! Form::select('modalidade_id',$modalidades, null,['id'=>'modalidade_id','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('modalidade_id') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('responsavel_nome') ? ' has-danger' : '' }}">
						            	<label>* Responsável pelo Projeto</label>
						                {!! Form::text('responsavel_nome',null,['id'=>'responsavel_nome','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('responsavel_nome') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('responsavel_telefone') ? ' has-danger' : '' }}">
						            	<label>* Telefone do Responsável pelo Projeto</label>
						                {!! Form::text('responsavel_telefone',null,['id'=>'responsavel_telefone','class'=>'form-control fone']) !!}
						                <span class="help-block text-danger">{{ $errors->first('responsavel_telefone') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('responsavel_email') ? ' has-danger' : '' }}">
						            	<label>* E-mail do Responsável pelo Projeto</label>
						                {!! Form::text('responsavel_email',null,['id'=>'responsavel_email','class'=>'form-control']) !!}
						                <span class="help-block text-danger">{{ $errors->first('responsavel_email') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						   	<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
						        	<div class="form-group {{ $errors->has('arquivo') ? ' has-danger' : '' }}">
						            	<label>* Arquivo (Tamanho máximo 20MB)</label>
						                {!! Form::file('arquivo',['class'=>'dropify','data-max-file-size'=>'20M','data-allowed-file-extensions'=>'jpg jpeg png doc docx pdf','type'=>'file']) !!}
						                <span class="help-block text-danger">{{ $errors->first('arquivo') }}</span>
						        	</div>
						      	</div>
						      	<div class="col-md-2"></div>
						   	</div>
						</div>
						<div class="form-actions right">
							<button type="submit" class="btn btn-primary block-page">
								<i class="fa fa-check-square-o"></i> Salvar
							</button>
						</div>
                   	{!! Form::close() !!}
                   	@else
						Período encerrado
                   	@endif
				</div>
			</div>
		</div>
    </div>
</div>

@stop