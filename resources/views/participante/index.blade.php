@extends('participante.template.layout')

@section('content')

<div class="row match-height">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block text-md-center">
                    <b>Informações da inscrição</b>
                </div>
                <div class="card-block">
                    <div class="row text-md-center text-sm-center text-xl-center">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                            <b>MODALIDADES</b>
                        </div>
                    </div>
                    <br>
                    @forelse ($inscricao->inscricao_modalidades as $inscricao_modalidade)
                    <div class="row text-md-center text-sm-center text-xl-center">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                            {{ $inscricao_modalidade->modalidade->nome }}
                        </div>
                    </div>
                    <br>
                    @empty
                    @endforelse
                    <div class="row">
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <p class="card-text text-md-center"><b>PARTICIPANTE</b></p>
                        </div>
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <p class="card-text text-md-center"><b>EMPRESA ({{ $inscricao->porte->nome }})</b></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <p class="card-text"><b>N° Inscrição:</b>&nbsp;{{ $inscricao->login }}</p>
                            <p class="card-text"><b>CPF:</b>&nbsp;{{ $inscricao->cpf }}</p>
                            <p class="card-text"><b>Nome:</b>&nbsp;{{ $inscricao->nome }}</p>
                            <p class="card-text"><b>Endereço:</b>&nbsp;{{ $inscricao->endereco }}</p>
                            <p class="card-text"><b>E-mail:</b>&nbsp;{{ $inscricao->email }}</p>
                            <p class="card-text"><b>E-mail Alternativo:</b>&nbsp;{{ $inscricao->email_alternativo }}</p>
                            <p class="card-text"><b>Telefone:</b>&nbsp;{{ $inscricao->telefone }}</p>
                            <p class="card-text"><b>Celular:</b>&nbsp;{{ $inscricao->celular }}</p>
                        </div>
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <p class="card-text"><b>CNPJ:</b>&nbsp;{{ $inscricao->cnpj }}</p>
                            <p class="card-text"><b>Nome Fantasia:</b>&nbsp;{{ $inscricao->nome_fantasia }}</p>
                            <p class="card-text"><b>Razão Social:</b>&nbsp;{{ $inscricao->razao_social }}</p>
                            <p class="card-text"><b>Representante:</b>&nbsp;{{ $inscricao->representante }}</p>
                            <p class="card-text"><b>CEP:</b>&nbsp;{{ $inscricao->cep_empresa }}</p>
                            <p class="card-text"><b>Endereço:</b>&nbsp;{{ $inscricao->endereco_empresa }}</p>
                            <p class="card-text"><b>Estado:</b>&nbsp;{{ $inscricao->estado_empresa }}</p>
                            <p class="card-text"><b>Município:</b>&nbsp;{{ $inscricao->municipio_empresa }}</p>
                            <p class="card-text"><b>Bairro:</b>&nbsp;{{ $inscricao->bairro_empresa }}</p>
                            <p class="card-text"><b>Complemento:</b>&nbsp;{{ $inscricao->complemento_empresa }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop