<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>Prêmio FIEC por Desempenho Ambiental</title>
<base href="{{ url('') }}/">
<link rel="apple-touch-icon" href="img/icon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/icon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/feather/style.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap-extended.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/app.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/colors.min.css">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="stack/assets/css/style.css">
<!-- END Custom CSS-->
<link rel="stylesheet" href="js/dropify/dist/css/dropify.min.css">
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
	
	<!-- content -->
    <div class="container">
    	<div class="content-wrapper">
    		<div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-body text-md-center text-sm-center">
                                    <img class="card-img-top img-fluid" src="img/login-logo.png" alt="Card image cap" width="50%">
                                    <div class="card-block">
                                        <h4 class="card-title"><h5>{{ $evento->nome }}</h5></h4>
                                        <p class="card-text"><h5>{{ $evento->descricao }}</h5></p>
                                        <p class="card-text"><h5>{{ $evento->data_inicio }} à {{ $evento->data_fim }}</h5></p>
                                        <p class="card-text"><small class="text-muted"></small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="{{ route('participante.index') }}" class="btn mb-1 btn-primary btn-lg btn-block">Início</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('participante.documento') }}" class="btn mb-1 btn-primary btn-lg btn-block">Documento</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('participante.projeto') }}" class="btn mb-1 btn-primary btn-lg btn-block">Adicionar Projeto</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('participante.password') }}" class="btn mb-1 btn-primary btn-lg btn-block">Alterar Senha</a>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('participante.logout') }}" class="btn mb-1 btn-primary btn-lg btn-block">Sair</a>
                        </div>
                    </div>
                    @yield('content')
                </section>
				
    		</div>
    	</div>
    </div>
    <!-- content end -->

    <br><br>

    <!-- BEGIN VENDOR JS-->
    <script src="stack/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="stack/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="stack/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="stack/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="js/dropify/dist/js/dropify.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>

    @if(Session::get('message'))
    <script type="text/javascript">
        $(document).ready(function() {
            swal({
                title: "{{ Session::get('message')['title'] }}!",
                text: "{!! Session::get('message')['msg'] !!}",
                type: "{{ Session::get('message')['color'] }}"
            });
        });
    </script>
    @endif

    @yield('script')
</body>
</html>