@extends('admin.template.layout')

@section('title')

Editar Modalidade

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('modalidade.index') }}" >Modalidade</a></li>
<li class="breadcrumb-item active"><a href="{{ route('modalidade.edit',$modalidade) }}">Editar Modalidade</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::model($modalidade,['route'=>['modalidade.update', $modalidade]]) !!}
							@include('admin.modalidade.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop