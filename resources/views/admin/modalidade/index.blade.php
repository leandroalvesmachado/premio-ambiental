@extends('admin.template.layout')

@section('title')

Lista de Modalidades

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('modalidade.index') }}" >Modalidade</a></li>
<li class="breadcrumb-item active"><a href="{{ route('modalidade.create') }}">Lista de Modalidades</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
						{!! Form::open(['route'=>'modalidade.index','class'=>'form']) !!}
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
                                        <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label>Nome</label>
                                            {!! Form::text('nome','',['id'=>'nome','class'=>'form-control']) !!}
                                            <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
                                        </div>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-10"></div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-lg btn-block font-medium-1 btn-primary mb-1 block-page">
											<i class="fa fa-search"></i>
										</button>
									</div>
	                            </div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Modalidades ({{ $modalidades->total() }})</h4>
				    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				    <div class="heading-elements">
				    	<span class="float-xs-right">
				    		<button type="button" class="btn btn-success btn-block" onclick="excel();">Excel</button>
				    	</span>
				    </div>
				</div>
				<div class="card-body collapse in">
					<div class="table-responsive">
						<table class="table mb-0 table-bordered table-striped">
							<thead>
								<tr class="text-uppercase">
									<th class="text-md-center">NOME</th>
									<th class="text-md-center">DESCRIÇÃO</th>
									<th class="text-md-center">AÇÕES</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($modalidades as $modalidade)
								<tr>
									<td class="text-md-center" style="vertical-align: middle;">{{ $modalidade->nome }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $modalidade->descricao }}</td>
									<td class="text-md-center" style="vertical-align: middle;">
										<a href="javascript:void" data-toggle="modal" data-target="#{{ $modalidade->id }}">
											<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
										</a>
										<br>
										<a href="{{ route('modalidade.edit',$modalidade) }}">
											<i class="ft-edit"></i>&nbsp;<span class="">Editar</span>
										</a>
									</td>
								</tr>
								
								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $modalidade->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel9" aria-hidden="true">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content">
								            <div class="modal-header bg-warning white">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                    <span aria-hidden="true">&times;</span>
								                </button>
								                <h4 class="modal-title" id="myModalLabel9">Modalidade: {{ $modalidade->nome }}</h4>
								            </div>
								            <div class="modal-body">
								            	<h5><i class="fa fa-arrow-right"></i> ID: {{ $modalidade->id }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> NOME: {{ $modalidade->nome }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> DESCRIÇÃO: {{ $modalidade->descricao }}</h5>
								            </div>
								            <div class="modal-footer">
								                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
								            </div>
								        </div>
								    </div>
								</div>

								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer text-muted">
					<span></span>
					<span class="float-xs-right">
						{!! 
					    	$modalidades->appends([
					    		'nome'=>$modalidade_parametro['nome']
					    	])->render() 
					    !!}
						<br>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>

@stop