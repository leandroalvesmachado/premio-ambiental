
<div class="form-body">
	<h4 class="form-section">Modalidade</h4>
	<div class="row">
		<div class="col-md-6">
        	<div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
            	<label>* Nome</label>
                {!! Form::text('nome',null,['id'=>'nome','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
        	</div>
      	</div>
   	</div>
	<div class="row">
		<div class="col-md-12">
        	<div class="form-group {{ $errors->has('descricao') ? ' has-danger' : '' }}">
            	<label>* Descrição</label>
                {!! Form::textarea('descricao',null,['id'=>'descricao','class'=>'form-control','rows'=>'3']) !!}
                <span class="help-block text-danger">{{ $errors->first('descricao') }}</span>
        	</div>
      	</div>
	</div>
</div>
<div class="form-actions right">
    <!-- <a href="{{ URL::previous() }}" class="btn btn-warning mr-1">
        <i class="ft-x"></i> Voltar
    </a> -->
	<button type="submit" class="btn btn-primary block-page">
		<i class="fa fa-check-square-o"></i> Salvar
	</button>
</div>