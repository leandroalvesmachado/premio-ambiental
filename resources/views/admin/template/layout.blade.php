<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>Prêmio FIEC por Desempenho Ambiental</title>
<base href="{{ url('') }}/">
<link rel="apple-touch-icon" href="img/icon.ico">
<link rel="shortcut icon" type="image/x-icon" href="img/icon.ico">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/feather/style.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/pace.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/ui/prism.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/vendors/css/extensions/sweetalert.css">
<!-- END VENDOR CSS-->
<!-- BEGIN STACK CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/bootstrap-extended.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/app.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/colors.min.css">
<!-- END STACK CSS-->
<!-- BEGIN Page Level CSS-->
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="stack/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="stack/assets/css/style.css">
<!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
    
    <!-- navbar-fixed-top -->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                        <a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs">
                            <i class="ft-menu font-large-1"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="index-2.html" class="navbar-brand">
                            <img alt="stack admin logo" src="img/numa.png" class="brand-logo" width="32px" height="30px">
                            <h2 class="brand-text">PRÊMIO</h2>
                        </a>
                    </li>
                    <li class="nav-item hidden-md-up float-xs-right">
                        <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                            <i class="fa fa-ellipsis-v"></i>        
                        </a>
                    </li>
                </ul>
            </div>
            <div class="navbar-container content container-fluid">
                <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                    <ul class="nav navbar-nav">
                        <li class="nav-item hidden-sm-down">
                            <a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs">
                                <i class="ft-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item hidden-sm-down">
                            <a href="javascript:void()" class="nav-link nav-link-expand">
                                <i class="ficon ft-maximize"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav float-xs-right">
                        <!-- <li class="dropdown dropdown-notification nav-item">
                            <a href="#" data-toggle="dropdown" class="nav-link nav-link-label">
                                <i class="ficon ft-bell"></i>
                                <span class="tag tag-pill tag-default tag-danger tag-default tag-up">5</span>
                            </a>
                        </li>
                        <li class="dropdown dropdown-notification nav-item">
                            <a href="#" data-toggle="dropdown" class="nav-link nav-link-label">
                                <i class="ficon ft-mail"></i>
                                <span class="tag tag-pill tag-default tag-warning tag-default tag-up">3</span>
                            </a>
                        </li> -->
                        <li class="dropdown dropdown-notification nav-item">
                            <a href="javascript:void()" data-toggle="dropdown" class="nav-link nav-link-label" style="color: #008C8F; font-weight: bold; font-size: 12px;">
                                Último acesso: {{ Session::get('usuario')['ultimo_acesso'] }}
                            </a>
                        </li>
                        <li class="dropdown dropdown-user nav-item">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                <span class="avatar avatar-online">
                                    <img src="img/user.png" alt="avatar">
                                    <i></i>
                                </span>
                                <span class="user-name">{{ Session::get('usuario')['login'] }}</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- <a href="#" class="dropdown-item">
                                    <i class="ft-user"></i> Edit Profile
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ft-mail"></i> My Inbox
                                </a>
                                <a href="#" class="dropdown-item">
                                    <i class="ft-check-square"></i> Task
                                </a> -->
                                <!-- <a href="#" class="dropdown-item">
                                    <i class="ft-message-square"></i> Alterar Senha
                                </a> -->
                                <!-- <div class="dropdown-divider"></div> -->
                                <a href="{{ route('login.destroy') }}" class="dropdown-item">
                                    <i class="ft-power"></i> Sair
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- navbar-fixed-end -->

    <!-- menu -->
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
        <div class="main-menu-content">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                <!-- <li class="navigation-header">
                    <i class="ft-home"></i>
                    <span>Dashboard</span>
                    <i data-toggle="tooltip" data-placement="right" data-original-title="Dashboard" class=" ft-minus"></i>
                </li> -->

                <li class="nav-item {{ Route::getCurrentRoute()->getName() == 'admin.index' ? 'active' : '' }}">
                    <a href="{{ route('admin.index') }}">
                        <i class=""><img src="img/menu/house.png" width="24px" alt=""></i>
                        <span data-i18n="" class="menu-title">Dashboard</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <!-- <ul class="menu-content">
                        <li class="">
                            <a href="dashboard-ecommerce.html" class="menu-item">eCommerce</a>
                        </li>
                        <li class="">
                            <a href="dashboard-analytics.html" class="menu-item">Analytics</a>
                        </li>
                        <li class="">
                            <a href="dashboard-fitness.html" class="menu-item">Fitness</a>
                        </li>
                    </ul> -->
                </li>

                <li class="nav-item {{ Request::segment(3) == 'avaliador' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/teacher.png" width="24px" alt=""></i>
                        <span data-i18n="" class="menu-title">Avaliador</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'avaliador.create' ? 'active' : '' }}">
                            <a href="{{ route('avaliador.create') }}" class="menu-item">
                                <i class="ft-plus-square"></i> Novo Avaliador
                            </a>
                        </li>
                        <li class="{{ Route::getCurrentRoute()->getName() == 'avaliador.index' || Route::getCurrentRoute()->getName() == 'avaliador.edit' ? 'active' : '' }}">
                            <a href="{{ route('avaliador.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Avaliadores
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::segment(3) == 'documento' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/test.png" width="24px" alt=""></i>
                        <span data-i18n="" class="menu-title">Documento</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'documento.create' ? 'active' : '' }}">
                            <a href="{{ route('documento.create') }}" class="menu-item">
                                <i class="ft-plus-square"></i> Novo Documento
                            </a>
                        </li>
                        <li class="{{ Route::getCurrentRoute()->getName() == 'documento.index' || Route::getCurrentRoute()->getName() == 'documento.edit' ? 'active' : '' }}">
                            <a href="{{ route('documento.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Documentos
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Request::segment(3) == 'evento' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/event.png" width="24px" alt=""></i>
                        <span data-i18n="" class="menu-title">Evento</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'evento.create' ? 'active' : '' }}">
                            <a href="{{ route('evento.create') }}" class="menu-item">
                                <i class="ft-plus-square"></i> Novo Evento
                            </a>
                        </li>
                        <li class="{{ Route::getCurrentRoute()->getName() == 'evento.index' || Route::getCurrentRoute()->getName() == 'evento.edit' ? 'active' : '' }}">
                            <a href="{{ route('evento.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Eventos
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Request::segment(3) == 'inscricao' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/add-user.png"></i>
                        <span data-i18n="" class="menu-title">Inscrição</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'inscricao.index'  || Route::getCurrentRoute()->getName() == 'inscricao.edit' ? 'active' : '' }}">
                            <a href="{{ route('inscricao.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Inscrições
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Request::segment(3) == 'modalidade' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/park.png"></i>
                        <span data-i18n="" class="menu-title">Modalidade</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'modalidade.create' ? 'active' : '' }}">
                            <a href="{{ route('modalidade.create') }}" class="menu-item">
                                <i class="ft-plus-square"></i> Nova Modalidade
                            </a>
                        </li>
                        <li class="{{ Route::getCurrentRoute()->getName() == 'modalidade.index' || Route::getCurrentRoute()->getName() == 'modalidade.edit' ? 'active' : '' }}">
                            <a href="{{ route('modalidade.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Modalidades
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Request::segment(3) == 'porte' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/building.png" alt=""></i>
                        <span data-i18n="" class="menu-title">Porte</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'porte.create' ? 'active' : '' }}">
                            <a href="{{ route('porte.create') }}" class="menu-item">
                                <i class="ft-plus-square"></i> Novo Porte
                            </a>
                        </li>
                        <li class="{{ Route::getCurrentRoute()->getName() == 'porte.index' || Route::getCurrentRoute()->getName() == 'porte.edit' ? 'active' : '' }}">
                            <a href="{{ route('porte.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Portes
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{ Request::segment(3) == 'projeto' ? 'open active' : '' }}">
                    <a href="javascript:void()">
                        <i class=""><img src="img/menu/document.png" alt=""></i>
                        <span data-i18n="" class="menu-title">Projeto</span>
                        <!-- <span class="tag tag tag-primary tag-pill float-xs-right mr-2">3</span> -->
                    </a>
                    <ul class="menu-content">
                        <li class="{{ Route::getCurrentRoute()->getName() == 'projeto.index' ? 'active' : '' }}">
                            <a href="{{ route('projeto.index') }}" class="menu-item">
                                <i class="ft-grid"></i> Lista de Projetos
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item {{ Route::getCurrentRoute()->getName() == 'relatorio.index' ? 'active' : '' }}">
                    <a href="{{ route('relatorio.index') }}">
                        <i class=""><img src="img/menu/analytics.png" width="24px" alt=""></i>
                        <span data-i18n="" class="menu-title">Relatório</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- menu end -->

    <!-- content -->
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-xs-12 mb-1">
                    <h4 class="content-header-title">@yield('title')</h4>
                </div>
                <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                    <div class="breadcrumb-wrapper col-xs-12">
                        <ol class="breadcrumb">
                            @yield('breadcrumb')
                        </ol>
                    </div>
                </div>
                <div class="content-header-left col-md-6 col-xs-12">
                    <!-- Custom elements -->
                </div>
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>


    <!-- content end -->

    <footer class="footer navbar-fixed-bottom footer-light navbar-border" >
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-xs-block d-md-inline-block">
                <a href="https://www1.sfiec.org.br/sites/numa/?st=index" target="_blank" class="text-bold-800 grey darken-2" style="font-weight: bold; font-size: 12px;">
                    <!-- <img src="{{ url('img/logo-great-v2.png') }}" width="50px" alt=""> -->
                    https://www1.sfiec.org.br/sites/numa
                </a> 
            </span>
            <span class="float-md-right d-xs-block d-md-inline-block"  style="color: #000000; font-weight: bold; font-size: 12px;">
                (85) 3421.5916 / Av. Barão de Studart, 1980, Aldeota - Fortaleza-CE
            </span>
        </p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="stack/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="stack/app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="stack/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="stack/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="stack/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->

    <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="js/dropify/dist/js/dropify.min.js"></script>
    <script src="js/main.js" type="text/javascript"></script>

    @if(Session::get('message'))
    <script type="text/javascript">
        $(document).ready(function() {
            swal({
                title: "{{ Session::get('message')['title'] }}!",
                text: "{!! Session::get('message')['msg'] !!}",
                type: "{{ Session::get('message')['color'] }}"
            });
        });
    </script>
    @endif

    @yield('script')
</body>
</html>