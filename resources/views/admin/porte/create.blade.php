@extends('admin.template.layout')

@section('title')

Novo Porte 

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('porte.index') }}" >Porte</a></li>
<li class="breadcrumb-item active"><a href="{{ route('porte.create') }}">Novo Porte</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::open(['route'=>'porte.store','class'=>'form']) !!}
							@include('admin.porte.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop