<div class="form-body">
	<h4 class="form-section">Dados do Avaliador</h4>
	<div class="row">
        <div class="col-md-4">
        	<div class="form-group {{ $errors->has('cpf') ? ' has-danger' : '' }}">
            	<label>* CPF</label>
                {!! Form::text('cpf',null,['id'=>'cpf','class'=>'form-control cpf']) !!}
                <span class="help-block text-danger">{{ $errors->first('cpf') }}</span>
       		</div>
    	</div>
        <div class="col-md-8">
            <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                <label>* Nome</label>
                {!! Form::text('nome',null,['id'=>'nome','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
            </div>
        </div>
    </div>
	<div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                <label>* E-mail</label>
                {!! Form::text('email',null,['id'=>'email','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('email') }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('telefone') ? ' has-danger' : '' }}">
                <label>* Telefone</label>
                {!! Form::text('telefone',null,['id'=>'telefone','class'=>'form-control fone']) !!}
                <span class="help-block text-danger">{{ $errors->first('telefone') }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('celular') ? ' has-danger' : '' }}">
                <label>* Celular</label>
                {!! Form::text('celular',null,['id'=>'celular','class'=>'form-control celular']) !!}
                <span class="help-block text-danger">{{ $errors->first('celular') }}</span>
            </div>
        </div>
   	</div>
    <h4 class="form-section">Dados da Empresa do Avaliador</h4>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('cnpj') ? ' has-danger' : '' }}">
                <label>* CNPJ</label>
                {!! Form::text('cnpj',null,['id'=>'cnpj','class'=>'form-control cnpj']) !!}
                <span class="help-block text-danger">{{ $errors->first('cnpj') }}</span>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group {{ $errors->has('cargo') ? ' has-danger' : '' }}">
                <label>* Cargo do Avaliador</label>
                {!! Form::text('cargo',null,['id'=>'cargo','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('cargo') }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('nome_fantasia') ? ' has-danger' : '' }}">
                <label>* Nome Fantasia</label>
                {!! Form::text('nome_fantasia',null,['id'=>'nome_fantasia','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('nome_fantasia') }}</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('razao_social') ? ' has-danger' : '' }}">
                <label>* Razão Social</label>
                {!! Form::text('razao_social',null,['id'=>'razao_social','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('razao_social') }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('cep_empresa') ? ' has-danger' : '' }}">
                <label>* CEP</label>
                {!! Form::text('cep_empresa',null,['id'=>'cep_empresa','class'=>'form-control cep']) !!}
                <span class="help-block text-danger">{{ $errors->first('cep_empresa') }}</span>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('endereco_empresa') ? ' has-danger' : '' }}">
                <label>* Endereço</label>
                {!! Form::text('endereco_empresa',null,['id'=>'endereco_empresa','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('endereco_empresa') }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('estado_empresa') ? ' has-danger' : '' }}">
                <label>* Estado</label>
                {!! Form::text('estado_empresa',null,['id'=>'estado_empresa','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('estado_empresa') }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('municipio_empresa') ? ' has-danger' : '' }}">
                <label>* Município</label>
                {!! Form::text('municipio_empresa',null,['id'=>'municipio_empresa','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('municipio_empresa') }}</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('bairro_empresa') ? ' has-danger' : '' }}">
                <label>* Bairro</label>
                {!! Form::text('bairro_empresa',null,['id'=>'bairro_empresa','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('bairro_empresa') }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group {{ $errors->has('complemento_empresa') ? ' has-danger' : '' }}">
                <label>Complemento</label>
                {!! Form::text('complemento_empresa',null,['id'=>'complemento_empresa','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('complemento_empresa') }}</span>
            </div>
        </div>
    </div>
    <h4 class="form-section">Escolha qual evento o avaliador será responsável</h4>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group {{ $errors->has('evento_id') ? ' has-danger' : '' }}">
                <label>* Evento</label>
                {!! Form::select('evento_id',$eventos, null,['id'=>'evento_id','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('evento_id') }}</span>
            </div>
        </div>
    </div>
</div>

<div class="form-actions right">
	<button type="submit" class="btn btn-primary block-page">
		<i class="fa fa-check-square-o"></i> Salvar
	</button>
</div>