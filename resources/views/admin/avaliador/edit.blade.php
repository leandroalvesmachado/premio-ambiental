@extends('admin.template.layout')

@section('title')

Editar Avaliador

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('avaliador.index') }}" >Avaliador</a></li>
<li class="breadcrumb-item active"><a href="{{ route('avaliador.edit',$avaliador) }}">Editar Avaliador</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::model($avaliador,['route'=>['avaliador.update', $avaliador]]) !!}
							@include('admin.avaliador.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop