@extends('admin.template.layout')

@section('title')

Lista de Avaliadores

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('avaliador.index') }}" >Avaliador</a></li>
<li class="breadcrumb-item active"><a href="{{ route('avaliador.index') }}">Lista de Avaliadores</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
						{!! Form::open(['route'=>'avaliador.index','class'=>'form']) !!}
							<div class="form-body">
								<div class="row">
									<div class="col-md-6">
							            <div class="form-group {{ $errors->has('evento_id') ? ' has-danger' : '' }}">
							                <label>Evento</label>
							                {!! Form::select('evento_id',$eventos, null,['id'=>'evento_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('evento_id') }}</span>
							            </div>
							        </div>
									<div class="col-md-6">
                                        <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label>Nome</label>
                                            {!! Form::text('nome','',['id'=>'nome','class'=>'form-control']) !!}
                                            <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
                                        </div>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-10"></div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-lg btn-block font-medium-1 btn-primary mb-1 block-page">
											<i class="fa fa-search"></i>
										</button>
									</div>
	                            </div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Avaliadores ({{ $avaliadores->total() }})</h4>
				    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				    <div class="heading-elements">
				    	<!-- <span class="float-xs-right">
				    		<button type="button" class="btn btn-success btn-block">Novo Avaliador</button>
				    	</span> -->
				    </div>
				</div>
				<div class="card-body collapse in">
					<div class="table-responsive">
						<table class="table mb-0 table-bordered table-striped">
							<thead>
								<tr class="text-uppercase">
									<th class="text-md-center">LOGIN</th>
									<th class="text-md-center">CPF</th>
									<th class="text-md-center">NOME</th>
									<th class="text-md-center">EMPRESA</th>
									<th class="text-md-center">EVENTO</th>
									<th class="text-md-center">AÇÕES</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($avaliadores as $avaliador)
								<tr>
									<td class="text-md-center" style="vertical-align: middle;">{{ $avaliador->login }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $avaliador->cpf }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $avaliador->nome }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $avaliador->nome_fantasia }}</td>
									<td class="text-md-center" style="vertical-align: middle;">
										{{ $avaliador->evento->id.' | '.$avaliador->evento->nome.' | '.$avaliador->evento->data_inicio.' à '.$avaliador->evento->data_fim }}
									</td>
									<td class="text-md-center" style="vertical-align: middle;">
										<a href="javascript:void" data-toggle="modal" data-target="#{{ $avaliador->id }}">
											<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
										</a>
										<br>
										<a href="{{ route('avaliador.edit',$avaliador) }}">
											<i class="ft-edit"></i>&nbsp;<span class="">Editar</span>
										</a>
										<br>
										@if (!$avaliador->status)
										<a href="#" data-toggle="modal" data-target="#{{ $avaliador->id }}-status">
											<i class="ft-check-circle"></i>&nbsp;<span class="">Ativar</span>
										</a>
										@else
										<a href="#" data-toggle="modal" data-target="#{{ $avaliador->id }}-status">
											<i class="ft-x-circle"></i>&nbsp;<span class="">Desativar</span>
										</a>
										@endif
									</td>
								</tr>
								
								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $avaliador->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel9" aria-hidden="true">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content">
								            <div class="modal-header bg-warning white">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                    <span aria-hidden="true">&times;</span>
								                </button>
								                <h4 class="modal-title" id="myModalLabel9">Avaliador: {{ $avaliador->nome }}</h4>
								            </div>
								            <div class="modal-body">
								            	<h5>Evento que o avaliador será responsável</h5>
								            	<h5><i class="fa fa-arrow-right"></i> Evento: {{ $avaliador->evento->nome }}</h5>
								            	<br>
								            	<h5>Dados do Avaliador</h5>
								            	<h5><i class="fa fa-arrow-right"></i> CPF: {{ $avaliador->cpf }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Nome: {{ $avaliador->nome }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> E-mail: {{ $avaliador->email }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Telefone: {{ $avaliador->telefone }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Celular: {{ $avaliador->celular }}</h5>
								                <br>
								                <h5>Dados da Empresa do Avaliador</h5>
								                <h5><i class="fa fa-arrow-right"></i> CNPJ: {{ $avaliador->cnpj }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Nome Fantasia: {{ $avaliador->nome_fantasia }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Razão Social: {{ $avaliador->razao_social }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Cargo do Avaliador: {{ $avaliador->cargo }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> CEP: {{ $avaliador->cep_empresa }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Endereço: {{ $avaliador->endereco_empresa }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Estado: {{ $avaliador->estado_empresa }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Município: {{ $avaliador->municipio_empresa }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Bairro: {{ $avaliador->bairro_empresa }}</h5>
								                <h5><i class="fa fa-arrow-right"></i> Complemento: {{ $avaliador->complemento_empresa }}</h5>
								            </div>
								            <div class="modal-footer">
								                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
								            </div>
								        </div>
								    </div>
								</div>

								<!-- Modal Status -->
								<div class="modal fade text-xs-left" id="{{ $avaliador->id }}-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="myModalLabel1">Avaliador: {{ $avaliador->nome }} / Evento: {{ $avaliador->evento->nome }}</h4>
											</div>
											<div class="modal-body text-md-center">
												<h4 class="modal-title">
													<p style="margin-bottom: 4px;"></p>
												</h4>
												<h4 class="modal-title">
													@if (!$avaliador->status)
													Têm certeza que deseja ativar o avaliador?
													@else
													Têm certeza que deseja desativar o avaliador?
													@endif
												</h4>
												<br>
												<div class="row">
													<div class="col-md-3">&nbsp;</div>
													<div class="col-md-3">
														<a href="{{ route('avaliador.status', $avaliador) }}">
															<button class="btn btn-block btn-success">Sim</button>
														</a>
													</div>
													<div class="col-md-3">
														<button class="btn btn-block btn-danger" data-dismiss="modal">Não</button>
													</div>
													<div class="col-md-3">&nbsp;</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
											</div>
										</div>
									</div>
								</div>

								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer text-muted">
					<span></span>
					<span class="float-xs-right">
						{!! 
					    	$avaliadores->appends([
					    		'evento_id' => $avaliador_parametro['evento_id'],
					    		'nome' => $avaliador_parametro['nome']
					    	])->render() 
					    !!}
						<br>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>

@stop