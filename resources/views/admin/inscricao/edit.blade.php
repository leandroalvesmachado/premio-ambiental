@extends('admin.template.layout')

@section('title')

Editar Inscrição

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('inscricao.index') }}" >Inscrição</a></li>
<li class="breadcrumb-item active"><a href="{{ route('inscricao.edit',$inscricao) }}">Editar Inscrição</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::model($inscricao,['route'=>['inscricao.update', $inscricao]]) !!}
							@include('admin.inscricao.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop