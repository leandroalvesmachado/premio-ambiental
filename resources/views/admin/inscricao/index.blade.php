@extends('admin.template.layout')

@section('title')

Lista de Inscrições

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('inscricao.index') }}" >Inscrição</a></li>
<li class="breadcrumb-item active"><a href="#">Lista de Inscrições</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
						{!! Form::open(['route'=>'inscricao.index','class'=>'form']) !!}
							<div class="form-body">
								<div class="row">
									<div class="col-md-4">
                                        <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label>Nome</label>
                                            {!! Form::text('nome','',['id'=>'nome','class'=>'form-control']) !!}
                                            <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
                                        </div>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-6">
							        	<div class="form-group {{ $errors->has('evento_id') ? ' has-danger' : '' }}">
							            	<label>Evento</label>
							                {!! Form::select('evento_id',$eventos, null,['id'=>'evento_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('evento_id') }}</span>
							       		</div>
							    	</div>
								</div>
								<div class="row">
									<div class="col-md-6">
							        	<div class="form-group {{ $errors->has('porte_id') ? ' has-danger' : '' }}">
							            	<label>Porte</label>
							                {!! Form::select('porte_id',$portes, null,['id'=>'porte_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('porte_id') }}</span>
							       		</div>
							    	</div>
								</div>
								<div class="row">
									<div class="col-md-10"></div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-lg btn-block font-medium-1 btn-primary mb-1 block-page">
											<i class="fa fa-search"></i>
										</button>
									</div>
	                            </div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Inscrições ({{ $inscricoes->total() }})</h4>
				    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				    <div class="heading-elements">
				    	<span class="float-xs-right">
				    		<button type="button" class="btn btn-success btn-block" onclick="excel();">Excel</button>
				    	</span>
				    </div>
				</div>
				<div class="card-body collapse in">
					<div class="table-responsive">
						<table class="table mb-0 table-bordered table-striped">
							<thead>
								<tr class="text-uppercase">
									<th class="text-md-center">N°</th>
									<!-- <th class="text-md-center">Evento</th> -->
									<th class="text-md-center">Participante</th>
									<th class="text-md-center">Empresa</th>
									<!-- <th class="text-md-center">Login</th> -->
									<th class="text-md-center">Doc</th>
									<th class="text-md-center">AÇÕES</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($inscricoes as $inscricao)
								<tr>
									<td class="text-md-center" style="vertical-align: middle;">{{ $inscricao->id }}</td>
									<!-- <td class="text-md-center" style="vertical-align: middle;">{{ $inscricao->evento->nome }}</td> -->
									<td class="text-md-center" style="vertical-align: middle;">
										<p>
											{{ $inscricao->evento->nome }}
											<br>
										    <b>CPF:</b> {{ $inscricao->cpf }}
										    <br>
										    <b>Nome:</b> {{ $inscricao->nome }}
										    <br>
										    <b>Login:</b> {{ $inscricao->login }}
										</p>
									</td>
									<td class="text-md-center" style="vertical-align: middle;">
										<p>
											<b>CNPJ:</b> {{ $inscricao->cnpj }}
											<br>
											<b>Nome Fantasia:</b> {{ $inscricao->nome_fantasia }}
											<br>
										    <b>Porte:</b> {{ $inscricao->porte->nome }}
										</p>
									</td>
									<td class="text-md-center" style="vertical-align: middle;">
										{{--- {{ $inscricao->status ? 'Aceita' : 'Não Aceita' }} ---}}
										@forelse ($inscricao->documentos as $documento)
											<a href="{{ route('inscricao.download',['pasta'=>'documentos','id'=>$documento->id,'arquivo'=>$documento->arquivo]) }}">
											<!-- <a href="{{ str_replace('public/','',URL::to('/').Storage::url('app/documentos/'.$documento->id.'/'.$documento->arquivo)) }}" download> -->
												<i class="ft-download"></i>&nbsp;
												<span class="">{{ $documento->documento->nome }}</span>
											</a>
											<br>
										@empty
										
										@endforelse
									</td>
									
									<td class="text-md-center" style="vertical-align: middle;">
										<a href="javascript:void" data-toggle="modal" data-target="#{{ $inscricao->id }}">
											<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
										</a>
										<br>
										{{--- 
										<a href="{{ route('inscricao.edit',$inscricao) }}">
											<i class="ft-edit"></i>&nbsp;<span class="">Editar</span>
										</a>
										<br>
										
										@if (!$inscricao->status)
										<a href="#" data-toggle="modal" data-target="#{{ $inscricao->id }}-status">
											<i class="ft-check-circle"></i>&nbsp;<span class="">Aceitar</span>
										</a>
										@else
										<a href="#" data-toggle="modal" data-target="#{{ $inscricao->id }}-status">
											<i class="ft-x-circle"></i>&nbsp;<span class="">Remover</span>
										</a>
										@endif
										---}}
									</td>
								</tr>
								
								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $inscricao->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel9" aria-hidden="true">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content">
								            <div class="modal-header bg-warning white">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                    <span aria-hidden="true">&times;</span>
								                </button>
								                <h4 class="modal-title" id="myModalLabel9">Inscrição: N° {{ $inscricao->id }}</h4>
								            </div>
								            <div class="modal-body">
								            	<h5><i class="fa fa-arrow-right"></i> Participante</h5>
								            	<div class="row">
								            		<div class="col-md-6">
										            	<p>
										            		<b>CPF:</b> {{ $inscricao->cpf }}
										            		<br>
										            		<b>Nome:</b> {{ $inscricao->nome }}
										            		<br>
										            		<b>Endereço:</b> {{ $inscricao->endereco }}
										            		<br>
										            		<b>E-mail:</b> {{ $inscricao->email }} 
										            	</p>
									            	</div>
									            	<div class="col-md-6">
										            	<p>
										            		<b>E-mail Alternativo:</b> {{ $inscricao->email_alternativo }}
										            		<br>
										            		<b>Telefone:</b> {{ $inscricao->telefone }}
										            		<br>
										            		<b>Celular:</b> {{ $inscricao->celular }}
										            	</p>
									            	</div>
								            	</div>
								            	<h5><i class="fa fa-arrow-right"></i> Empresa</h5>
								            	<div class="row">
													<div class="col-md-6">
														<p>
										            		<b>CNPJ:</b> {{ $inscricao->cnpj }}
										            		<br>
										            		<b>Representante:</b> {{ $inscricao->representante }}
										            		<br>
										            		<b>Nome Fantasia:</b> {{ $inscricao->nome_fantasia }}
										            		<br>
										            		<b>Razão Social:</b> {{ $inscricao->razao_social }}
										            		<br>
										            		<b>CEP:</b> {{ $inscricao->cep_empresa }}
										            		<br>
										            		<b>Endereço:</b> {{ $inscricao->endereco_empresa }}
										            	</p>
													</div>
													<div class="col-md-6">
														<p>
										            		<b>Estado:</b> {{ $inscricao->estado_empresa }}
										            		<br>
										            		<b>Município:</b> {{ $inscricao->municipio_empresa }}
										            		<br>
										            		<b>Bairro:</b> {{ $inscricao->bairro_empresa }}
										            		<br>
										            		<b>Complemento:</b> {{ $inscricao->complemento_empresa }}
										            		<br>
										            		<b>Porte:</b> {{ $inscricao->porte->nome }}
										            	</p>
													</div>
								            	</div>
								            	<h5><i class="fa fa-arrow-right"></i> Modalidade</h5>
								            	<div class="row">
													<div class="col-md-12">
														@forelse ($inscricao->inscricao_modalidades as $modalidade)
														<p>{{ $modalidade->modalidade->nome }}</p>
														@empty
														@endforelse
													</div>
								            	</div>
								            	<h5><i class="fa fa-arrow-right"></i> Documentos</h5>
								            	@forelse ($inscricao->documentos as $documento)
								            	<div class="row">
								            		<div class="col-md-12">
								            			<b>{{ $documento->documento->nome }}:</b>
														<a href="{{ str_replace('public/','',URL::to('/').Storage::url('app/documentos/'.$documento->id.'/'.$documento->arquivo)) }}" download>
															<i class="ft-download"></i>&nbsp;<span class="">{{ $documento->arquivo }}</span>
														</a>
													</div>
												</div>
												@empty
												Nenhum documento adicionado
												@endforelse
								            </div>
								            <div class="modal-footer">
								                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
								            </div>
								        </div>
								    </div>
								</div>

								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $inscricao->id }}-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="myModalLabel1">Inscrição N° {{ $inscricao->id }}</h4>
											</div>
											<div class="modal-body text-md-center">
												<h4 class="modal-title">
													<p style="margin-bottom: 4px;"></p>
												</h4>
												<h4 class="modal-title">
													@if (!$inscricao->status)
													Têm certeza que deseja aprovar a inscrição?
													@else
													Têm certeza que deseja remover a inscrição?
													@endif
												</h4>
												<br>
												<div class="row">
													<div class="col-md-3">&nbsp;</div>
													<div class="col-md-3">
														<a href="{{ route('inscricao.status', $inscricao) }}">
															<button class="btn btn-block btn-success">Sim</button>
														</a>
													</div>
													<div class="col-md-3">
														<button class="btn btn-block btn-danger" data-dismiss="modal">Não</button>
													</div>
													<div class="col-md-3">&nbsp;</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
											</div>
										</div>
									</div>
								</div>

								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer text-muted">
					<span></span>
					<span class="float-xs-right">
						<nav aria-label="Page navigation">
						{!! 
					    	$inscricoes->appends([
					    		'nome'=>$inscricao_parametro['nome'],
					    		'porte_id'=>$inscricao_parametro['porte_id'],
					    		'evento_id'=>$inscricao_parametro['evento_id']
					    	])->render() 
					    !!}
						</nav>
						
						<br>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>

@stop