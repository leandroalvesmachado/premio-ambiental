@extends('admin.template.layout')

@section('title')

Lista de Projetos

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('projeto.index') }}" >Projeto</a></li>
<li class="breadcrumb-item active"><a href="{{ route('projeto.index') }}">Lista de Projetos</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
						{!! Form::open(['route'=>'projeto.index','class'=>'form']) !!}
							<div class="form-body">
								<div class="row">
									<div class="col-md-8">
							            <div class="form-group {{ $errors->has('evento_id') ? ' has-danger' : '' }}">
							                <label>Evento</label>
							                {!! Form::select('evento_id',$eventos, null,['id'=>'evento_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('evento_id') }}</span>
							            </div>
							        </div>
							        <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
                                            <label>Nome do Projeto</label>
                                            {!! Form::text('nome','',['id'=>'nome','class'=>'form-control']) !!}
                                            <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
                                        </div>
                                    </div>
								</div>
								<div class="row">
									<div class="col-md-4">
							            <div class="form-group {{ $errors->has('modalidade_id') ? ' has-danger' : '' }}">
							                <label>Modalidade</label>
							                {!! Form::select('modalidade_id',$modalidades, null,['id'=>'modalidade_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('modalidade_id') }}</span>
							            </div>
							        </div>
							        <div class="col-md-4">
							            <div class="form-group {{ $errors->has('porte_id') ? ' has-danger' : '' }}">
							                <label>Porte</label>
							                {!! Form::select('porte_id',$portes, null,['id'=>'porte_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('porte_id') }}</span>
							            </div>
							        </div>
								</div>
								<div class="row">
									<div class="col-md-10"></div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-lg btn-block font-medium-1 btn-primary mb-1 block-page">
											<i class="fa fa-search"></i>
										</button>
									</div>
	                            </div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Projetos ({{ $projetos->total() }})</h4>
				    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				    <div class="heading-elements">
				    	<!-- <span class="float-xs-right">
				    		<button type="button" class="btn btn-success btn-block">Novo Avaliador</button>
				    	</span> -->
				    </div>
				</div>
				<div class="card-body collapse in">
					<div class="table-responsive">
						<table class="table mb-0 table-bordered table-striped">
							<thead>
								<tr class="text-uppercase">
									<th class="text-md-center">PROJETO</th>
									<th class="text-md-center">INSCRIÇÃO</th>
									<th class="text-md-center">Evento</th>
									<th class="text-md-center">Modalidade</th>
									<th class="text-md-center">Porte</th>
									<th class="text-md-center">AÇÕES</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($projetos as $projeto)
								<tr>
									
									<td class="text-md-center" style="vertical-align: middle;">
										{{ $projeto->projeto_nome }}
									</td>
									<td class="text-md-center" style="vertical-align: middle;">
										CPF: {{ $projeto->inscricao_cpf }}
										<br>
										Nome: {{ $projeto->inscricao_nome }}
										<br>
										CNPJ: {{ $projeto->inscricao_cnpj }}
										<br>
										Empresa: {{ $projeto->inscricao_nome_fantasia }}
									</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->evento_nome }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->modalidade_nome }}</td>
									<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->porte_nome }}</td>
									<td class="text-md-center" style="vertical-align: middle;">
										<a href="javascript:void" data-toggle="modal" data-target="#{{ $projeto->projeto_id }}">
											<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
										</a>
										<br>
										<a href="{{ route('projeto.download',['pasta'=>'projetos','id'=>$projeto->projeto_id,'arquivo'=>$projeto->projeto_arquivo]) }}">
											<i class="ft-download"></i>&nbsp;<span class="">Download</span>
										</a>
									</td>

									<!-- Modal -->
									<div class="modal fade text-xs-left" id="{{ $projeto->projeto_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel9" aria-hidden="true">
									    <div class="modal-dialog modal-lg" role="document">
									        <div class="modal-content">
									            <div class="modal-header bg-warning white">
									                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									                    <span aria-hidden="true">&times;</span>
									                </button>
									                <h4 class="modal-title" id="myModalLabel9">Projeto: {{ $projeto->projeto_nome }}</h4>
									            </div>
									            <div class="modal-body">
									            	<h5><i class="fa fa-arrow-right"></i> Projeto</h5>
									            	<div class="row">
									            		<div class="col-md-12">
											            	<p>
											            		<b>Nome:</b> {{ $projeto->projeto_nome }}
											            		<br>
											            		<b>Evento:</b> {{ $projeto->evento_nome }}
											            		<br>
											            		<b>Modalidade:</b> {{ $projeto->modalidade_nome }}
											            		<br>
											            		<b>Responsável:</b> {{ $projeto->projeto_responsavel_nome }}
											            		<br>
											            		<b>Responsável Telefone:</b> {{ $projeto->projeto_responsavel_telefone }}
											            		<br>
											            		<b>Responsável E-mail:</b> {{ $projeto->projeto_responsavel_email }}
											            	</p>
										            	</div>
									            	</div>
									            	<h5><i class="fa fa-arrow-right"></i> Inscrição</h5>
									            	<div class="row">
									            		<div class="col-md-6">
											            	<p>
											            		<b>CPF:</b> {{ $projeto->inscricao_cpf }}
											            		<br>
											            		<b>Nome:</b> {{ $projeto->inscricao_nome }}
											            		<br>
											            		<b>Endereço:</b> {{ $projeto->inscricao_endereco }}
											            		<br>
											            		<b>E-mail:</b> {{ $projeto->inscricao_email }} 
											            	</p>
										            	</div>
										            	<div class="col-md-6">
											            	<p>
											            		
											            	</p>
										            	</div>
									            	</div>
									            	<h5><i class="fa fa-arrow-right"></i> Empresa</h5>
									            	<div class="row">
														<div class="col-md-6">
															<p>
											            		<b>CNPJ:</b> {{ $projeto->inscricao_cnpj }}
											            		<br>
											            		<b>Representante:</b> {{ $projeto->inscricao_representante }}
											            		<br>
											            		<b>Nome Fantasia:</b> {{ $projeto->inscricao_nome_fantasia }}
											            		<br>
											            		<b>Razão Social:</b> {{ $projeto->inscricao_razao_social }}
											            		<br>
											            		<b>CEP:</b> {{ $projeto->inscricao_cep_empresa }}
											            		<br>
											            		<b>Endereço:</b> {{ $projeto->inscricao_endereco_empresa }}
										            		</p>
														</div>
														<div class="col-md-6">
															<p>
											            		<b>Estado:</b> {{ $projeto->inscricao_estado_empresa }}
											            		<br>
											            		<b>Município:</b> {{ $projeto->inscricao_municipio_empresa }}
											            		<br>
											            		<b>Bairro:</b> {{ $projeto->inscricao_bairro_empresa }}
											            		<br>
											            		<b>Complemento:</b> {{ $projeto->inscricao_complemento_empresa }}
											            		<br>
											            		<b>Porte:</b> {{ $projeto->porte_nome }}
											            	</p>
														</div>
									            	</div>
									            </div>
									            <div class="modal-footer">
									                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
									            </div>
									        </div>
									    </div>
									</div>
								</tr>
								@empty
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-footer text-muted">
					<span></span>
					<span class="float-xs-right">
						{!! 
					    	$projetos->appends([
					    		'nome' => $projeto_parametro['nome'],
					    		'evento_id' => $projeto_parametro['evento_id'],
					    		'modalidade_id' => $projeto_parametro['modalidade_id'],
					    		'porte_id' => $projeto_parametro['porte_id']
					    	])->render() 
					    !!}
						<br>
					</span>
				</div>
			</div>
		</div>
	</div>
</section>

@stop