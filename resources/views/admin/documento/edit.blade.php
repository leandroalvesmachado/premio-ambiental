@extends('admin.template.layout')

@section('title')

Editar Documento

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('documento.index') }}" >Documento</a></li>
<li class="breadcrumb-item active"><a href="{{ route('documento.edit',$documento) }}">Editar Documento</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::model($documento,['route'=>['documento.update', $documento]]) !!}
							@include('admin.documento.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop