@extends('admin.template.layout')

@section('title')

Novo Evento

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('evento.index') }}" >Evento</a></li>
<li class="breadcrumb-item active"><a href="{{ route('evento.create') }}">Novo Evento</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::open(['route'=>'evento.store','class'=>'form']) !!}
							@include('admin.evento.partials.form')
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop