
<div class="form-body">
	<h4 class="form-section">Evento</h4>
	<div class="row">
		<div class="col-md-6">
        	<div class="form-group {{ $errors->has('nome') ? ' has-danger' : '' }}">
            	<label>* Nome</label>
                {!! Form::text('nome',null,['id'=>'nome','class'=>'form-control']) !!}
                <span class="help-block text-danger">{{ $errors->first('nome') }}</span>
        	</div>
      	</div>
   	</div>
   	<div class="row">
		<div class="col-md-4">
            <div class="form-group {{ $errors->has('data_inicio') ? ' has-danger' : '' }}">
            	<label>* Data Início</label>
                {!! Form::text('data_inicio',null,['id'=>'data_inicio','class'=>'form-control data']) !!}
                <span class="help-block text-danger">{{ $errors->first('data_inicio') }}</span>
        	 </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('data_fim') ? ' has-danger' : '' }}">
            	<label>* Data Término</label>
                {!! Form::text('data_fim',null,['id'=>'data_fim','class'=>'form-control data']) !!}
            	<span class="help-block text-danger">{{ $errors->first('data_fim') }}</span>
        	</div>
        </div>
	</div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('hora_inicio') ? ' has-danger' : '' }}">
                <label>* Hora Início</label>
                {!! Form::text('hora_inicio',null,['id'=>'hora_inicio','class'=>'form-control hora']) !!}
                <span class="help-block text-danger">{{ $errors->first('hora_inicio') }}</span>
             </div>
        </div>
        <div class="col-md-4">
            <div class="form-group {{ $errors->has('hora_fim') ? ' has-danger' : '' }}">
                <label>* Hora Término</label>
                {!! Form::text('hora_fim',null,['id'=>'hora_fim','class'=>'form-control hora']) !!}
                <span class="help-block text-danger">{{ $errors->first('hora_fim') }}</span>
            </div>
        </div>
    </div>
	<div class="row">
		<div class="col-md-12">
        	<div class="form-group {{ $errors->has('descricao') ? ' has-danger' : '' }}">
            	<label>* Descrição</label>
                {!! Form::textarea('descricao',null,['id'=>'descricao','class'=>'form-control','rows'=>'3']) !!}
                <span class="help-block text-danger">{{ $errors->first('descricao') }}</span>
        	</div>
      	</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group {{ $errors->has('modalidade') ? ' has-danger' : '' }}">
				
                <input type="hidden" name="modalidade[]" value="0" checked>
                
                <label>Modalidades adicionadas:</label>
                <br>
                @if(isset($evento))
                     @forelse ($evento->modalidades as $evento_modalidade)
                        {{ $evento_modalidade->modalidade->nome }}
                        <br>
                    @empty
                    @endforelse
                @endif
                <br>

                <label>* Modalidade</label>
                <br>
                @forelse ($modalidades as $modalidade)
                    {!! Form::checkbox('modalidade[]',$modalidade->id) !!} {{ $modalidade->nome }}
                    <br>
                @empty
                    Nenhuma modalidade cadastrada
                @endforelse
                <span class="help-block text-danger">{{ $errors->first('modalidade') }}</span>
			</div>
		</div>
	</div>
</div>
<div class="form-actions right">
    <!-- <a href="{{ URL::previous() }}" class="btn btn-warning mr-1">
        <i class="ft-x"></i> Voltar
    </a> -->
	<button type="submit" class="btn btn-primary block-page">
		<i class="fa fa-check-square-o"></i> Salvar
	</button>
</div>