@extends('admin.template.layout')

@section('title')

Dashboard

@stop

@section('breadcrumb')

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	<div id="poll_div1"></div>
                    	@columnchart('Inscrições por Modalidade - Último Evento Cadastrado', 'poll_div1')
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	<div id="poll_div2"></div>
                    	@columnchart('Inscrições por Porte - Último Evento Cadastrado', 'poll_div2')
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop