<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="NUMA - Admin Template">
<meta name="keywords" content="materia, webapp, admin, dashboard, template, ui">
<meta name="author" content="sfiec">
</head>
<body>
	<table class="table table-hover table-bordered">
		<thead>
			<tr style="vertical-align: middle; height: 30px;">
				<th colspan="11" style="text-align: center;">
					{{ $evento->nome }} - {{ $evento->data_inicio }} à {{ $evento->data_fim }}
				</th>
			</tr>
			<tr style="vertical-align: middle; height: 30px;">
				<!-- <th style="text-align: center;">INSCRIÇÃO</th>
				<th style="text-align: center;">NOME</th>
				<th style="text-align: center;">EMPRESA</th>
				<th style="text-align: center;">PORTE</th>
				<th style="text-align: center;">PROJETO</th>
				<th style="text-align: center;">MODALIDADE</th>
				<th style="text-align: center;">AVALIADOR</th>
				<th style="text-align: center;">NOTA</th>
				<th style="text-align: center;">COMENTÀRIO</th> -->
				<th style="text-align: center;">MODALIDADE</th>
				<th style="text-align: center;">PORTE</th>
				<th style="text-align: center;">PROJETO</th>
				<th style="text-align: center;">AVALIADORES</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($resultados as $modalidade => $modalidade_item)
				@forelse ($modalidade_item as $porte => $porte_item)
					@forelse ($porte_item as $projeto => $projeto_item)
						<tr style="vertical-align: middle;">
							<td style="text-align: center;">{{ $modalidade }}</td>
							<td style="text-align: center;">{{ $porte }}</td>
							<td style="text-align: center;">{{ $projeto }}</td>
							<td style="text-align: center; height: 60px;">
								@php
								$avaliador = [];
								foreach ($projeto_item as $item => $a) {
									array_push($avaliador,$a->avaliador_nome.' - '.$a->avaliador_projeto_nota);
								}

								echo implode("<br>",$avaliador);
								@endphp
							</td>
						</tr>
					@empty
					@endforelse
				@empty
				@endforelse
			@empty
			@endforelse
			{{--
			<tr>
				<td style="text-align: center;">{{ $resultado->inscricao_id }}</td>
				<td style="text-align: center;">{{ $resultado->inscricao_nome }}</td>
				<td style="text-align: center;">{{ $resultado->inscricao_nome_fantasia }}</td>
				<td style="text-align: center;">{{ $resultado->porte_nome }}</td>
				<td style="text-align: center;">{{ $resultado->projeto_nome }}</td>
				<td style="text-align: center;">{{ $resultado->modalidade_nome }}</td>
				<td style="text-align: center;">{{ $resultado->avaliador_nome }}</td>
				<td style="text-align: center;">{{ $resultado->avaliador_projeto_nota }}</td>
				<td style="text-align: center;">{{ $resultado->avaliador_projeto_comentario }}</td>
			</tr>
			--}}
			
		</tbody>
	</table>
</body>
</html>