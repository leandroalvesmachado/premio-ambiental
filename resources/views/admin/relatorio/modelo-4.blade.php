<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="NUMA - Admin Template">
<meta name="keywords" content="materia, webapp, admin, dashboard, template, ui">
<meta name="author" content="sfiec">
</head>
<body>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th colspan="11" style="text-align: center;">
					{{ $evento->nome }} - {{ $evento->data_inicio }} à {{ $evento->data_fim }}
				</th>
			</tr>
			<tr>
				<th style="text-align: center;">INSCRIÇÃO</th>
				<th style="text-align: center;">CPF</th>
				<th style="text-align: center;">NOME</th>
				<th style="text-align: center;">ENDEEREÇO</th>
				<th style="text-align: center;">EMAIL</th>
				<th style="text-align: center;">EMAIL ALTERNATIVO</th>
				<th style="text-align: center;">TELEFONE</th>
				<th style="text-align: center;">CELULAR</th>
				<th style="text-align: center;">EMPRESA</th>
				<th style="text-align: center;">PORTE</th>
				<th style="text-align: center;">MODALIDADE</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($resultados as $resultado)
			<tr>
				<td style="text-align: center;">{{ $resultado->id }}</td>
				<td style="text-align: center;">{{ $resultado->cpf }}</td>
				<td style="text-align: center;">{{ $resultado->nome }}</td>
				<td style="text-align: center;">{{ $resultado->endereco }}</td>
				<td style="text-align: center;">{{ $resultado->email }}</td>
				<td style="text-align: center;">{{ $resultado->email_alternativo }}</td>
				<td style="text-align: center;">{{ $resultado->telefone }}</td>
				<td style="text-align: center;">{{ $resultado->celular }}</td>
				<td style="text-align: center;">{{ $resultado->nome_fantasia }}</td>
				<td style="text-align: center;">{{ $resultado->porte->nome }}</td>
				<td style="text-align: center;">
					@php
						$modalidades = [];
						foreach ($resultado->inscricao_modalidades as $inscricao_modalidade) {
							array_push($modalidades,$inscricao_modalidade->modalidade->nome);
						}

						echo implode(", ",$modalidades);
					@endphp
					
				</td>
			</tr>
			@empty
			@endforelse
		</tbody>
	</table>
</body>
</html>