<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="NUMA - Admin Template">
<meta name="keywords" content="materia, webapp, admin, dashboard, template, ui">
<meta name="author" content="sfiec">
</head>
<body>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th colspan="22" style="text-align: center;">
					{{ $evento->nome }} - {{ $evento->data_inicio }} à {{ $evento->data_fim }}
				</th>
			</tr>
			<tr>
				<th style="text-align: center;">Inscrição</th>
				<th style="text-align: center;">Login</th>
				<th style="text-align: center;">CPF</th>
				<th style="text-align: center;">Nome</th>
				<th style="text-align: center;">Endereço</th>
				<th style="text-align: center;">E-mail</th>
				<th style="text-align: center;">E-mail Alternativo</th>
				<th style="text-align: center;">Telefone</th>
				<th style="text-align: center;">Celular</th>
				<th style="text-align: center;">CNPJ</th>
				<th style="text-align: center;">Nome Fantasia</th>
				<th style="text-align: center;">Razão Social</th>
				<th style="text-align: center;">Representante</th>
				<th style="text-align: center;">Representante Cargo</th>
				<th style="text-align: center;">CEP Empresa</th>
				<th style="text-align: center;">Endereço Empresa</th>
				<th style="text-align: center;">Estado Empresa</th>
				<th style="text-align: center;">Município Empresa</th>
				<th style="text-align: center;">Bairro Empresa</th>
				<th style="text-align: center;">Complemento Empresa</th>
				<th style="text-align: center;">Modalidade</th>
				<th style="text-align: center;">Porte</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($resultados as $resultado)
			<tr>
				<td style="text-align: center;">{{ $resultado->id }}</td>
				<td style="text-align: center;">{{ $resultado->login }}</td>
				<td style="text-align: center;">{{ $resultado->cpf }}</td>
				<td style="text-align: center;">{{ $resultado->nome }}</td>
				<td style="text-align: center;">{{ $resultado->endereco }}</td>
				<td style="text-align: center;">{{ $resultado->email }}</td>
				<td style="text-align: center;">{{ $resultado->email_alternativo }}</td>
				<td style="text-align: center;">{{ $resultado->telefone }}</td>
				<td style="text-align: center;">{{ $resultado->celular }}</td>
				<td style="text-align: center;">{{ $resultado->cnpj }}</td>
				<td style="text-align: center;">{{ $resultado->nome_fantasia }}</td>
				<td style="text-align: center;">{{ $resultado->razao_social }}</td>
				<td style="text-align: center;">{{ $resultado->representante }}</td>
				<td style="text-align: center;">{{ $resultado->representante_cargo }}</td>
				<td style="text-align: center;">{{ $resultado->cep_empresa }}</td>
				<td style="text-align: center;">{{ $resultado->endereco_empresa }}</td>
				<td style="text-align: center;">{{ $resultado->estado_empresa }}</td>
				<td style="text-align: center;">{{ $resultado->municipio_empresa }}</td>
				<td style="text-align: center;">{{ $resultado->bairro_empresa }}</td>
				<td style="text-align: center;">{{ $resultado->complemento_empresa }}</td>
				<td style="text-align: center;">
					@php
						$modalidades = [];
						foreach ($resultado->inscricao_modalidades as $inscricao_modalidade) {
							array_push($modalidades,$inscricao_modalidade->modalidade->nome);
						}

						echo implode(", ",$modalidades);
					@endphp
					
				</td>
				<td style="text-align: center;">{{ $resultado->porte->nome }}</td>
			</tr>
			@empty
			@endforelse
		</tbody>
	</table>
</body>
</html>