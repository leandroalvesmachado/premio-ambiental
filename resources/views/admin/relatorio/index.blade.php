@extends('admin.template.layout')

@section('title')

Relatórios

@stop

@section('breadcrumb')

<li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
<li class="breadcrumb-item active"><a href="{{ route('relatorio.index') }}">Relatórios</a></li>

@stop

@section('content')

<section id="basic-form-layouts">
	<div class="row match-height">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body collapse in">
					<div class="card-block">
                    	{!! Form::open(['route'=>'relatorio.create','class'=>'form']) !!}
							<div class="form-body">
								<h4 class="form-section">Escolha o Evento</h4>
								<div class="row">
									<div class="col-md-6">
							        	<div class="form-group {{ $errors->has('evento_id') ? ' has-danger' : '' }}">
							            	<label>* Evento</label>
							                {!! Form::select('evento_id',$eventos, null,['id'=>'evento_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('evento_id') }}</span>
							       		</div>
							    	</div>
								</div>
								<h4 class="form-section">Escolha o Modelo do Relatório</h4>
								<div class="row">
									<div class="col-md-8">
							        	<div class="form-group {{ $errors->has('relatorio_id') ? ' has-danger' : '' }}">
							            	<label>* Modelo</label>
							                {!! Form::select('relatorio_id',$relatorios, null,['id'=>'relatorio_id','class'=>'form-control']) !!}
							                <span class="help-block text-danger">{{ $errors->first('relatorio_id') }}</span>
							       		</div>
							    	</div>
								</div>
							</div>
							<div class="form-actions right">
								<button type="submit" class="btn btn-primary block-page">
									<i class="fa fa-check-square-o"></i> GERAR
								</button>
							</div>
                    	{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@stop