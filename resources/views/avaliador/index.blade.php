@extends('avaliador.template.layout')

@section('content')

<div class="row match-height">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body collapse in">
                <div class="card-block text-md-center">
                    <b>Informações do Avaliador</b>
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 text-md-center">
                            Bem-vindo, você foi escolhido para avaliar o evento abaixo:
                            <p class="card-text">&nbsp;{{ $evento->nome }}</p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <div class="col-xl-2 col-md-2 col-sm-2"></div>
                            <div class="col-xl-10 col-md-10 col-sm-10">
                                <p class="card-text"><b>CPF:</b>&nbsp;{{ $avaliador->cpf }}</p>
                                <p class="card-text"><b>Nome:</b>&nbsp;{{ $avaliador->nome }}</p>
                                <p class="card-text"><b>E-mail:</b>&nbsp;{{ $avaliador->email }}</p>
                                <p class="card-text"><b>Telefone:</b>&nbsp;{{ $avaliador->telefone }}</p>
                                <p class="card-text"><b>Celular:</b>&nbsp;{{ $avaliador->celular }}</p>
                            </div>
                            <div class="col-xl-2 col-md-2 col-sm-2"></div>
                        </div>
                        <div class="col-xl-6 col-md-6 col-sm-6">
                            <div class="col-xl-2 col-md-2 col-sm-2"></div>
                            <div class="col-xl-10 col-md-10 col-sm-10">
                                <p class="card-text"><b>CNPJ:</b>&nbsp;{{ $avaliador->cnpj }}</p>
                                <p class="card-text"><b>Nome Fantasia:</b>&nbsp;{{ $avaliador->nome_fantasia }}</p>
                                <p class="card-text"><b>Razão Social:</b>&nbsp;{{ $avaliador->razao_social }}</p>
                                <p class="card-text"><b>Cargo:</b>&nbsp;{{ $avaliador->cargo }}</p>
                            </div>
                            <div class="col-xl-2 col-md-2 col-sm-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop