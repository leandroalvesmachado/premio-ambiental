@extends('avaliador.template.layout')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Projetos</h4>
				<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
			</div>
			<div class="card-body collapse in">
				<div class="table-responsive">
					<table class="table mb-0 table-bordered table-striped">
						<thead>
							<tr class="text-uppercase">
								<th class="text-md-center">ARQUIVO</th>
								<th class="text-md-center">NOME</th>
								<th class="text-md-center">PORTE</th>
								<th class="text-md-center">MODALIDADE</th>
								<th class="text-md-center">NOTA</th>
								<th class="text-md-center">AÇÕES</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($projetos as $projeto)
							<tr>
								<td class="text-md-center" style="vertical-align: middle;">
									<a href="{{ route('avaliacao.download',['pasta'=>'projetos','id'=>$projeto->projeto_id,'arquivo'=>$projeto->projeto_arquivo]) }}">
										<i class="ft-download"></i>&nbsp;<span class="">Download</span>
									</a>
								</td>
								<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->projeto_nome }}</td>
								<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->porte_nome}}</td>
								<td class="text-md-center" style="vertical-align: middle;">{{ $projeto->modalidade_nome}}</td>
								
								@php
									$avaliado = $avaliador_projeto_repository->projetoAvaliado($projeto->projeto_id,$avaliador->id);
								@endphp

								@if (count($avaliado) == 0)
								<td class="text-md-center" style="vertical-align: middle;"><p class="bg-danger">AGUARDANDO NOTA</p></td>
								<td class="text-md-center" style="vertical-align: middle;">			
									<a href="javascript:void" data-toggle="modal" data-target="#{{ $projeto->projeto_id }}">
										<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
									</a>
									<br>
									<a href="{{ route('avaliacao.create',$projeto->projeto_id) }}">
										<i class="ft-clipboard"></i>&nbsp;<span class="">Nota</span>
									</a>
								</td>
								@else
								<td class="text-md-center" style="vertical-align: middle;">
									<p class="bg-success">AVALIADO <br> {{ $avaliado->nota }}</p>
								</td>
								<td class="text-md-center" style="vertical-align: middle;">			
									<a href="javascript:void" data-toggle="modal" data-target="#{{ $projeto->projeto_id }}">
										<i class="ft-clipboard"></i>&nbsp;<span class="">Detalhes</span>
									</a>
								</td>
								@endif

								<!-- Modal -->
								<div class="modal fade text-xs-left" id="{{ $projeto->projeto_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel9" aria-hidden="true">
								    <div class="modal-dialog modal-lg" role="document">
								        <div class="modal-content">
								            <div class="modal-header bg-warning white">
								                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								                    <span aria-hidden="true">&times;</span>
								                </button>
								                <h4 class="modal-title" id="myModalLabel9">Projeto: {{ $projeto->projeto_nome }}</h4>
								            </div>
								            <div class="modal-body">
								            	<div class="row">
													<div class="col-md-12 text-md-center">
														<b>PROJETO</b>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<p style="margin-bottom: 4px; text-align: justify;"><b>Nome:</b> {{ $projeto->projeto_nome }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Modalidade:</b> {{ $projeto->modalidade_nome }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Responsável:</b> {{ $projeto->projeto_responsavel_nome }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Responsável Telefone:</b> {{ $projeto->projeto_responsavel_telefone }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Responsável E-mail:</b> {{ $projeto->projeto_responsavel_email }}</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 text-md-center">
														<b>PARTICIPANTE</b>
													</div>
													<div class="col-md-6 text-md-center">
														<b>EMPRESA</b>
													</div>
												</div>
								            	<div class="row">
													<div class="col-md-6">
														<p style="margin-bottom: 4px; text-align: justify;"><b>CPF:</b> {{ $projeto->inscricao_cpf }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Nome:</b> {{ $projeto->inscricao_nome }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Endereço:</b> {{ $projeto->inscricao_endereco }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>E-mail:</b> {{ $projeto->inscricao_email }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>E-mail Alternativo:</b> {{ $projeto->inscricao_email_alternativo }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Telefone:</b> {{ $projeto->inscricao_telefone }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Celular:</b> {{ $projeto->inscricao_celular }}</p>
													</div>
													<div class="col-md-6">
														<p style="margin-bottom: 4px; text-align: justify;"><b>CNPJ:</b> {{ $projeto->inscricao_cnpj }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Nome Fantasia:</b> {{ $projeto->inscricao_nome_fantasia }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Razão Social:</b> {{ $projeto->inscricao_razao_social }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Porte:</b> {{ $projeto->porte_nome }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Representante:</b> {{ $projeto->inscricao_representante }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Cargo do Representante:</b> {{ $projeto->inscricao_representante_cargo }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>CEP:</b> {{ $projeto->inscricao_cep_empresa }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Endereço:</b> {{ $projeto->inscricao_endereco_empresa }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Estado:</b> {{ $projeto->inscricao_estado_empresa }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Munícipio:</b> {{ $projeto->inscricao_municipio_empresa }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Bairro:</b> {{ $projeto->inscricao_bairro_empresa }}</p>
														<p style="margin-bottom: 4px; text-align: justify;"><b>Complemento:</b> {{ $projeto->inscricao_complemento_empresa }}</p>
													</div>
												</div>
								            </div>
								            <div class="modal-footer">
								                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Fechar</button>
								            </div>
								        </div>
								    </div>
								</div>
							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@stop