@extends('avaliador.template.layout')

@section('content')



<div class="row match-height">
    <div class="col-md-12">
    	<div class="card">
			<div class="card-body collapse in">
				<div class="card-block">
                    {!! Form::open(['route'=>['avaliacao.store',$projeto->projeto_id],'class'=>'form','files'=>true]) !!}
						<div class="form-body">
							<h4 class="form-section">Projeto</h4>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Nome:</label>
								                {{ $projeto->projeto_nome }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Modalidade:</label>
								                {{ $projeto->modalidade_nome }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
								        	<div class="form-group">
								            	<label>Responsável:</label>
								                {{ $projeto->projeto_responsavel_nome }}
								        	</div>
								      	</div>
								   	</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
								        	<div class="form-group">
								            	<label>Responsável Telefone:</label>
								                {{ $projeto->projeto_responsavel_telefone }}
								        	</div>
								      	</div>
								   	</div>
								   	<div class="row">
										<div class="col-md-12">
								        	<div class="form-group">
								            	<label>Responsável E-mail:</label>
								                {{ $projeto->projeto_responsavel_email }}
								        	</div>
								      	</div>
								   	</div>
								   	<div class="row">
										<div class="col-md-12">
								        	<div class="form-group">
								            	<label>Arquivo:</label>
								                <a href="{{ str_replace('public/','',URL::to('/').Storage::url('app/projetos/'.$projeto->projeto_id.'/'.$projeto->projeto_arquivo)) }}" download>
													<i class="ft-download"></i>&nbsp;<span class="">Download</span>
												</a>
								        	</div>
								      	</div>
								   	</div>
								</div>
							</div>

						   	<div class="row">
								<div class="col-md-6">
									<h4 class="form-section">Participante</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>CPF:</label>
								                {{ $projeto->inscricao_cpf }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Nome:</label>
								                {{ $projeto->inscricao_nome }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Endereço:</label>
								                {{ $projeto->inscricao_endereco }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>E-mail:</label>
								                {{ $projeto->inscricao_email }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>E-mail Alternativo:</label>
								                {{ $projeto->inscricao_email_alternativo }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Telefone:</label>
								                {{ $projeto->inscricao_telefone }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Celular:</label>
								                {{ $projeto->inscricao_celular }}
								        	</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<h4 class="form-section">Empresa</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>CNPJ:</label>
								                {{ $projeto->inscricao_cnpj }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Nome Fantasia:</label>
								                {{ $projeto->inscricao_nome_fantasia }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Razão Social:</label>
								                {{ $projeto->inscricao_razao_social }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Porte:</label>
								                {{ $projeto->porte_nome }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Representante:</label>
								                {{ $projeto->inscricao_representante }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Cargo do Representante:</label>
								                {{ $projeto->inscricao_representante_cargo }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>CEP:</label>
								                {{ $projeto->inscricao_cep_empresa }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Endereço:</label>
								                {{ $projeto->inscricao_endereco_empresa }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Estado:</label>
								                {{ $projeto->inscricao_estado_empresa }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Munícipio:</label>
								                {{ $projeto->inscricao_municipio_empresa }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Bairro:</label>
								                {{ $projeto->inscricao_bairro_empresa }}
								        	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
								            	<label>Complemento:</label>
								                {{ $projeto->inscricao_complemento_empresa }}
								        	</div>
										</div>
									</div>
								</div>
						   	</div>
							<h4 class="form-section">Nota do Projeto</h4>
						   	<div class="row">
								<div class="col-md-4">
									<div class="form-group {{ $errors->has('nota') ? ' has-danger' : '' }}">
						            	<label>* Nota</label>
						                {!! Form::number('nota',null,['id'=>'nota','class'=>'form-control','min'=>'5','max'=>'10','step'=>'0.5']) !!}
						                <span class="help-block text-danger">{{ $errors->first('nota') }}</span>
						        	</div>
								</div>
						   	</div>
						   	<div class="row">
								<div class="col-md-8">
									<div class="form-group {{ $errors->has('comentario') ? ' has-danger' : '' }}">
						            	<label>Comentário</label>
						                {!! Form::textarea('comentario',null,['id'=>'comentario','class'=>'form-control','rows'=>'5']) !!}
						                <span class="help-block text-danger">{{ $errors->first('comentario') }}</span>
						        	</div>
								</div>
						   	</div>
						</div>
						<div class="form-actions right">
							<a href="{{ route('avaliacao.projeto') }}" class="btn btn-primary">
								Voltar
							</a>
							<button type="submit" class="btn btn-primary block-page">
								<i class="fa fa-check-square-o"></i> Salvar
							</button>
						</div>
                   	{!! Form::close() !!}
				</div>
			</div>
		</div>
    </div>
</div>

@stop