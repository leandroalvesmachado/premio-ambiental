<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute deve ser aceito.',
    'active_url'           => ':attribute não é uma URL válida.',
    'after'                => ':attribute deve ser uma data depois de :date.',
    'alpha'                => ':attribute deve conter somente letras.',
    'alpha_dash'           => ':attribute deve conter letras, números e traços.',
    'alpha_num'            => ':attribute deve conter somente letras e números.',
    'array'                => ':attribute deve ser um array.',
    'before'               => ':attribute deve ser uma data antes de :date.',
    'between'              => [
        'numeric' => ':attribute deve estar entre :min e :max.',
        'file'    => ':attribute deve estar entre :min e :max kilobytes.',
        'string'  => ':attribute deve estar entre :min e :max caracteres.',
        'array'   => ':attribute deve ter entre :min e :max itens.',
    ],
    'boolean'              => ':attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação de :attribute não confere.',
    'date'                 => ':attribute não é uma data válida.',
    'date_format'          => ':attribute não confere com o formato :format.',
    'different'            => ':attribute e :other devem ser diferentes.',
    'digits'               => ':attribute deve ter :digits dígitos.',
    'digits_between'       => ':attribute deve ter entre :min e :max dígitos.',
    'dimensions'           => ':attribute não tem dimensões válidas.',
    'distinct'             => ':attribute campo contém um valor duplicado.',
    'email'                => ':attribute deve ser um endereço de e-mail válido.',
    'exists'               => ':attribute selecionado é inválido.',
    'file'                 => ':attribute precisa ser um arquivo.',
    'filled'               => ':attribute é um campo obrigatório.',
    'image'                => ':attribute deve ser uma imagem.',
    'in'                   => ':attribute é inválido.',
    'in_array'             => ':attribute campo não existe em :other.',
    'integer'              => ':attribute deve ser um inteiro.',
    'ip'                   => ':attribute deve ser um endereço IP válido.',
    'json'                 => ':attribute deve ser um JSON válido.',
    'max'                  => [
        'numeric' => ':attribute não deve ser maior que :max.',
        'file'    => ':attribute não deve ter mais que :max kilobytes.',
        'string'  => ':attribute não deve ter mais que :max caracteres.',
        'array'   => ':attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => ':attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => ':attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute deve ser no mínimo :min.',
        'file'    => ':attribute deve ter no mínimo :min kilobytes.',
        'string'  => ':attribute deve ter no mínimo :min caracteres.',
        'array'   => ':attribute deve ter no mínimo :min itens.',
    ],
    'not_in'               => 'O :attribute selecionado é inválido.',
    'numeric'              => ':attribute deve ser um número.',
    'present'              => 'O :attribute deve estar presente.',
    'regex'                => 'O formato de :attribute é inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_unless'      => 'O :attribute é necessário a menos que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values.',
    'same'                 => ':attribute e :other devem ser iguais.',
    'size'                 => [
        'numeric' => ':attribute deve ser :size.',
        'file'    => ':attribute deve ter :size kilobytes.',
        'string'  => ':attribute deve ter :size caracteres.',
        'array'   => ':attribute deve conter :size itens.',
    ],
    'string'               => ':attribute deve ser uma string',
    'timezone'             => ':attribute deve ser uma timezone válida.',
    'unique'               => ':attribute já está em uso.',
    'uploaded'             => ':attribute falhou no upload.',
    'url'                  => 'O formato de :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'login' => 'LOGIN',
        'password' => 'SENHA',
        'email' => 'EMAIL',
        'cnpj' => 'CNPJ',
        'razao_social' => 'RAZÃO SOCIAL',
        'nome_fantasia' => 'NOME FANTASIA',
        'endereco' => 'ENDEREÇO',
        'bairro' => 'BAIRRO',
        'complemento' => 'COMPLEMENTO',
        'cep' => 'CEP',
        'nome' => 'NOME',
        'descricao' => 'DESCRIÇÃO',
        'data_inicio' => 'DATA INÍCIO',
        'data_termino' => 'DATA TÉRMINO',
        'faturamento_max' => 'FATURAMENTO ANUAL MÍNIMO',
        'faturamento_min' => 'FATURAMENTO ANUAL MÁXIMO',
        'funcionarios' => 'FUNCIONÁRIOS',
        'porte_id' => 'PORTE',
        'senha' => 'SENHA',
        'nome_curso' => 'NOME DO CURSO',
        'pre_requisito' => 'PRÉ-REQUISITOS', 
        'publico_alvo' => 'PÚBLICO ALVO',
        'id_modalidade' => 'MODALIDADE',
        'id_servico' => 'SERVIÇO',
        'objetivo' => 'OBJETIVO',
        'nome_disciplina' => 'NOME DA DISCIPLINA',
        'carga_horaria' => 'CARGA HORÁRIA',
        'programa' => 'PROGRAMA',
        'cpf' => 'CPF',
        'data_nasc' => 'DATA NASCIMENTO',
        'sexo' => 'SEXO',
        'id_estado_civil' => 'ESTADO CIVIL',
        'cpf' => 'CPF',
        'identidade' => 'IDENTIDADE',
        'orgao_expedidor' => 'ORGÃO EXPEDIDOR',
        'uf_orgao_expedidor' => 'UF ORGÃO EXPEDIDOR',
        'data_emissao_identidade' => 'DATA EMISSÃO IDENTIDADE',
        'id_nacionalidade' => 'NACIONALIDADE',
        'id_necessidades_especiais' => 'NECESSIDADES ESPECIAIS',
        'estado' => 'ESTADO',
        'id_municipio' => 'MUNICÍPIO',
        'id_bairro' => 'BAIRRO',
        'id_curso' => 'CURSO',
        'curso_incompany' => 'CURSO IN COMPANY',
        'horas_turno' => 'HORAS POR TURNO',
        'id_turno' => 'TURNO',
        'hora_inicio' => 'HORA INÍCIO',
        'hora_fim' => 'HORA FIM',
        'num_min_aluno' => 'NÚMERO MINIMO DE ALUNOS',
        'num_max_aluno' => 'NÚMERO MÁXIMO DE ALUNOS',
        'data_inicio_matricula' => 'DATA DE INÍCIO DA MATRÍCULA',
        'data_fim_matricula' => 'DATA DE FIM DA MATRÍCULA',
        'data_inicio' => 'DATA DE INÍCIO',
        'data_fim' => 'DATA DE TÉRMINO',
        'gestao_financeira' => 'GESTÃO FINANCEIRA',
        'sistema_avaliacao' => 'SISTEMA DE AVALIAÇÃO',
        'id_local_realizacao' => 'LOCAL DE REALIZAÇÃO',
        'id_cidade' => 'CIDADE',
        'investimento' => 'INVESTIMENTO',
        'parcelamento_especial' => 'PARCELAMENTO ESPECIAL',
        'domingo' => 'DOMINGO',
        'segunda' => 'SEGUNDA-FEIRA',
        'terca' => 'TERÇA-FEIRA',
        'quarta' => 'QUARTA-FEIRA',
        'quinta' => 'QUINTA-FEIRA',
        'sexta' => 'SEXTA-FEIRA',
        'sabado' => 'SÁBADO',
        'id_proposta_servico' => 'PROPOSTA DE SERVIÇO',
        'sintese_curricular_docente' => 'CURRÍCULO',
        'senha_confirmar' => 'CONFIRMAR SENHA',
        'id_situacao' => 'SITUAÇÃO',
        'nome_mae' => 'NOME DA MÃE',
        'nome_pai' => 'NOME DO PAI',
        'login_responsavel' => 'LOGIN DO RESPONSÁVEL',
        'id_escolaridade' => 'ESCOLARIDADE',
        'telefone' => 'TELEFONE',
        'celular' => 'CELULAR',
        'celular2' => 'CELULAR 2',
        'celular 3' => 'CELULAR 3',
        'modalidade' => 'MODALIDADE',
        'endereco_empresa' => 'ENDEREÇO',
        'cep_empresa' => 'CEP',
        'representante' => 'REPRESENTANTE',
        'representante_cargo' => 'CARGO DO REPRESENTANTE',
        'porte_id' => 'PORTE',
        'estado_empresa' => 'ESTADO',
        'municipio_empresa' => 'MUNICÍPIO',
        'bairro_empresa' => 'BAIRRO',
        'login' => 'LOGIN',
        'responsavel_nome' => 'RESPONSÁVEL',
        'responsavel_telefone' => 'TELEFONE DO RESPONSÁVEL',
        'responsavel_email' => 'E-MAIL DO RESPONSÁVEL',
        'arquivo' => 'ARQUIVO',
        'cargo' => 'CARGO',
        'evento_id' => 'EVENTO',
        'nota' => 'NOTA',
        'perfil' => 'PERFIL',
        'relatorio_id' => 'MODELO',
        'comentario' => 'COMENTÁRIO',
        'documento_id' => 'DOCUMENTO'
    ],

];
