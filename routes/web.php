<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=>'HomeController@index']);


Route::get('/inscricao',['uses'=>'HomeController@inscricao']);
Route::get('/edital',['uses'=>'HomeController@edital']);


/* LOGIN */
Route::group(['prefix'=>'login'], function() {
	Route::get('/',['as'=>'login.create','uses'=>'LoginController@create']);
	Route::post('/',['as'=>'login.store','uses'=>'LoginController@store']);

	Route::get('/logout',['as'=>'login.destroy','uses'=>'LoginController@destroy']);
});

/* RECUPERAR SENHA */
Route::group(['prefix'=>'recuperar-senha'], function() {
	Route::get('/',['as'=>'recover.create','uses'=>'RecoverPasswordController@create']);
	Route::post('/',['as'=>'recover.store','uses'=>'RecoverPasswordController@store']);
});

/* INSCRICAO */
Route::group(['prefix'=>'inscricao'], function() {
	Route::get('{evento_token}/cadastro',['as'=>'inscricao.create','uses'=>'Inscricao\HomeController@create']);
	Route::post('{evento_token}/cadastro',['as'=>'inscricao.store','uses'=>'Inscricao\HomeController@store']);
});

/* ADMIN */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin'], function() {
	Route::get('/',['as'=>'admin.index','uses'=>'Admin\HomeController@index']);
});

/* ADMIN - AVALIADOR */
Route::group(['middleware'=>'login','prefix'=>'admin/avaliador'], function() {
	Route::get('/',['as'=>'avaliador.index','uses'=>'Admin\AvaliadorController@index']);
	Route::post('/',['as'=>'avaliador.index','uses'=>'Admin\AvaliadorController@index']);

	Route::get('/cadastro',['as'=>'avaliador.create','uses'=>'Admin\AvaliadorController@create']);
	Route::post('/cadastro',['as'=>'avaliador.store','uses'=>'Admin\AvaliadorController@store']);

	Route::get('/{avaliador}/edicao',['as'=>'avaliador.edit','uses'=>'Admin\AvaliadorController@edit']);
	Route::post('/{avaliador}/edicao',['as'=>'avaliador.update','uses'=>'Admin\AvaliadorController@update']);

	Route::get('/{avaliador}/status',['as'=>'avaliador.status','uses'=>'Admin\AvaliadorController@status']);
});

/* ADMIN - DOCUMENTO */
Route::group(['middleware'=>'login','prefix'=>'admin/documento'], function() {
	Route::get('/',['as'=>'documento.index','uses'=>'Admin\DocumentoController@index']);
	Route::post('/',['as'=>'documento.index','uses'=>'Admin\DocumentoController@index']);

	Route::get('/cadastro',['as'=>'documento.create','uses'=>'Admin\DocumentoController@create']);
	Route::post('/cadastro',['as'=>'documento.store','uses'=>'Admin\DocumentoController@store']);

	Route::get('/{documento}/edicao',['as'=>'documento.edit','uses'=>'Admin\DocumentoController@edit']);
	Route::post('/{documento}/edicao',['as'=>'documento.update','uses'=>'Admin\DocumentoController@update']);
});

/* ADMIN - EVENTO */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/evento'], function() {
	Route::get('/',['as'=>'evento.index','uses'=>'Admin\EventoController@index']);
	Route::post('/',['as'=>'evento.index','uses'=>'Admin\EventoController@index']);

	Route::get('/cadastro',['as'=>'evento.create','uses'=>'Admin\EventoController@create']);
	Route::post('/cadastro',['as'=>'evento.store','uses'=>'Admin\EventoController@store']);

	Route::get('/{evento}/edicao',['as'=>'evento.edit','uses'=>'Admin\EventoController@edit']);
	Route::post('/{evento}/edicao',['as'=>'evento.update','uses'=>'Admin\EventoController@update']);
});

/* ADMIN - MODALIDADE */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/modalidade'], function() {
	Route::get('/',['as'=>'modalidade.index','uses'=>'Admin\ModalidadeController@index']);
	Route::post('/',['as'=>'modalidade.index','uses'=>'Admin\ModalidadeController@index']);

	Route::get('/cadastro',['as'=>'modalidade.create','uses'=>'Admin\ModalidadeController@create']);
	Route::post('/cadastro',['as'=>'modalidade.store','uses'=>'Admin\ModalidadeController@store']);

	Route::get('/{modalidade}/edicao',['as'=>'modalidade.edit','uses'=>'Admin\ModalidadeController@edit']);
	Route::post('/{modalidade}/edicao',['as'=>'modalidade.update','uses'=>'Admin\ModalidadeController@update']);
});

/* ADMIN - PORTE */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/porte'], function() {
	Route::get('/',['as'=>'porte.index','uses'=>'Admin\PorteController@index']);
	Route::post('/',['as'=>'porte.index','uses'=>'Admin\PorteController@index']);

	Route::get('/cadastro',['as'=>'porte.create','uses'=>'Admin\PorteController@create']);
	Route::post('/cadastro',['as'=>'porte.store','uses'=>'Admin\PorteController@store']);

	Route::get('/{porte}/edicao',['as'=>'porte.edit','uses'=>'Admin\PorteController@edit']);
	Route::post('/{porte}/edicao',['as'=>'porte.update','uses'=>'Admin\PorteController@update']);
});

/* ADMIN - INSCRICAO */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/inscricao'], function() {
	Route::get('/',['as'=>'inscricao.index','uses'=>'Admin\InscricaoController@index']);
	Route::post('/',['as'=>'inscricao.index','uses'=>'Admin\InscricaoController@index']);

	Route::get('/{inscricao}/edicao',['as'=>'inscricao.edit','uses'=>'Admin\InscricaoController@edit']);
	Route::post('/{inscricao}/edicao',['as'=>'inscricao.update','uses'=>'Admin\InscricaoController@update']);

	Route::get('/{inscricao}/status',['as'=>'inscricao.status','uses'=>'Admin\InscricaoController@status']);

	Route::get('/download/{pasta}/{id}/{arquivo}',['as'=>'inscricao.download','uses'=>'Admin\InscricaoController@download']);
});

/* ADMIN - PROJETO */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/projeto'], function() {
	Route::get('/',['as'=>'projeto.index','uses'=>'Admin\ProjetoController@index']);
	Route::post('/',['as'=>'projeto.index','uses'=>'Admin\ProjetoController@index']);
	Route::get('/download/{pasta}/{id}/{arquivo}',['as'=>'projeto.download','uses'=>'Admin\ProjetoController@download']);
});

/* ADMIN - RELATORIO */
Route::group(['middleware'=>['login','admin','profile'],'prefix'=>'admin/relatorio'], function() {
	Route::get('/',['as'=>'relatorio.index','uses'=>'Admin\RelatorioController@index']);
	Route::post('/',['as'=>'relatorio.create','uses'=>'Admin\RelatorioController@create']);
});


/* PARTICIPANTE */
Route::group(['middleware'=>['login','profile'],'prefix'=>'participante'], function() {
	Route::get('/',['as'=>'participante.index','uses'=>'Participante\HomeController@index']);
	Route::get('/logout',['as'=>'participante.logout','uses'=>'Participante\HomeController@logout']);
});

/* PARTICIPANTE - DOCUMENTO */
Route::group(['middleware'=>['login','profile'],'prefix'=>'participante/documento'], function() {
	Route::get('/',['as'=>'participante.documento','uses'=>'Participante\DocumentoController@create']);
	Route::post('/',['as'=>'participante.documento','uses'=>'Participante\DocumentoController@store']);

	Route::get('/{documento}/deletar',['as'=>'participante.documento.destroy','uses'=>'Participante\DocumentoController@destroy']);
});

/* PARTICIPANTE - PROJETO */
Route::group(['middleware'=>['login','profile'],'prefix'=>'participante/projeto'], function() {
	Route::get('/',['as'=>'participante.projeto','uses'=>'Participante\ProjetoController@create']);
	Route::post('/',['as'=>'participante.projeto','uses'=>'Participante\ProjetoController@store']);

	Route::get('/{projeto}/deletar',['as'=>'participante.destroy','uses'=>'Participante\ProjetoController@destroy']);
});

/* PARTICIPANTE - SENHA */
Route::group(['middleware'=>['login','profile'],'prefix'=>'participante/alterar-senha'], function() {
	Route::get('/',['as'=>'participante.password','uses'=>'Participante\PasswordController@edit']);
	Route::post('/',['as'=>'participante.password','uses'=>'Participante\PasswordController@update']);
});

/* AVALIADOR */
Route::group(['middleware'=>['login','profile'],'prefix'=>'avaliador'], function() {
	Route::get('/',['as'=>'avaliacao.index','uses'=>'Avaliador\HomeController@index']);
	Route::get('/logout',['as'=>'avaliacao.logout','uses'=>'Avaliador\HomeController@logout']);
});

/* AVALIADOR - PROJETO */
Route::group(['middleware'=>['login','profile'],'prefix'=>'avaliador/projeto'], function() {
	Route::get('/',['as'=>'avaliacao.projeto','uses'=>'Avaliador\ProjetoController@index']);

	Route::get('/{projeto}/nota',['as'=>'avaliacao.create','uses'=>'Avaliador\ProjetoController@create']);
	Route::post('/{projeto}/nota',['as'=>'avaliacao.store','uses'=>'Avaliador\ProjetoController@store']);

	Route::get('/download/{pasta}/{id}/{arquivo}',['as'=>'avaliacao.download','uses'=>'Avaliador\ProjetoController@download']);
});

/* AVALIADOR - SENHA */
Route::group(['middleware'=>['login','profile'],'prefix'=>'avaliador/alterar-senha'], function() {
	Route::get('/',['as'=>'avaliacao.password','uses'=>'Avaliador\PasswordController@edit']);
	Route::post('/',['as'=>'avaliacao.password','uses'=>'Avaliador\PasswordController@update']);
});