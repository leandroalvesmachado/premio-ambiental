<?php

namespace App\Http\Middleware;

use Closure;
use Route;

class Profile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = Route::getCurrentRoute()->getActionName();
        $profile = session()->get('usuario')['profile'];

        if (strpos($route, 'Admin') && $profile == 1) {
            return $next($request);
        }

        if (strpos($route, 'Participante') && $profile == 2) {
            return $next($request);
        }

        if (strpos($route, 'Avaliador') && $profile == 3) {
            return $next($request);
        }

        return redirect()->route('login.create');

    }
}
