<?php

namespace App\Http\Middleware;

use Closure;
use Route;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rota = Route::getCurrentRoute()->getActionName();
        
        if (strpos($rota, 'Admin')) {
            if (!array_key_exists("admin",session()->get('usuario'))) {
                return redirect()->route('login.create');
            }
        }

        return $next($request);
    }
}
