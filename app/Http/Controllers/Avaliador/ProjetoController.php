<?php

namespace App\Http\Controllers\Avaliador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Avaliador\ProjetoRequest;
use App\Repositories\ProjetoRepository;
use App\Repositories\AvaliadorProjetoRepository;
use Illuminate\Support\Facades\Storage;
use App\Models\Projeto;

class ProjetoController extends Controller
{
    protected $projeto_repository;
    protected $avaliador_projeto_repository;

    public function __construct(ProjetoRepository $projeto, AvaliadorProjetoRepository $avaliador_projeto)
    {
        $this->projeto_repository = $projeto;
        $this->avaliador_projeto_repository = $avaliador_projeto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $avaliador = $request->session()->get('usuario')['avaliador'];

        return view('avaliador.projeto.index',[
            'projetos' => $this->projeto_repository->avaliadorProjetos($avaliador->evento->id),
            'avaliador_projeto_repository' => $this->avaliador_projeto_repository,
            'avaliador' => $avaliador
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Projeto $projeto)
    {
        $avaliador = $request->session()->get('usuario')['avaliador'];

        return view('avaliador.projeto.create',[
            'projeto' => $this->projeto_repository->findProjeto($projeto->id)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjetoRequest $request, Projeto $projeto)
    {
        $resultado = $this->avaliador_projeto_repository->addNota($projeto,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Nota cadastrada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar a nota. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('avaliacao.projeto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($pasta, $id, $arquivo)
    {
        return response()->download(storage_path('app/'.$pasta.'/'.$id.'/'.$arquivo));
    }
}
