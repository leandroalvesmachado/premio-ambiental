<?php

namespace App\Http\Controllers\Avaliador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Avaliador\PasswordRequest;
use App\Repositories\AvaliadorRepository;

class PasswordController extends Controller
{
    protected $avaliador_repository;

    public function __construct(
        AvaliadorRepository $avaliador
    )
    {
        $this->avaliador_repository = $avaliador;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return view('avaliador.password.edit',[
            'evento' => [],
            'inscricao' => []
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordRequest $request)
    {
        $request->merge(['login_alteracao_avaliador' => session()->get('usuario')['id']]);
        
        $resultado = $this->avaliador_repository->edit(session()->get('usuario')['avaliador']['id'],$request->except(['_token','senha_confirmar']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Senha alterada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao alterar a senha. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('avaliacao.password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
