<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DocumentoRequest;
use App\Repositories\DocumentoRepository;
use App\Models\Documento;

class DocumentoController extends Controller
{
    protected $documento_repository;

    public function __construct(DocumentoRepository $documento)
    {
        $this->documento_repository = $documento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $documentos = $this->documento_repository->paginateWhere(10,'nome','ASC',$request->except(['_token','page']));
            $documento_parametro = [
                'nome' => $request->get('nome')
            ];
        } else {
            $documentos = $this->documento_repository->paginate(10,'nome','ASC');
            $documento_parametro = [
                'nome' => ''    
            ];
        }

        return view('admin.documento.index',[
            'documentos' => $documentos,
            'documento_parametro' => $documento_parametro
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.documento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentoRequest $request)
    {
        $resultado = $this->documento_repository->add($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Documento cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o documento. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('documento.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Documento $documento)
    {
        return view('admin.documento.edit',[
            'documento' => $documento
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentoRequest $request, Documento $documento)
    {
        $resultado = $this->documento_repository->edit($documento->id,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Documento editado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao editar o documento. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('documento.edit',['documento'=>$documento]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
