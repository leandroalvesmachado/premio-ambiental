<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PorteRequest;
use App\Repositories\PorteRepository;
use App\Models\Porte;

class PorteController extends Controller
{
    protected $porte_repository;

    public function __construct(PorteRepository $porte)
    {
        $this->porte_repository = $porte;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $portes = $this->porte_repository->paginateWhere(10,'nome','ASC',$request->except(['_token','page']));
            $porte_parametro = [
                'nome' => $request->get('nome')
            ];
        } else {
            $portes = $this->porte_repository->paginate(10,'nome','ASC');
            $porte_parametro = [
                'nome' => ''    
            ];
        }

        return view('admin.porte.index',[
            'portes' => $portes,
            'porte_parametro' => $porte_parametro
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.porte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PorteRequest $request)
    {
        $resultado = $this->porte_repository->add($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Porte cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o porte. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('porte.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Porte $porte)
    {
        return view('admin.porte.edit',[
            'porte' => $porte
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PorteRequest $request, Porte $porte)
    {
        $resultado = $this->porte_repository->edit($porte->id,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Porte editado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao editar o porte. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('porte.edit',['porte'=>$porte]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
