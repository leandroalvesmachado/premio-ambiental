<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProjetoRepository;
use App\Repositories\PorteRepository;
use App\Repositories\ModalidadeRepository;
use App\Repositories\EventoRepository;
use Exception;

class ProjetoController extends Controller
{
    protected $projeto_repository;
    protected $porte_repository;
    protected $modalidade_repository;
    protected $evento_repository;

    public function __construct(
        ProjetoRepository $projeto,
        PorteRepository $porte,
        ModalidadeRepository $modalidade,
        EventoRepository $evento
    )
    {
        $this->projeto_repository = $projeto;
        $this->porte_repository = $porte;
        $this->modalidade_repository = $modalidade;
        $this->evento_repository = $evento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $projetos = $this->projeto_repository->paginateWhereProjeto(10,$request->except(['_token','page']));
            $projeto_parametro = [
                'nome' => $request->get('nome'),
                'evento_id' => $request->get('evento_id'),
                'modalidade_id' => $request->get('modalidade_id'),
                'porte_id' => $request->get('porte_id')
            ];
        } else {
            $projetos = $this->projeto_repository->paginateProjeto(10);
            $projeto_parametro = [
                'nome' => '',
                'evento_id' => '',
                'modalidade_id' => '',
                'porte_id' => ''
            ];
        }

        return view('admin.projeto.index',[
            'projetos' => $projetos,
            'projeto_parametro' => $projeto_parametro,
            'eventos' => $this->evento_repository->dropdown(),
            'modalidades' => $this->modalidade_repository->dropdown(),
            'portes' => $this->porte_repository->dropdown()
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download(Request $request, $pasta, $id, $arquivo)
    {
        try {
            return response()->download(storage_path('app/'.$pasta.'/'.$id.'/'.$arquivo));
        } catch (Exception $e) {
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Arquivo não encontrado.','color'=>'error']);
            return redirect()->route('inscricao.index');
        }   
    }
}
