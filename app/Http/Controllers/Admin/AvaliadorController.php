<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AvaliadorRequest;
use App\Repositories\AvaliadorRepository;
use App\Repositories\EventoRepository;
use App\Models\Avaliador;

class AvaliadorController extends Controller
{
    protected $avaliador_repository;
    protected $evento_repository;

    public function __construct(AvaliadorRepository $avaliador, EventoRepository $evento)
    {
        $this->avaliador_repository = $avaliador;
        $this->evento_repository = $evento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $avaliadores = $this->avaliador_repository->paginateWhere(10,'nome','ASC',$request->except(['_token','page']));
            $avaliador_parametro = [
                'nome' => $request->get('nome'),
                'evento_id' => $request->get('evento_id')
            ];
        } else {
            $avaliadores = $this->avaliador_repository->paginate(10,'nome','ASC');
            $avaliador_parametro = [
                'nome' => '',
                'evento_id' => ''    
            ];
        }

        return view('admin.avaliador.index',[
            'avaliadores' => $avaliadores,
            'avaliador_parametro' => $avaliador_parametro,
            'eventos' => $this->evento_repository->dropDown()
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.avaliador.create',[
            'eventos' => $this->evento_repository->dropDown()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AvaliadorRequest $request)
    {
        $resultado = $this->avaliador_repository->addAvaliador($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Avaliador cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o avaliador. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('avaliador.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Avaliador $avaliador)
    {
        return view('admin.avaliador.edit',[
            'avaliador' => $avaliador,
            'eventos' => $this->evento_repository->dropDown()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AvaliadorRequest $request, Avaliador $avaliador)
    {
        $resultado = $this->avaliador_repository->edit($avaliador->id,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Avaliador editado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao editar o avaliador. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('avaliador.edit',['avaliador'=>$avaliador]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request, Avaliador $avaliador)
    {
        $resultado = $this->avaliador_repository->status($avaliador->id);

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Avaliador atualizado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao ativar o avaliador. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('avaliador.index');
    }
}
