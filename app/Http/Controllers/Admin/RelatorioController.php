<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\EventoRepository;
use App\Repositories\RelatorioRepository;
use App\Http\Requests\Admin\RelatorioRequest;


class RelatorioController extends Controller
{
    protected $evento_repository;
    protected $relatorio_repository;

    public function __construct(
        EventoRepository $evento,
        RelatorioRepository $relatorio
    )
    {
        $this->evento_repository = $evento;
        $this->relatorio_repository = $relatorio;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.relatorio.index',[
            'eventos' => $this->evento_repository->dropDown()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(RelatorioRequest $request)
    {
        if ($request->get('relatorio_id') == 1) {
            $resultados = $this->relatorio_repository->relatorioModelo1($request->except(['_token']));
            $evento = $this->evento_repository->find($request->get('evento_id'));

            if ($resultados != FALSE) {
                $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Relatório gerado com sucesso.','color'=>'success']);

                Excel::create(str_slug($evento->nome).'-relatorio-modelo-1-'.date('dmYHis'), function($excel) use ($evento, $resultados) {
                    $excel->sheet('Modelo 1 - Todas as inscrições', function($sheet) use ($evento, $resultados) {
                            $sheet->loadView('admin.relatorio.modelo-1',[
                                'evento' => $evento,
                                'resultados'=>$resultados
                            ]);
                    });
                })->download('xls');
            } else {
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao gerar o relatório.','color'=>'error']);
            }
        }

        if ($request->get('relatorio_id') == 2) {
            $resultado = $this->relatorio_repository->relatorioModelo2($request->except(['_token']));

            $request->session()->flash('message',['title'=>'Informação','msg'=>'Relatório em desenvolvimento.','color'=>'info']);
            return redirect()->route('relatorio.index');
        }

        if ($request->get('relatorio_id') == 3) {
            $resultados = $this->relatorio_repository->relatorioModelo3($request->except(['_token']));
            $evento = $this->evento_repository->find($request->get('evento_id'));

            // return view('admin.relatorio.modelo-3',[
            //     'resultados' => $resultados,
            //     'evento' => $evento
            // ]);

            Excel::create(str_slug($evento->nome).'-relatorio-modelo-3-'.date('dmYHis'), function($excel) use ($evento, $resultados) {
                $excel->sheet('Modelo 3 - Projetos Avaliados', function($sheet) use ($evento, $resultados) {
                    $sheet->loadView('admin.relatorio.modelo-3',[
                        'evento' => $evento,
                        'resultados'=> $resultados
                    ]);
                });
            })->download('xls');
        }

        if ($request->get('relatorio_id') == 4) {
            $resultados = $this->relatorio_repository->relatorioModelo1($request->except(['_token']));
            $evento = $this->evento_repository->find($request->get('evento_id'));

            if ($resultados != FALSE) {
                $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Relatório gerado com sucesso.','color'=>'success']);

                Excel::create(str_slug($evento->nome).'-relatorio-modelo-4-'.date('dmYHis'), function($excel) use ($evento, $resultados) {
                    $excel->sheet('Modelo 4 - Inscrições resumo', function($sheet) use ($evento, $resultados) {
                            $sheet->loadView('admin.relatorio.modelo-4',[
                                'evento' => $evento,
                                'resultados'=>$resultados
                            ]);
                    });
                })->download('xls');
            } else {
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao gerar o relatório.','color'=>'error']);
            }
        }

        if ($request->get('relatorio_id') == 5) {
            $resultados = $this->relatorio_repository->relatorioModelo1($request->except(['_token']));
            $evento = $this->evento_repository->find($request->get('evento_id'));

            if ($resultados != FALSE) {
                $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Relatório gerado com sucesso.','color'=>'success']);

                Excel::create(str_slug($evento->nome).'-relatorio-modelo-5-'.date('dmYHis'), function($excel) use ($evento, $resultados) {
                    $excel->sheet('Modelo 4 - Inscrições resumo', function($sheet) use ($evento, $resultados) {
                            $sheet->loadView('admin.relatorio.modelo-5',[
                                'evento' => $evento,
                                'resultados'=>$resultados
                            ]);
                    });
                })->download('xls');

                // return view('admin.relatorio.modelo-5',[
                //     'evento' => $evento,
                //     'resultados'=>$resultados
                // ]);
            } else {
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao gerar o relatório.','color'=>'error']);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
