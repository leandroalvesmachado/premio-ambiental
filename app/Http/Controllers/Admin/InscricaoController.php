<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\InscricaoRepository;
use App\Repositories\ModalidadeRepository;
use App\Repositories\EventoRepository;
use App\Repositories\PorteRepository;
use App\Models\Inscricao;
use Exception;

class InscricaoController extends Controller
{
    protected $evento_repository;
    protected $modalidade_repository;
    protected $porte_repository;
    protected $inscricao_repository;

    public function __construct(
        ModalidadeRepository $modalidade, 
        PorteRepository $porte,
        EventoRepository $evento,
        InscricaoRepository $inscricao
    )
    {
        $this->evento_repository = $evento;
        $this->modalidade_repository = $modalidade;
        $this->porte_repository = $porte;
        $this->inscricao_repository = $inscricao;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $inscricoes = $this->inscricao_repository->paginateWhere(10,'created_at','DESC',$request->except(['_token','page']));
            $inscricao_parametro = [
                'nome' => $request->get('nome'),
                'porte_id' => $request->get('porte_id'),
                'evento_id' => $request->get('evento_id'),
            ];
        } else {
            $inscricoes = $this->inscricao_repository->paginate(10,'created_at','DESC');
            $inscricao_parametro = [
                'nome' => '',
                'porte_id' => '',
                'evento_id' => '' 
            ];
        }

        return view('admin.inscricao.index',[
            'inscricoes' => $inscricoes,
            'inscricao_parametro' => $inscricao_parametro,
            'eventos' => $this->evento_repository->dropDown(),
            'portes' => $this->porte_repository->dropDown()
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inscricao $inscricao)
    {
        return view('admin.inscricao.edit',[
            'inscricao' => $inscricao,
            'modalidades' => $this->modalidade_repository->findAll(),
            'portes' => $this->porte_repository->dropDown()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status(Request $request, Inscricao $inscricao)
    {
        $resultado = $this->inscricao_repository->status($inscricao->id);

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Inscrição aceita com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao aceitar a inscrição. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('inscricao.index');
    }

    public function download(Request $request, $pasta, $id, $arquivo)
    {
        try {
            return response()->download(storage_path('app/'.$pasta.'/'.$id.'/'.$arquivo));
        } catch (Exception $e) {
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Arquivo não encontrado.','color'=>'error']);
            return redirect()->route('inscricao.index');
        }
    }
}
