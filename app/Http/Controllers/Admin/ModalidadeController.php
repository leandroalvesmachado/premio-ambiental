<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ModalidadeRequest;
use App\Repositories\ModalidadeRepository;
use App\Models\Modalidade;

class ModalidadeController extends Controller
{
    protected $modalidade_repository;

    public function __construct(ModalidadeRepository $modalidade)
    {
        $this->modalidade_repository = $modalidade;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $modalidades = $this->modalidade_repository->paginateWhere(10,'nome','ASC',$request->except(['_token','page']));
            $modalidade_parametro = [
                'nome' => $request->get('nome')
            ];
        } else {
            $modalidades = $this->modalidade_repository->paginate(10,'nome','ASC');
            $modalidade_parametro = [
                'nome' => ''    
            ];
        }

        return view('admin.modalidade.index',[
            'modalidades' => $modalidades,
            'modalidade_parametro' => $modalidade_parametro
        ])->with($request->flash());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modalidade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModalidadeRequest $request)
    {
        $resultado = $this->modalidade_repository->add($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Modalidade cadastrada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar a modalidade. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('modalidade.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Modalidade $modalidade)
    {
        return view('admin.modalidade.edit',[
            'modalidade' => $modalidade
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModalidadeRequest $request, Modalidade $modalidade)
    {
        $resultado = $this->modalidade_repository->edit($modalidade->id,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Modalidade editada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao editar a modalidade. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('modalidade.edit',['modalidade'=>$modalidade]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
