<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Lavacharts;
use App\Repositories\InscricaoModalidadeRepository;
use App\Repositories\InscricaoRepository;
use App\Repositories\EventoRepository;

class HomeController extends Controller
{
    protected $inscricao_modalidade_repository;
    protected $inscricao_repository;
    protected $evento_repository;

    public function __construct(
        InscricaoModalidadeRepository $inscricao_modalidade,
        InscricaoRepository $inscricao,
        EventoRepository $evento
    )
    {
        $this->inscricao_modalidade_repository = $inscricao_modalidade;
        $this->inscricao_repository = $inscricao;
        $this->evento_repository = $evento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* Último evento cadastrado */
        $evento = $this->evento_repository->findLast();

        $inscricao_modalidade = [];
        $inscricao_porte = [];

        /* Grafico total de inscricoes por evento */


        /* Grafico inscricoes por modalidade */
        if (count($evento) > 0) {
            $inscricao_modalidade = $this->inscricao_modalidade_repository->inscricaoPorModalidade($evento->id);
        }

        $inscricao_modalidade_datatable = \Lava::DataTable();
        $inscricao_modalidade_datatable->addStringColumn('Modalidade');
        $inscricao_modalidade_datatable->addNumberColumn('Quantidade');

        foreach ($inscricao_modalidade as $modalidade) {
            $inscricao_modalidade_datatable->addRow([$modalidade->nome,$modalidade->quantidade]);
        }

        $inscricao_modalidade_view = \Lava::ColumnChart('Inscrições por Modalidade - Último Evento Cadastrado', $inscricao_modalidade_datatable,[
            'title' => 'Inscrições por Modalidade - Último Evento Cadastrado',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 12
            ],
            'legend' => [
                'position' => 'bottom',
                'alignment' => 'center'
            ]
        ]);

        /* Grafico inscricoes por porte */
        if (count($evento) > 0) {
            $inscricao_porte = $this->inscricao_repository->inscricaoPorPorte($evento->id);
        }

        $inscricao_porte_datatable = \Lava::DataTable();
        $inscricao_porte_datatable->addStringColumn('Porte');
        $inscricao_porte_datatable->addNumberColumn('Quantidade');

        foreach ($inscricao_porte as $porte) {
            $inscricao_porte_datatable->addRow([$porte->nome,$porte->quantidade]);
        }

        $inscricao_porte_view = \Lava::ColumnChart('Inscrições por Porte - Último Evento Cadastrado', $inscricao_porte_datatable,[
            'title' => 'Inscrições por Porte - Último Evento Cadastrado',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 12
            ],
            'legend' => [
                'position' => 'bottom',
                'alignment' => 'center'
            ]
        ]);

        return view('admin.index',[
            'inscricao_modalidade_view' => $inscricao_modalidade_view,
            'inscricao_porte_view' => $inscricao_porte_view
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
