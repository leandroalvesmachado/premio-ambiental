<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EventoRequest;
use App\Repositories\EventoRepository;
use App\Models\Evento;
use App\Repositories\ModalidadeRepository;

class EventoController extends Controller
{
    protected $evento_repository;
    protected $modalidade_repository;

    public function __construct(EventoRepository $evento, ModalidadeRepository $modalidade)
    {
        $this->evento_repository = $evento;
        $this->modalidade_repository = $modalidade;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (count($request->all()) > 0) {
            $eventos = $this->evento_repository->paginateWhere(10,'nome','ASC',$request->except(['_token','page']));
            $evento_parametro = [
                'nome' => $request->get('nome')
            ];
        } else {
            $eventos = $this->evento_repository->paginate(10,'nome','ASC');
            $evento_parametro = [
                'nome' => ''    
            ];
        }

        return view('admin.evento.index',[
            'eventos' => $eventos,
            'evento_parametro' => $evento_parametro
        ])->with($request->flash());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.evento.create',[
            'modalidades' => $this->modalidade_repository->findAll()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventoRequest $request)
    {
        $resultado = $this->evento_repository->addEvento($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Evento cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o evento. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('evento.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Evento $evento)
    {
        return view('admin.evento.edit',[
            'evento' => $evento,
            'modalidades' => $this->modalidade_repository->findAll()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventoRequest $request, Evento $evento)
    {
        $resultado = $this->evento_repository->editEvento($evento->id,$request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Evento editado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao editar o evento. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('evento.edit',['evento'=>$evento]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
