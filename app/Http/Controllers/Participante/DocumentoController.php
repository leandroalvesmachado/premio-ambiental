<?php

namespace App\Http\Controllers\Participante;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Participante\DocumentoRequest;
use App\Repositories\DocumentoRepository;
use App\Repositories\InscricaoDocumentoRepository;
use Illuminate\Support\Facades\Storage;
use App\Models\Documento;
use App\Models\InscricaoDocumento;

class DocumentoController extends Controller
{
    protected $documento_repository;
    protected $inscricao_documento_repository;

    public function __construct(DocumentoRepository $documento, InscricaoDocumentoRepository $inscricao_documento)
    {
        $this->documento_repository = $documento;
        $this->inscricao_documento_repository = $inscricao_documento;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('participante.documento.create',[
            'evento' => $request->session()->get('usuario')['evento'],
            'inscricao' => $request->session()->get('usuario')['inscricao'],
            'documentos' => $this->documento_repository->dropDown(),
            'inscricao_documentos' => $this->inscricao_documento_repository->findByInscricao($request->session()->get('usuario')['inscricao']['id'])

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentoRequest $request)
    {
        $resultado = $this->inscricao_documento_repository->addInscricaoDocumento($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Documento cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o documento. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('participante.documento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, InscricaoDocumento $documento)
    {
        $resultado = $this->inscricao_documento_repository->deleteInscricaoDocumento($documento->id);

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Documento deletado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao deletar o documento. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('participante.documento');
    }
}
