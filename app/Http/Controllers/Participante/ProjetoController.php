<?php

namespace App\Http\Controllers\Participante;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Participante\ProjetoRequest;
use App\Repositories\ProjetoRepository;
use Illuminate\Support\Facades\Storage;
use App\Models\Projeto;

class ProjetoController extends Controller
{
    protected $projeto_repository;

    public function __construct(ProjetoRepository $projeto)
    {
        $this->projeto_repository = $projeto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $inscricao_modalidades = $request->session()->get('usuario')['inscricao']->inscricao_modalidades;

        $modalidades = [];
        foreach ($inscricao_modalidades as $inscricao_modalidade) {
            $modalidades[$inscricao_modalidade->modalidade->id] = $inscricao_modalidade->modalidade->nome;
        }

        $modalidades = array(""=>'Escolha a opção') + $modalidades;

        asort($modalidades);

        //array_unshift($modalidades,'Escolha a opção');


        return view('participante.projeto.create',[
            'evento' => $request->session()->get('usuario')['evento'],
            'inscricao' => $request->session()->get('usuario')['inscricao'],
            'modalidades' => $modalidades,
            'projetos' => $this->projeto_repository->findByInscricao($request->session()->get('usuario')['inscricao']['id'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjetoRequest $request)
    {
        $resultado = $this->projeto_repository->addProjeto($request->except(['_token']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Projeto cadastrado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao cadastrar o projeto. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('participante.projeto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Projeto $projeto)
    {
        $resultado = $this->projeto_repository->deleteProjeto($projeto->id);

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Projeto deletado com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao deletar o projeto. '.$resultado,'color'=>'error']);
        }

        return redirect()->route('participante.projeto');
    }
}
