<?php

namespace App\Http\Controllers\Participante;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Participante\PasswordRequest;
use App\Repositories\InscricaoRepository;

class PasswordController extends Controller
{
    protected $inscricao_repository;

    public function __construct(
        InscricaoRepository $inscricao
    )
    {
        $this->inscricao_repository = $inscricao;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return view('participante.password.edit',[
            'evento' => $request->session()->get('usuario')['evento'],
            'inscricao' => $request->session()->get('usuario')['inscricao']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordRequest $request)
    {
        $resultado = $this->inscricao_repository->edit(session()->get('usuario')['inscricao']['id'],$request->except(['_token','senha_confirmar']));

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Senha alterada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao alterar a senha. '.$resultado,'color'=>'error']);
        }
            
        return redirect()->route('participante.password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
