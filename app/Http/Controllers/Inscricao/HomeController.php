<?php

namespace App\Http\Controllers\Inscricao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Inscricao\InscricaoRequest;
use App\Repositories\ModalidadeRepository;
use App\Repositories\PorteRepository;
use App\Repositories\EventoRepository;
use App\Repositories\InscricaoRepository;

class HomeController extends Controller
{
    protected $evento_repository;
    protected $modalidade_repository;
    protected $porte_repository;
    protected $inscricao_repository;

    public function __construct(
        ModalidadeRepository $modalidade, 
        PorteRepository $porte,
        EventoRepository $evento,
        InscricaoRepository $inscricao
    )
    {
        $this->evento_repository = $evento;
        $this->modalidade_repository = $modalidade;
        $this->porte_repository = $porte;
        $this->inscricao_repository = $inscricao;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($evento_token)
    {
        $evento = $this->evento_repository->findByToken($evento_token);

        if (count($evento) > 0) {
            return view('inscricao.create',[
                'evento' => $evento,
                'evento_token' => $evento_token,
                'modalidades' => $this->modalidade_repository->findAll(),
                'portes' => $this->porte_repository->dropDown()
            ]);
        } else {
            echo 'Link inválido';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InscricaoRequest $request, $evento_token)
    {
        $evento = $this->evento_repository->findByToken($evento_token);

        if (count($evento) > 0) {
            $resultado = $this->inscricao_repository->addInscricao($evento->id,$request->except(['_token','senha_confirmar']));

            if ($resultado === TRUE) {
                $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Inscrição realizada com sucesso.','color'=>'success']);
            } else {        
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao realizar a inscrição. '.$resultado,'color'=>'error']);
            }
                
            return redirect()->route('inscricao.create',['evento_token'=>$evento_token]);
        } else {
            echo 'Token inválido';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
