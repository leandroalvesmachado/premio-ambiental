<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Repositories\UsuarioRepository;
use App\Repositories\InscricaoRepository;
use App\Repositories\AvaliadorRepository;
use Session;

class LoginController extends Controller
{
    protected $usuario_repository;
    protected $inscricao_repository;
    protected $avaliador_repository;

    public function __construct(UsuarioRepository $user, 
        InscricaoRepository $inscricao,
        AvaliadorRepository $avaliador
    )
    {
        $this->usuario_repository = $user;
        $this->inscricao_repository = $inscricao;
        $this->avaliador_repository = $avaliador;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginRequest $request)
    {
        if ($request->get('perfil') == 1) {
            $usuario = $this->usuario_repository->login($request->except('token'));
        }
        if ($request->get('perfil') == 2) {
            $usuario = $this->inscricao_repository->login($request->except('token'));
        }
        if ($request->get('perfil') == 3) {
            $usuario = $this->avaliador_repository->login($request->except('token'));
        }

        if (count($usuario) > 0) {
            if ($request->get('perfil') == 1) {
                $data = array(
                    'id' => $usuario->id,
                    'inscricao_id' => $usuario->inscricao_id,
                    'login' => $usuario->login,
                    'admin' => $usuario->admin,
                    'status' => $usuario->status,
                    'ultimo_acesso' => $usuario->ultimo_acesso,
                    'logged' => true,
                    'profile' => $request->get('perfil')
                );
            }
            if ($request->get('perfil') == 2) {
                $data = [
                    'id' => $usuario->id,
                    'evento' => $usuario->evento,
                    'inscricao' => $usuario,
                    'profile' => $request->get('perfil')
                ];
            }
            if ($request->get('perfil') == 3) {
                $data = [
                    'id' => $usuario->id,
                    'avaliador' => $usuario,
                    'evento' => $usuario->evento,
                    'profile' => $request->get('perfil')
                ];
            }

            Session::put('usuario',$data);
            
            if (count(Session::get('usuario') > 0)) {
                if ($request->get('perfil') == 1) {
                    return redirect()->route('admin.index');
                }
                if ($request->get('perfil') == 2) {
                    return redirect()->route('participante.index');
                }
                if ($request->get('perfil') == 3) {
                    return redirect()->route('avaliacao.index');
                }
            } else {
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Não foi possível efetuar o login.','color'=>'danger']);
                return redirect()->route('login.create');
            }
        } else {
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Login/Senha incorreto.','color'=>'danger']);
            return redirect()->route('login.create');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $usuario = $this->usuario_repository->logout($request->session()->get('usuario')['id']);

            if ($usuario === TRUE) {
                $request->session()->forget('usuario');
                return redirect()->route('login.create');
            } else {
                $request->session()->flash('message',['title'=>'Erro','msg'=>'Não foi possível efetuar o logout.','color'=>'error']);
                return redirect()->route('admin.index');
            }
        } catch (Exception $e) {
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Não foi possível efetuar o logout.','color'=>'error']);
            return redirect()->route('admin.index');
        }
    }
}
