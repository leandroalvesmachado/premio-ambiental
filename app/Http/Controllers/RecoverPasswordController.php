<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RecoveryPasswordRequest;
use App\Repositories\UsuarioRepository;
use App\Repositories\InscricaoRepository;
use App\Repositories\AvaliadorRepository;

class RecoverPasswordController extends Controller
{
    protected $usuario_repository;
    protected $inscricao_repository;
    protected $avaliador_repository;

    public function __construct(UsuarioRepository $user, 
        InscricaoRepository $inscricao,
        AvaliadorRepository $avaliador
    )
    {
        $this->usuario_repository = $user;
        $this->inscricao_repository = $inscricao;
        $this->avaliador_repository = $avaliador;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recover-password');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecoveryPasswordRequest $request)
    {
        if ($request->get('perfil') == 1) {
            $resultado = $this->usuario_repository->recuperarSenha($request->except('token'));
        }
        if ($request->get('perfil') == 2) {
            $resultado = $this->inscricao_repository->recuperarSenha($request->except('token'));
        }
        if ($request->get('perfil') == 3) {
            $resultado = $this->avaliador_repository->recuperarSenha($request->except('token'));
        }

        if ($resultado === TRUE) {
            $request->session()->flash('message',['title'=>'Sucesso','msg'=>'Nova senha enviada com sucesso.','color'=>'success']);
        } else {        
            $request->session()->flash('message',['title'=>'Erro','msg'=>'Erro ao criar a nova senha. '.$resultado,'color'=>'danger']);
        }
            
        return redirect()->route('recover.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
