<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EventoRepository;

class HomeController extends Controller
{
    protected $evento_repository;

    public function __construct(
        EventoRepository $evento
    )
    {
        $this->evento_repository = $evento;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('login.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inscricao()
    {
        /* Último evento cadastrado */
        $evento = $this->evento_repository->findLast();

        return redirect()->route('inscricao.create',$evento->token);
    }

     public function edital()
    {

        return \Redirect::to('https://www1.sfiec.org.br/sites/numa/?st=exibir&id=88981');
    }
}
