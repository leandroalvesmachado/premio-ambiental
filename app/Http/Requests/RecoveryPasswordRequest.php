<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Validator;
use App\Repositories\UsuarioRepository;
use App\Repositories\InscricaoRepository;
use App\Repositories\AvaliadorRepository;

class RecoveryPasswordRequest extends FormRequest
{
    protected $usuario_repository;
    protected $inscricao_repository;
    protected $avaliador_repository;

    public function __construct(UsuarioRepository $user, 
        InscricaoRepository $inscricao,
        AvaliadorRepository $avaliador
    )
    {
        $this->usuario_repository = $user;
        $this->inscricao_repository = $inscricao;
        $this->avaliador_repository = $avaliador;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'recover.store' =>
            [
                'login' => 'required|validar_recuperar_senha',
                'email' => 'required|email',
                'perfil' => 'required'
            ]
        ];

        Validator::extend('validar_recuperar_senha', function($attribute, $value, $parameters) {
            $login = Request::get('login');
            $email = Request::get('email');
            $perfil = Request::get('perfil');

            if (!empty($login) && !empty($email) && !empty($perfil)) {
                if ($perfil == 1) {
                    $resultado = $this->usuario_repository->verificarLogin($login,$email);
                }
                if ($perfil == 2) {
                    $resultado = $this->inscricao_repository->verificarLogin($login,$email);
                }
                if ($perfil == 3) {
                    $resultado = $this->avaliador_repository->verificarLogin($login,$email);
                }

                if ($resultado) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'login.validar_recuperar_senha' => 'Usuário não encontrado. Verifique suas informações de login, email e perfil estão corretas.'
        ];
    }
}
