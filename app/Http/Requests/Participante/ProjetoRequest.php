<?php

namespace App\Http\Requests\Participante;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use App\Repositories\ProjetoRepository;

class ProjetoRequest extends FormRequest
{
    protected $projeto_repository;

    public function __construct(ProjetoRepository $projeto)
    {
        $this->projeto_repository = $projeto;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'participante.projeto' =>
            [
                'nome' => 'required|max:255',
                'modalidade_id' => 'modalidade_required|validar_modalidade',
                'responsavel_nome' => 'required|max:255',
                'responsavel_telefone' => 'required|max:255',
                'responsavel_email' => 'required|email|max:255',
                'arquivo' => 'required'
            ]
        ];

        Validator::extend('modalidade_required', function($attribute, $value, $parameters) {
            $modalidade = $value;

            if ($modalidade == 0) {
                return FALSE;
            }

            return TRUE;
        });

        Validator::extend('validar_modalidade', function($attribute, $value, $parameters) {
            $modalidade_id = $value;
            $inscricao_id = session()->get('usuario')['inscricao']['id'];

            $resultado = $this->projeto_repository->findProjetoAdicionado($inscricao_id,$modalidade_id);

            if (count($resultado) > 0) {
                return FALSE;
            }

            return TRUE;
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'modalidade_id.modalidade_required' => 'O campo MODALIDADE é obrigatório.',
            'modalidade_id.validar_modalidade' => 'Modalidade já possui projeto adicionado.'
        ];
    }
}
