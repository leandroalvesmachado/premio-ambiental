<?php

namespace App\Http\Requests\Participante;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use App\Repositories\DocumentoRepository;

use App\Repositories\InscricaoDocumentoRepository;

class DocumentoRequest extends FormRequest
{
    protected $documento_repository;
    protected $inscricao_documento_repository;

    public function __construct(DocumentoRepository $documento, InscricaoDocumentoRepository $inscricao_documento)
    {
        $this->documento_repository = $documento;
        $this->inscricao_documento_repository = $inscricao_documento;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'participante.documento' =>
            [
                'nome' => 'required|max:255',
                'documento_id' => 'required|validar_documento',
                'arquivo' => 'required'
            ]
        ];

        Validator::extend('validar_documento', function($attribute, $value, $parameters) {
            $documento_id = $value;
            $inscricao_id = session()->get('usuario')['inscricao']['id'];


            $resultado = $this->inscricao_documento_repository->findInscricaoDocumentoAdicionado($inscricao_id,$documento_id);

            if (count($resultado) > 0) {
                return FALSE;
            }

            return TRUE;
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'documento_id.validar_documento' => 'Documento já adicionado.'
        ];
    }
}
