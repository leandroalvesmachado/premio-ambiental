<?php

namespace App\Http\Requests\Avaliador;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Illuminate\Http\Request;
use App\Repositories\AvaliadorProjetoRepository;

class ProjetoRequest extends FormRequest
{
    protected $avaliador_projeto_repository;

    public function __construct(AvaliadorProjetoRepository $avaliador_projeto)
    {
        $this->avaliador_projeto_repository = $avaliador_projeto;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'avaliacao.store' =>
            [
                'nota' => 'required|valida_nota|valida_projeto_avaliado',
                'comentario' => 'max:255'
            ]
        ];

        Validator::extend('valida_nota', function($attribute, $value, $parameters) {
            $nota = $value;

            if ($nota >= 5 && $nota <= 10) {
                return TRUE;
            }

            return FALSE;
        });

        Validator::extend('valida_projeto_avaliado', function($attribute, $value, $parameters) {
            $nota = $value;
            $avaliador_id = session()->get('usuario')['avaliador']['id'];
            $projeto = Request::route()->parameter('projeto');

            $resultado = $this->avaliador_projeto_repository->projetoAvaliado($projeto->id,$avaliador_id);

            if (count($resultado) > 0) {
                return FALSE;
            }

            return TRUE;
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'nota.valida_nota' => 'NOTA inválida. A NOTA deve ficar entre 5 e 10.',
            'nota.valida_projeto_avaliado' => 'Projeto já avaliado.'
        ];
    }
}
