<?php

namespace App\Http\Requests\Avaliador;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'avaliacao.password' =>
            [
                'senha' => 'required|min:8|regex:/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
                'senha_confirmar' => 'required|min:8|same:senha',
            ]
        ];

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'senha.regex' => 'Sua senha deve conter 1 caractere minúsculo, 1 caractere maiúsculo e 1 número no mínimo.',
        ];
    }
}
