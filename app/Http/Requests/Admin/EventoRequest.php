<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Illuminate\Http\Request;

class EventoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'evento.store' =>
            [
                'nome' => 'required|max:255',
                'data_inicio' => 'required|date_format:d/m/Y|before:data_fim',
                'data_fim' => 'required|date_format:d/m/Y|after:data_inicio',
                'hora_inicio' => 'required|date_format:H:i|before:hora_fim',
                'hora_fim' => 'required|date_format:H:i|after:hora_inicio',
                'descricao' => 'required|max:1000',
                'modalidade' => 'validar_modalidade'
            ],
            'evento.update' =>
            [
                'nome' => 'required|max:255',
                'data_inicio' => 'required|date_format:d/m/Y|before:data_fim',
                'data_fim' => 'required|date_format:d/m/Y|after:data_inicio',
                'hora_inicio' => 'required|date_format:H:i|before:hora_fim',
                'hora_fim' => 'required|date_format:H:i|after:hora_inicio',
                'descricao' => 'required|max:1000',
                'modalidade' => 'validar_modalidade_edicao'
            ],
        ];

        // Validator::extend('validar_modalidade', function($attribute, $value, $parameters) {
        //     $modalidades = $value;

        //     if (!isset($modalidades)) {
        //         return FALSE;
        //     }

        //     if (count($modalidades) == 0) {
        //         return FALSE;
        //     }

        //     return TRUE;
        // });

        Validator::extend('validar_modalidade', function($attribute, $value, $parameters) {
            $modalidades = $value;

            if (count($modalidades) == 1) {
                return FALSE;
            }

            return TRUE;
        });

        Validator::extend('validar_modalidade_edicao', function($attribute, $value, $parameters) {
            $modalidades = $value;

            if (count($modalidades) == 0) {
                return TRUE;
            }

            return TRUE;
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'modalidade.validar_modalidade' => 'Escolha no mínimo 1 modalidade.'
        ];
    }
}
