<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ModalidadeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'modalidade.store' =>
            [
                'nome' => 'required|max:255',
                'descricao' => 'required|max:255'
            ],
            'modalidade.update' =>
            [
                'nome' => 'required|max:255',
                'descricao' => 'required|max:255'
            ],
        ];

        return $rules[\Route::getCurrentRoute()->getName()];
    }
}
