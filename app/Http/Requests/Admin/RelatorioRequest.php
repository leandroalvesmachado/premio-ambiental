<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RelatorioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'relatorio.create' =>
            [
                'evento_id' => 'required',
                'relatorio_id' => 'required'
            ]
        ];

        return $rules[\Route::getCurrentRoute()->getName()];
    }
}
