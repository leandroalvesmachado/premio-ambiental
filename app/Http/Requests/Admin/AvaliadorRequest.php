<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Validator;
use App\Repositories\AvaliadorRepository;

class AvaliadorRequest extends FormRequest
{
    protected $avaliador_repository;

    public function __construct(AvaliadorRepository $avaliador)
    {
        $this->avaliador_repository = $avaliador;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'avaliador.store' =>
            [
                'cpf' => 'required|validar_cpf',
                'nome' => 'required|max:255',
                'email' => 'required|email|max:255',
                'telefone' => 'required',
                'celular' => 'required',
                'cnpj' => 'required',
                'cargo' => 'required|max:255',
                'nome_fantasia' => 'required|max:255',
                'razao_social' => 'required|max:255',
                'cep_empresa' => 'required',
                'endereco_empresa' => 'required|max:255',
                'estado_empresa' => 'required|max:255',
                'municipio_empresa' => 'required|max:255',
                'bairro_empresa' => 'required|max:255',
                'evento_id' => 'required|validar_avaliador'
            ],
            'avaliador.update' =>
            [
                'cpf' => 'required|validar_cpf',
                'nome' => 'required|max:255',
                'email' => 'required|email|max:255',
                'telefone' => 'required',
                'celular' => 'required',
                'cnpj' => 'required',
                'cargo' => 'required|max:255',
                'nome_fantasia' => 'required|max:255',
                'razao_social' => 'required|max:255',
                'cep_empresa' => 'required',
                'endereco_empresa' => 'required|max:255',
                'estado_empresa' => 'required|max:255',
                'municipio_empresa' => 'required|max:255',
                'bairro_empresa' => 'required|max:255',
                'evento_id' => 'required|validar_avaliador_edicao'
            ]
        ];

        Validator::extend('validar_cpf', function($attribute, $value, $parameters) {
            $cpf = str_pad ( preg_replace ( '/[^0-9]/', '', $value ), 11, '0', STR_PAD_LEFT );
            // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
            if (strlen ( $cpf ) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || 
                    $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || 
                    $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
                return FALSE;
            } else { // Calcula os números para verificar se o CPF é verdadeiro
                for($t = 9; $t < 11; $t ++) {
                    for($d = 0, $c = 0; $c < $t; $c ++) {
                        $d += $cpf {$c} * (($t + 1) - $c);
                    }
                    $d = ((10 * $d) % 11) % 10;
                    if ($cpf {$c} != $d) {
                        return FALSE;
                    }
                }
                return TRUE;
            }
        });

        Validator::extend('validar_avaliador', function($attribute, $value, $parameters) {
            $evento_id = $value;
            $cpf = str_pad(preg_replace( '/[^0-9]/','', Request::get('cpf')),11,'0',STR_PAD_LEFT);

            $resultado = $this->avaliador_repository->validarAvalidorEvento($evento_id, $cpf);

            if (count($resultado) > 0) {
                return FALSE;
            }

            return TRUE;
        });

        Validator::extend('validar_avaliador_edicao', function($attribute, $value, $parameters) {
            $evento_id = $value;
            $cpf = str_pad(preg_replace( '/[^0-9]/','', Request::get('cpf')),11,'0',STR_PAD_LEFT);
            $avaliador = Request::route()->parameter('avaliador');

            $resultado = $this->avaliador_repository->validarAvalidorEventoEdicao($evento_id, $cpf, $avaliador->id);

            if (!$resultado) {
                return FALSE;
            }

            return TRUE;
        });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'cpf.validar_cpf' => 'CPF inválido.',
            'evento_id.validar_avaliador' => 'Avaliador já adicionado com este CPF para este evento.',
            'evento_id.validar_avaliador_edicao' => 'Avaliador já adicionado com este CPF para este evento.'
        ];
    }
}
