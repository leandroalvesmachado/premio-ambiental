<?php

namespace App\Http\Requests\Inscricao;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Validator;
use App\Repositories\EventoRepository;
use App\Repositories\InscricaoRepository;

class InscricaoRequest extends FormRequest
{
    protected $evento_repository;
    protected $inscricao_repository;

    public function __construct(EventoRepository $evento, InscricaoRepository $inscricao)
    {
        $this->evento_repository = $evento;
        $this->inscricao_repository = $inscricao;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'inscricao.store' =>
            [
                'cpf' => 'required|validar_cpf',
                'nome' => 'required|max:255',
                'endereco' => 'required|max:255',
                'email' => 'required|email|max:255',
                'telefone' => 'required',
                'celular' => 'required',
                'cnpj' => 'required',
                'nome_fantasia' => 'required|max:255',
                'razao_social' => 'required|max:255',
                'cep_empresa' => 'required',
                'endereco_empresa' => 'required|max:255',
                'modalidade_id' => 'validar_modalidade',
                'representante' => 'required|max:255',
                'representante_cargo' => 'required|max:255',
                'porte_id' => 'required',
                'estado_empresa' => 'required|max:255',
                'municipio_empresa' => 'required|max:255',
                'bairro_empresa' => 'required|max:255',
                //'login' => 'required|min:8|validar_login',
                'senha' => 'required|min:8|regex:/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
                'senha_confirmar' => 'required|min:8|same:senha',
            ]
        ];

        Validator::extend('validar_cpf', function($attribute, $value, $parameters) {
            $cpf = str_pad ( preg_replace ( '/[^0-9]/', '', $value ), 11, '0', STR_PAD_LEFT );
            // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
            if (strlen ( $cpf ) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || 
                    $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || 
                    $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
                return FALSE;
            } else { // Calcula os números para verificar se o CPF é verdadeiro
                for($t = 9; $t < 11; $t ++) {
                    for($d = 0, $c = 0; $c < $t; $c ++) {
                        $d += $cpf {$c} * (($t + 1) - $c);
                    }
                    $d = ((10 * $d) % 11) % 10;
                    if ($cpf {$c} != $d) {
                        return FALSE;
                    }
                }
                return TRUE;
            }
        });

        Validator::extend('validar_modalidade', function($attribute, $value, $parameters) {
            $token = Request::route()->parameter('evento_token');
            $modalidades = $value;

            if (count($modalidades) == 1) {
                return FALSE;
            }

            return TRUE;
        });

        // Validator::extend('validar_login', function($attribute, $value, $parameters) {
        //     $token = Request::route()->parameter('evento_token');
        //     $evento = $this->evento_repository->findByToken($token);
        //     $login = $value;

        //     $resultado = $this->inscricao_repository->findByFields([
        //         'evento_id' => $evento->id,
        //         'login' => $login
        //     ]);

        //     var_dump($resultado);
        //     exit();

        //     if (count($resultado) > 0) {
        //         return FALSE;
        //     }

        //     return TRUE;
        // });

        return $rules[\Route::getCurrentRoute()->getName()];
    }

    public function messages()
    {
        return [
            'cpf.validar_cpf' => 'CPF inválido.',
            'modalidade_id.validar_modalidade' => 'Escolha no mínimo uma modalidade.',
            'login.validar_login' => 'LOGIN já existe.',
            'senha.regex' => 'Sua senha deve conter 1 caractere minúsculo, 1 caractere maiúsculo e 1 número no mínimo.',
        ];
    }
}
