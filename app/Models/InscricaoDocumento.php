<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class InscricaoDocumento extends Model
{
	use SoftDeletes;

	protected $primaryKey = 'id';
    protected $table = 'inscricao_documento';
	public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->login_cadastro = session()->get('usuario')['id'];
    }

    public function inscricao()
    {
        return $this->belongsTo('App\Models\Inscricao','inscricao_id');
    }

    public function documento()
    {
        return $this->belongsTo('App\Models\Documento','documento_id');
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento','evento_id');
    }
}
