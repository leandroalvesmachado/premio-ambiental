<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Projeto extends Model
{
	use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'projeto';
    public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->login_cadastro = session()->get('usuario')['id'];
        //$this->inscricao_id = session()->get('usuario')['inscricao']['id'];
        //$this->evento_id = session()->get('usuario')['evento']['id'];
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento','evento_id');
    }

    public function modalidade()
    {
        return $this->belongsTo('App\Models\Modalidade','modalidade_id');
    }

    public function inscricao()
    {
        return $this->belongsTo('App\Models\Inscricao','inscricao_id');
    }

    public function avaliador_projeto()
    {
        return $this->hasMany('App\Models\AvaliadorProjeto','projeto_id','id');
    }

}
