<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AvaliadorProjeto extends Model
{
	use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'avaliador_projeto';
    public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function projeto()
    {
        return $this->belongsTo('App\Models\Projeto','projeto_id');
    }

    public function avaliador()
    {
        return $this->belongsTo('App\Models\Avaliador','avaliador_id');
    }

}
