<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class EventoModalidade extends Model
{
    use SoftDeletes;

	protected $primaryKey = 'id';
    protected $table = 'evento_modalidade';
	public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->login_cadastro = session()->get('usuario')['id'];
    }

    public function fill(array $attributes)
    {   
        parent::fill($attributes);
        $this->login_alteracao = session()->get('usuario')['id'];
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento','evento_id');
    }

    public function modalidade()
    {
        return $this->belongsTo('App\Models\Modalidade','modalidade_id');
    }
}
