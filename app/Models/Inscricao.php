<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Inscricao extends Model
{
	use SoftDeletes;

	protected $primaryKey = 'id';
    protected $table = 'inscricao';
	public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function fill(array $attributes)
    {   
        parent::fill($attributes);
        $this->login_alteracao = session()->get('usuario')['id'];
    }

    public function setCpfAttribute($value)
    {
        $this->attributes['cpf'] = preg_replace('/[^0-9]/','',$value);
    }

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = preg_replace('/[^0-9]/','',$value);
    }

    public function setSenhaAttribute($value)
    {
        $this->attributes['senha'] = md5($value);
    }

    public function porte()
    {
        return $this->belongsTo('App\Models\Porte','porte_id');
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento','evento_id');
    }

    public function inscricao_modalidades()
    {
        return $this->hasMany('App\Models\InscricaoModalidade','inscricao_id','id');
    }

    public function projetos()
    {
        return $this->hasMany('App\Models\Projeto','inscricao_id','id');
    }

    public function documentos()
    {
        return $this->hasMany('App\Models\InscricaoDocumento','inscricao_id','id');
    }
}
