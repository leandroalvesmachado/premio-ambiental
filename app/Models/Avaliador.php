<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Avaliador extends Model
{
	use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'avaliador';
    public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->login_cadastro = session()->get('usuario')['id'];
    }

    public function fill(array $attributes)
    {   
        parent::fill($attributes);
        $this->login_alteracao = session()->get('usuario')['id'];
    }

    public function setCpfAttribute($value)
    {
        $this->attributes['cpf'] = preg_replace('/[^0-9]/','',$value);
    }

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = preg_replace('/[^0-9]/','',$value);
    }

    public function setSenhaAttribute($value)
    {
        $this->attributes['senha'] = md5($value);
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento','evento_id');
    }

}
