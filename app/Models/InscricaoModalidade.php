<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class InscricaoModalidade extends Model
{
	use SoftDeletes;

	protected $primaryKey = 'id';
    protected $table = 'inscricao_modalidade';
	public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function inscricao()
    {
        return $this->belongsTo('App\Models\Inscricao','inscricao_id');
    }

    public function modalidade()
    {
        return $this->belongsTo('App\Models\Modalidade','modalidade_id');
    }
}
