<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Evento extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'id';
    protected $table = 'evento';
    public $timestamps = true;
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['deleted_at'];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);
        $this->login_cadastro = session()->get('usuario')['id'];
    }

    public function fill(array $attributes)
    {   
        parent::fill($attributes);
        $this->login_alteracao = session()->get('usuario')['id'];
    }

    public function getDataInicioAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataInicioAttribute($value)
    {
        $data = explode('/',$value);
        $this->attributes['data_inicio'] = $data['2'].'-'.$data['1'].'-'.$data['0'].' 00:00:00';
    }

    public function getDataFimAttribute($value)
    {
       return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataFimAttribute($value)
    {
        $data = explode('/',$value);
        $this->attributes['data_fim'] = $data['2'].'-'.$data['1'].'-'.$data['0'].' 00:00:00';
    }

    public function modalidades()
    {
        return $this->hasMany('App\Models\EventoModalidade','evento_id','id');
    }
}
