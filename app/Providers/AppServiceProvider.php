<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::share('perfil',[
            ''=>'Escolha o perfil',
            '1'=>'Administrador',
            '3' => 'Avaliador',
            '2'=>'Participante'
        ]);

        View::share('relatorios',[
            ''=>'Escolha o relatório',
            '1'=>'Modelo 1 - Todas as inscrições realizadas no evento',
            '2' => 'Modelo 2 - Todas os projetos cadastrados no evento',
            '3' => 'Modelo 3 - Todas os projetos avaliados no evento',
            '4' => 'Modelo 4 - Todas as inscrições realizadas no evento (versão simplificada)',
            '5' => 'Modelo 5 - Relação das empresas inscritas (com seus contatos), o porte, a modalidade e se já submeteram seus projetos'
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
