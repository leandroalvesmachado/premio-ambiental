<?php

namespace App\Mail\Inscricao;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NovaInscricao extends Mailable
{
    use Queueable, SerializesModels;

    protected $input;
    protected $senha;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input, $senha)
    {
        $this->input = $input;
        $this->senha = $senha;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('inscricao.email.nova-inscricao')->with([
            'input' => $this->input,
            'senha' => $this->senha
        ]);
    }
}
