<?php

namespace App\Mail\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvaliadorAtivo extends Mailable
{
    use Queueable, SerializesModels;

    protected $input;
    protected $senha;
    protected $evento;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input, $senha, $evento)
    {
        $this->input = $input;
        $this->senha = $senha;
        $this->evento = $evento;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.email.avaliador-ativo')->with([
            'input' => $this->input,
            'senha' => $this->senha,
            'evento' => $this->evento
        ]);
    }
}
