<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NovaSenha extends Mailable
{
    use Queueable, SerializesModels;

    protected $input;
    protected $senha;
    protected $object;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input, $senha, $object)
    {
        $this->input = $input;
        $this->senha = $senha;
        $this->object = $object;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.nova-senha')->with([
            'input' => $this->input,
            'senha' => $this->senha,
            'object' => $this->object
        ]);
    }
}
