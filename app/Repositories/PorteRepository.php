<?php

namespace App\Repositories;

use App\Models\Porte;
use Exception;
use DB;

class PorteRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Porte $model)
	{
		$this->model = $model;
	}

	public function dropDown()
	{
		try {
			return $this->model->all()->sortBy('nome')->pluck('nome','id')->prepend('Escolha a opção','');
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}
}