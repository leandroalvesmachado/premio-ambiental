<?php

namespace App\Repositories;

use App\Models\Avaliador;
use Exception;
use DB;
use App\Mail\Admin\AvaliadorAtivo;
use Mail;

class AvaliadorRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Avaliador $model)
	{
		$this->model = $model;
	}

	public function login($input) 
  	{
        try {  
            $query = $this->model->query();
            $query->where('login','=',$input['login']);
            $query->where('senha',md5($input['senha']));
            $query->where('status','=','1');

			return $query->get()->first();
  		} catch (Exception $e) {
  			return [];
  		}
  	}

	public function addAvaliador($data)
	{
		try {
			DB::beginTransaction();

			$senha = mt_rand();

    		$avaliador = new $this->model($data);
	    	$avaliador->senha = $senha;
    		$avaliador->save();

    		$avaliador_login = $this->model->find($avaliador->id);
			$avaliador_login->login = str_pad($avaliador->id,10,0,STR_PAD_LEFT);
			$avaliador_login->save();

			DB::commit();

            return TRUE;
		} catch (Exception $e) {
			DB::rollBack();
			return $e->getMessage();
		}
	}

	public function validarAvalidorEvento($evento_id, $cpf)
	{
		try {
			$query = $this->model->query();
			$query->where('evento_id',$evento_id);
			$query->where('cpf',$cpf);

			return $query->get()->first();
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}

	public function validarAvalidorEventoEdicao($evento_id, $cpf, $avaliador_id)
	{
		try {
			$query = $this->model->query();
			$query->where('evento_id',$evento_id);
			$query->where('cpf',$cpf);
			$query->where('id',$avaliador_id);

			if (count($query->get()->first()) > 0) {
				return TRUE;
			} else {
				$query = $this->model->query();
				$query->where('evento_id',$evento_id);
				$query->where('cpf',$cpf);

				if (count($query->get()->first()) > 0) {
					return FALSE;
				} else {
					return TRUE;
				}
			}

		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}

	public function status($id)
	{
		try {
			DB::beginTransaction();

			$avaliador = $this->model->find($id);
			
			if ($avaliador->status == 0) {
				$avaliador->status = 1;
			} else {
				$avaliador->status = 0;
			}

			$avaliador->save();

			$avaliador_ativado = $this->model->find($avaliador->id);

			if ($avaliador_ativado->status == 1) {
				$senha = mt_rand();
				$avaliador_ativado->senha = $senha;
    			$avaliador_ativado->save();

    			Mail::to($avaliador_ativado->email,$avaliador_ativado->nome)
	            ->bcc(['lamachado@sfiec.org.br'])
	            ->send(new AvaliadorAtivo($avaliador_ativado,$senha,$avaliador_ativado->evento));
			}
			
			DB::commit();

            return TRUE;
		} catch (Exception $e) {
			DB::rollBack();
			return $e->getMessage();
		}
	}
}