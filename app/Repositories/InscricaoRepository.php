<?php

namespace App\Repositories;

use App\Models\Inscricao;
use App\Models\InscricaoModalidade;
use Exception;
use DB;
use App\Mail\Admin\NovaInscricao as NovaInscricaoAdmin;
use App\Mail\Inscricao\NovaInscricao as NovaInscricaoParticipante;
use Mail;

class InscricaoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Inscricao $model)
	{
		$this->model = $model;
	}

	public function login($input) 
  	{
        try {  
            $query = $this->model->query();
            $query->where('login','=',$input['login']);
            $query->where('senha',md5($input['senha']));
            $query->where('status','=','1');

			return $query->get()->first();
  		} catch (Exception $e) {
  			return [];
  		}
  	}

	public function addInscricao($evento_id, $data)
	{
		try {
			DB::beginTransaction();

			$modalidades = $data['modalidade_id'];
			$senha = $data['senha'];
			unset($data['modalidade_id']);

			$inscricao = new $this->model($data);
			$inscricao->evento_id = $evento_id;
			$inscricao->save();
			
			$inscricao_login = $this->model->find($inscricao->id);
			$inscricao_login->login = str_pad($inscricao->id,10,0,STR_PAD_LEFT);
			$inscricao_login->save();

			foreach ($modalidades as $modalidade) {
				if ($modalidade != 0) {
					$inscricao_modalidade = new InscricaoModalidade();
					$inscricao_modalidade->inscricao_id = $inscricao->id;
					$inscricao_modalidade->modalidade_id = $modalidade;
					$inscricao_modalidade->save();
				}
			}

			$inscricao_completa = $this->model->find($inscricao->id);
			
			DB::commit();

			Mail::to('lamachado@sfiec.org.br','Leandro Alves Machado')
            ->bcc(['lamachado@sfiec.org.br'])
            ->send(new NovaInscricaoAdmin($inscricao_completa));

            Mail::to($inscricao->email,$inscricao->nome)
            ->bcc(['lamachado@sfiec.org.br'])
            ->send(new NovaInscricaoParticipante($inscricao_completa,$senha));

            return TRUE;
		} catch (Exception $e) {
			DB::rollBack();
			return $e->getMessage();
		}
	}

	public function status($id)
	{
		try {
			DB::beginTransaction();

			$inscricao = $this->model->find($id);
			
			if ($inscricao->status == 0) {
				$inscricao->status = 1;
			} else {
				$inscricao->status = 0;
			}

			$inscricao->save();
			
			DB::commit();

            return TRUE;
		} catch (Exception $e) {
			DB::rollBack();
			return $e->getMessage();
		}
	}

	public function inscricaoPorPorte($evento_id)
	{
		try {
			$query = $this->model->query();
			$query->select(DB::raw("
				count(*) as quantidade,
				porte.nome
			"));
			$query->join('porte','porte.id','=','inscricao.porte_id');
			$query->where('inscricao.evento_id',$evento_id);
			$query->groupBy(DB::raw('inscricao.porte_id'));
			$query->orderBy('porte.nome');

			return $query->get();
		} catch (Exception $e) {
			return [];
		}
	}
}