<?php

namespace App\Repositories;

use App\Models\EventoModalidade;
use Exception;
use DB;

class EventoModalidadeRepository extends BaseRepository 
{
	protected $model;

	public function __construct(EventoModalidade $model)
	{
		$this->model = $model;
	}
}