<?php

namespace App\Repositories;

use App\Models\Modalidade;
use Exception;
use DB;

class ModalidadeRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Modalidade $model)
	{
		$this->model = $model;
	}


    public function findAll()
    {
        try {
            $query = $this->model->query();
            $query->orderBy('nome','ASC');

            return $query->get();
        } catch (Exception $e) {
            return [];
        }
    }

	public function dropDown()
	{
		try {
			return $this->model->all()->sortBy('nome')->pluck('nome','id')->prepend('Escolha a opção','');
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}
}