<?php

namespace App\Repositories;

use App\Models\Usuario;
use Exception;
use DB;

class UsuarioRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Usuario $model)
	{
		$this->model = $model;
	}

	public function login($input) 
  	{
        try {  
            $query = $this->model->query();
            $query->where('login',$input['login']);
            $query->where('senha',md5($input['senha']));

			return $query->get()->first();
  		} catch (Exception $e) {
  			return [];
  		}
  	}

    public function logout($id)
    {
        try {
            $object = $this->model->find($id);
            $object->ultimo_acesso = date('Y-m-d H:i:s');
            $object->save();

            return TRUE;
        } catch (Exception $e) {
            return FALSE;            
        }
    }
}