<?php

namespace App\Repositories;

use App\Models\AvaliadorProjeto;
use Exception;
use DB;

class AvaliadorProjetoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(AvaliadorProjeto $model)
	{
		$this->model = $model;
	}

	public function addNota($projeto, $data)
	{
		try {
            DB::beginTransaction();
            
            $object = new $this->model($data);
			$object->projeto_id = $projeto->id;
			$object->avaliador_id = session()->get('usuario')['avaliador']['id'];
			$object->login_cadastro = session()->get('usuario')['avaliador']['id'];
    		$object->save();

            DB::commit();

            return TRUE;
        } catch(Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
	}

	public function projetoAvaliado($projeto_id,$avaliador_id)
	{
		try {
			$query = $this->model->query();
            $query->where('avaliador_id',$avaliador_id);
            $query->where('projeto_id',$projeto_id);

			return $query->get()->first();
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}

}