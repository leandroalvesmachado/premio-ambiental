<?php

namespace App\Repositories;
use Exception;
use DB;
use Carbon\Carbon;
use App\Mail\NovaSenha;
use Mail;

class BaseRepository 
{
	protected $model;

	public function add($data)
    {
    	try {
    		$object = new $this->model($data);
    		$object->save();

    		return TRUE;
        } catch(Exception $e) {
			return $e->getMessage();
		}
    }

    public function edit($id, $data)
    {
        try {
            DB::beginTransaction();
            
            $object = $this->model->find($id);
            $object->fill($data);
            $object->save();

            DB::commit();

            return TRUE;
        } catch(Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function paginate($paginate = 10, $orderBy, $sort)
    {
        try {
            $query = $this->model->query();
            $query->orderBy($orderBy, $sort);

            return $query->paginate($paginate);
        } catch(Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function paginateWhere($paginate = 10, $orderBy, $sort, $columns)
    {
        try {
            $query = $this->model->query();

            if (count($columns) > 0) {
                foreach ($columns as $key => $value) {
                    if ($value != "") {
                        $query->where($key,'like', '%'.$value.'%');
                    }
                }
            }

            return $query->paginate($paginate);
        } catch(Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function find($id)
    {
        try {
            return $this->model->find($id);
        } catch(Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function findByFields($columns)
    {
        try {
            $query = $this->model->query();
            
            foreach ($columns as $key => $value) {
                $query->where($key,$value);
            }

            return $query->get()->first();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function verificarLogin($login,$email)
    {
        try {
            $query = $this->model->query();
            $query->where('login',$login);
            $query->where('email',$email);

            if (count($query->get()->first()) > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function recuperarSenha($data)
    {
        try {
            DB::beginTransaction();

            $query = $this->model->query();
            $query->where('login',$data['login']);
            $query->where('email',$data['email']);

            if (count($query->get()->first()) > 0) {
                $input = [];
                $senha = mt_rand();
                $input['senha'] = $senha;

                $object = $query->get()->first();
                $object->senha = md5($senha);
                $object->save();

                if ($data['perfil'] == 1) {
                    $input['perfil'] = 'ADMINISTRADOR';
                }
                if ($data['perfil'] == 2) {
                    $input['perfil'] = 'PARTICIPANTE';
                }
                if ($data['perfil'] == 3) {
                    $input['perfil'] = 'AVALIADOR';
                }

                Mail::to($object->email,$object->nome)
                ->bcc(['lamachado@sfiec.org.br'])
                ->send(new NovaSenha($input,$senha,$object));

                DB::commit();

                return TRUE;
            } else {
                DB::rollBack();
                return FALSE;
            }
        } catch (Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
}