<?php

namespace App\Repositories;

use App\Models\InscricaoModalidade;
use Exception;
use DB;

class InscricaoModalidadeRepository extends BaseRepository 
{
	protected $model;

	public function __construct(InscricaoModalidade $model)
	{
		$this->model = $model;
	}

	public function inscricaoPorModalidade($evento_id)
	{
		try {
			$query = $this->model->query();
			$query->select(DB::raw("
				count(*) as quantidade,
				modalidade.nome
			"));
			$query->join('modalidade','modalidade.id','=','inscricao_modalidade.modalidade_id');
			$query->join('inscricao','inscricao.id','=','inscricao_modalidade.inscricao_id');
			$query->where('inscricao.evento_id',$evento_id);
			$query->groupBy(DB::raw('inscricao_modalidade.modalidade_id'));
			$query->orderBy('modalidade.nome');

			return $query->get();
		} catch (Exception $e) {
			return [];
		}
	}
}