<?php

namespace App\Repositories;

use App\Models\Documento;
use Exception;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class DocumentoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Documento $model)
	{
		$this->model = $model;
	}

	public function dropDown()
	{
		try {
			return $this->model->all()->sortBy('nome')->pluck('nome','id')->prepend('Escolha a opção','');
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}
}