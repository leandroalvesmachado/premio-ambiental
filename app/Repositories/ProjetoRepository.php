<?php

namespace App\Repositories;

use App\Models\Projeto;
use Exception;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ProjetoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Projeto $model)
	{
		$this->model = $model;
	}

	public function addProjeto($data)
	{
		try {
    		$arquivo = $data['arquivo'];
			$arquivo_extensao = $data['arquivo']->getClientOriginalExtension();
			$arquivo_nome = Str::slug($data['nome']).'.'.$arquivo_extensao;

			$object = new $this->model($data);
			$object->arquivo = $arquivo_nome;
            $object->inscricao_id = session()->get('usuario')['inscricao']['id'];
            $object->evento_id = session()->get('usuario')['evento']['id'];
			$object->save();

			Storage::disk('local')->put('projetos/'.$object->id.'/'.$arquivo_nome, file_get_contents($arquivo->getRealPath()));

    		return TRUE;
        } catch(Exception $e) {
			return $e->getMessage();
		}
	}

	public function deleteProjeto($id)
	{
		try {
			$object = $this->model->find($id);
			Storage::delete('projetos/'.$object->id.'/'.$object->arquivo);
			$object->forceDelete();

			return TRUE;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function findByInscricao($inscricao_id)
    {
        try {
            $query = $this->model->query();
			$query->where('inscricao_id',$inscricao_id);

            return $query->get();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function paginateProjeto($paginate = 10)
    {
        try {
            $query = $this->model->query();
            $query->select('projeto.id as projeto_id',
                'projeto.inscricao_id as projeto_inscricao_id',
                'projeto.nome as projeto_nome',
                'projeto.modalidade_id as projeto_modalidade_id',
                'projeto.responsavel_nome as projeto_responsavel_nome',
                'projeto.responsavel_telefone as projeto_responsavel_telefone',
                'projeto.responsavel_email as projeto_responsavel_email',
                'projeto.arquivo as projeto_arquivo',
                'projeto.evento_id as projeto_evento_id',
                'projeto.created_at as projeto_created_at',
                'modalidade.nome as modalidade_nome',
                'evento.nome as evento_nome',
                'porte.nome as porte_nome',
                'inscricao.id as inscricao_id',
                'inscricao.cpf as inscricao_cpf',
                'inscricao.nome as inscricao_nome',
                'inscricao.endereco as inscricao_endereco',
                'inscricao.email as inscricao_email',
                'inscricao.email_alternativo as inscricao_email_alternativo',
                'inscricao.telefone as inscricao_telefone',
                'inscricao.celular as inscricao_celular',
                'inscricao.cnpj as inscricao_cnpj',
                'inscricao.nome_fantasia as inscricao_nome_fantasia',
                'inscricao.razao_social as inscricao_razao_social',
                'inscricao.representante as inscricao_representante',
                'inscricao.representante_cargo as inscricao_representante_cargo',
                'inscricao.cep_empresa as inscricao_cep_empresa',
                'inscricao.endereco_empresa as inscricao_endereco_empresa',
                'inscricao.estado_empresa as inscricao_estado_empresa',
                'inscricao.municipio_empresa as inscricao_municipio_empresa',
                'inscricao.complemento_empresa as inscricao_complemento_empresa'
            );
            $query->join('modalidade','modalidade.id','=','projeto.modalidade_id');
            $query->join('evento','evento.id','=','projeto.evento_id');
            $query->join('inscricao','inscricao.id','=','projeto.inscricao_id');
            $query->join('porte','porte.id','=','inscricao.porte_id');
            $query->orderBy('evento.nome','modalidade.nome','projeto.nome');

            return $query->paginate($paginate);
        } catch(Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function paginateWhereProjeto($paginate = 10, $columns)
    {
        try {
            $query = $this->model->query();
            $query->select('projeto.id as projeto_id',
                'projeto.inscricao_id as projeto_inscricao_id',
                'projeto.nome as projeto_nome',
                'projeto.modalidade_id as projeto_modalidade_id',
                'projeto.responsavel_nome as projeto_responsavel_nome',
                'projeto.responsavel_telefone as projeto_responsavel_telefone',
                'projeto.responsavel_email as projeto_responsavel_email',
                'projeto.arquivo as projeto_arquivo',
                'projeto.evento_id as projeto_evento_id',
                'projeto.created_at as projeto_created_at',
                'modalidade.nome as modalidade_nome',
                'evento.nome as evento_nome',
                'porte.nome as porte_nome',
                'inscricao.id as inscricao_id',
                'inscricao.cpf as inscricao_cpf',
                'inscricao.nome as inscricao_nome',
                'inscricao.endereco as inscricao_endereco',
                'inscricao.email as inscricao_email',
                'inscricao.email_alternativo as inscricao_email_alternativo',
                'inscricao.telefone as inscricao_telefone',
                'inscricao.celular as inscricao_celular',
                'inscricao.cnpj as inscricao_cnpj',
                'inscricao.nome_fantasia as inscricao_nome_fantasia',
                'inscricao.razao_social as inscricao_razao_social',
                'inscricao.representante as inscricao_representante',
                'inscricao.representante_cargo as inscricao_representante_cargo',
                'inscricao.cep_empresa as inscricao_cep_empresa',
                'inscricao.endereco_empresa as inscricao_endereco_empresa',
                'inscricao.estado_empresa as inscricao_estado_empresa',
                'inscricao.municipio_empresa as inscricao_municipio_empresa',
                'inscricao.complemento_empresa as inscricao_complemento_empresa'
            );
            $query->join('modalidade','modalidade.id','=','projeto.modalidade_id');
            $query->join('evento','evento.id','=','projeto.evento_id');
            $query->join('inscricao','inscricao.id','=','projeto.inscricao_id');
            $query->join('porte','porte.id','=','inscricao.porte_id');
            $query->orderBy('evento.nome','modalidade.nome','projeto.nome');


            if (count($columns) > 0) {
                if (!empty($columns['modalidade_id'])) {
                    $query->where('projeto.modalidade_id',$columns['modalidade_id']);
                }
                if (!empty($columns['evento_id'])) {
                    $query->where('projeto.evento_id',$columns['evento_id']);
                }
                if (!empty($columns['porte_id'])) {
                    $query->where('inscricao.porte_id',$columns['porte_id']);
                }
                if (!empty($columns['nome'])) {
                    $query->where('projeto.nome','like','%'.$columns['nome'].'%');
                }
            }

            return $query->paginate($paginate);
        } catch(Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function avaliadorProjetos($evento_id)
    {
        try {
            $query = $this->model->query();
            $query->select('projeto.id as projeto_id',
                'projeto.inscricao_id as projeto_inscricao_id',
                'projeto.nome as projeto_nome',
                'projeto.modalidade_id as projeto_modalidade_id',
                'projeto.responsavel_nome as projeto_responsavel_nome',
                'projeto.responsavel_telefone as projeto_responsavel_telefone',
                'projeto.responsavel_email as projeto_responsavel_email',
                'projeto.arquivo as projeto_arquivo',
                'projeto.evento_id as projeto_evento_id',
                'projeto.created_at as projeto_created_at',
                'modalidade.nome as modalidade_nome',
                'evento.nome as evento_nome',
                'porte.nome as porte_nome',
                'inscricao.id as inscricao_id',
                'inscricao.cpf as inscricao_cpf',
                'inscricao.nome as inscricao_nome',
                'inscricao.endereco as inscricao_endereco',
                'inscricao.email as inscricao_email',
                'inscricao.email_alternativo as inscricao_email_alternativo',
                'inscricao.telefone as inscricao_telefone',
                'inscricao.celular as inscricao_celular',
                'inscricao.cnpj as inscricao_cnpj',
                'inscricao.nome_fantasia as inscricao_nome_fantasia',
                'inscricao.razao_social as inscricao_razao_social',
                'inscricao.representante as inscricao_representante',
                'inscricao.representante_cargo as inscricao_representante_cargo',
                'inscricao.cep_empresa as inscricao_cep_empresa',
                'inscricao.endereco_empresa as inscricao_endereco_empresa',
                'inscricao.estado_empresa as inscricao_estado_empresa',
                'inscricao.municipio_empresa as inscricao_municipio_empresa',
                'inscricao.complemento_empresa as inscricao_complemento_empresa'
            );
            $query->join('modalidade','modalidade.id','=','projeto.modalidade_id');
            $query->join('evento','evento.id','=','projeto.evento_id');
            $query->join('inscricao','inscricao.id','=','projeto.inscricao_id');
            $query->join('porte','porte.id','=','inscricao.porte_id');
            $query->where('projeto.evento_id',$evento_id);
            $query->orderBy('porte.nome','projeto.nome');

            return $query->get();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function findProjeto($id)
    {
        try {
            $query = $this->model->query();
            $query->select('projeto.id as projeto_id',
                'projeto.inscricao_id as projeto_inscricao_id',
                'projeto.nome as projeto_nome',
                'projeto.modalidade_id as projeto_modalidade_id',
                'projeto.responsavel_nome as projeto_responsavel_nome',
                'projeto.responsavel_telefone as projeto_responsavel_telefone',
                'projeto.responsavel_email as projeto_responsavel_email',
                'projeto.arquivo as projeto_arquivo',
                'projeto.evento_id as projeto_evento_id',
                'projeto.created_at as projeto_created_at',
                'modalidade.nome as modalidade_nome',
                'evento.nome as evento_nome',
                'porte.nome as porte_nome',
                'inscricao.id as inscricao_id',
                'inscricao.cpf as inscricao_cpf',
                'inscricao.nome as inscricao_nome',
                'inscricao.endereco as inscricao_endereco',
                'inscricao.email as inscricao_email',
                'inscricao.email_alternativo as inscricao_email_alternativo',
                'inscricao.telefone as inscricao_telefone',
                'inscricao.celular as inscricao_celular',
                'inscricao.cnpj as inscricao_cnpj',
                'inscricao.nome_fantasia as inscricao_nome_fantasia',
                'inscricao.razao_social as inscricao_razao_social',
                'inscricao.representante as inscricao_representante',
                'inscricao.representante_cargo as inscricao_representante_cargo',
                'inscricao.cep_empresa as inscricao_cep_empresa',
                'inscricao.endereco_empresa as inscricao_endereco_empresa',
                'inscricao.estado_empresa as inscricao_estado_empresa',
                'inscricao.municipio_empresa as inscricao_municipio_empresa',
                'inscricao.complemento_empresa as inscricao_complemento_empresa'
            );
            $query->join('modalidade','modalidade.id','=','projeto.modalidade_id');
            $query->join('evento','evento.id','=','projeto.evento_id');
            $query->join('inscricao','inscricao.id','=','projeto.inscricao_id');
            $query->join('porte','porte.id','=','inscricao.porte_id');
            $query->where('projeto.id',$id);

            return $query->get()->first();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function findProjetoAdicionado($inscricao_id,$modalidade_id)
    {
        try {
            $query = $this->model->query();
            $query->where('inscricao_id',$inscricao_id);
            $query->where('modalidade_id',$modalidade_id);

            return $query->get()->first();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }
}