<?php

namespace App\Repositories;

use App\Models\Evento;
use Exception;
use DB;
use App\Models\EventoModalidade;

class EventoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(Evento $model)
	{
		$this->model = $model;
	}

	public function addEvento($data)
	{
		try {
			DB::beginTransaction();

    		if (array_key_exists("modalidade", $data)) {
	    		$input = array_except($data, ['modalidade']);
	    		$evento = new $this->model($input);
	    		$evento->token = md5($input['nome'].microtime());
    			$evento->save();

	    		foreach ($data['modalidade'] as $modalidade) {
	    			if ($modalidade != 0) {
	    				$evento_modalidade = new EventoModalidade();
    					$evento_modalidade->evento_id = $evento->id;
    					$evento_modalidade->modalidade_id = $modalidade;
    					$evento_modalidade->save();
    				}
	    		}
			} else {
				$evento = new $this->model($data);
				$evento->token = md5($data['nome'].microtime());
    			$evento->save();
			}

			DB::commit();

            return TRUE;
		} catch (Exception $e) {
			DB::rollBack();
			return $e->getMessage();
		}
	}

	public function editEvento($id, $data)
    {
    	
        try {
            DB::beginTransaction();
            
            if (array_key_exists("modalidade", $data)) {
            	$input = array_except($data, ['modalidade']);

            	if (count($data['modalidade']) > 1) {
		    		DB::table('evento_modalidade')->where('evento_id','=',$id)->delete();

		    		foreach ($data['modalidade'] as $modalidade) {
		    			if ($modalidade != 0) {
		    				$evento_modalidade = new EventoModalidade();
	    					$evento_modalidade->evento_id = $id;
	    					$evento_modalidade->modalidade_id = $modalidade;
	    					$evento_modalidade->save();
	    				}
		    		}
		    	}
            }

            $evento = $this->model->find($id);
	        $evento->fill($input);
	        $evento->save();

            

            DB::commit();

            return TRUE;
        } catch(Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

	public function findByToken($token)
	{
		try {
			$query = $this->model->query();
            $query->where('token',$token);

            return $query->get()->first();
		} catch (Exception $e) {
			return [];
		}
	}

	public function findLast()
	{
		try {
			$query = $this->model->query();
            $query->orderBy('id','DESC');
            $query->limit(1);

            return $query->get()->first();
		} catch (Exception $e) {
			return [];
		}
	}

	public function dropDown()
	{
		try {
			$query = $this->model->query();
			$query->select(
				DB::raw("CONCAT(id,' | ',nome,' | ',DATE_FORMAT(data_inicio,'%d/%m/%Y'),' à ',DATE_FORMAT(data_fim,'%d/%m/%Y')) AS evento"),
				'id'
			);
			$query->orderBy('nome');

			return $query->pluck('evento','id')->prepend('Escolha a opção','');
		} catch (Exception $e) {
			return [''=>$e->getMessage()];
		}
	}
}