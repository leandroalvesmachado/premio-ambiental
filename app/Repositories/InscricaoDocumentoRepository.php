<?php

namespace App\Repositories;

use App\Models\InscricaoDocumento;
use Exception;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class InscricaoDocumentoRepository extends BaseRepository 
{
	protected $model;

	public function __construct(InscricaoDocumento $model)
	{
		$this->model = $model;
	}

	public function addInscricaoDocumento($data)
	{
		try {
    		$arquivo = $data['arquivo'];
			$arquivo_extensao = $data['arquivo']->getClientOriginalExtension();
			$arquivo_nome = Str::slug($data['nome']).'.'.$arquivo_extensao;

			$object = new $this->model($data);
			$object->arquivo = $arquivo_nome;
            $object->inscricao_id = session()->get('usuario')['inscricao']['id'];
            $object->evento_id = session()->get('usuario')['evento']['id'];
			$object->save();

			Storage::disk('local')->put('documentos/'.$object->id.'/'.$arquivo_nome, file_get_contents($arquivo->getRealPath()));

    		return TRUE;
        } catch(Exception $e) {
			return $e->getMessage();
		}
	}

	public function deleteInscricaoDocumento($id)
	{
		try {
			$object = $this->model->find($id);
			Storage::delete('documentos/'.$object->id.'/'.$object->arquivo);
			$object->forceDelete();

			return TRUE;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

    public function findInscricaoDocumentoAdicionado($inscricao_id,$documento_id)
    {
        try {
            $query = $this->model->query();
            $query->where('inscricao_id',$inscricao_id);
            $query->where('documento_id',$documento_id);

            return $query->get()->first();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }

    public function findByInscricao($inscricao_id)
    {
        try {
            $query = $this->model->query();
			$query->where('inscricao_id',$inscricao_id);

            return $query->get();
        } catch (Exception $e) {
            return [''=>$e->getMessage()];
        }
    }
}