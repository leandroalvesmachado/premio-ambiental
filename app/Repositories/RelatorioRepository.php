<?php

namespace App\Repositories;

use Exception;
use DB;
use App\Models\Inscricao;
use App\Models\Projeto;
use App\Models\AvaliadorProjeto;

class RelatorioRepository 
{
	protected $inscricao_model;
	protected $projeto_model;
	protected $avaliador_projeto_model;

	public function __construct(Inscricao $inscricao, Projeto $projeto, AvaliadorProjeto $avaliador_projeto)
	{
		$this->inscricao_model = $inscricao;
		$this->projeto_model = $projeto;
		$this->avaliador_projeto_model = $avaliador_projeto;
	}

	/* Todas as inscrições realizadas no evento*/
	public function relatorioModelo1($data)
	{
		try {

			return $this->inscricao_model->with(['evento','porte','inscricao_modalidades','projetos'])
           	->select(
           		'inscricao.*',
           		'porte.nome as porte_nome'
           	)
			->join('porte','porte.id','=','inscricao.porte_id')
			->where('inscricao.evento_id',$data['evento_id'])
			->orderBy('porte.nome')
			->get();

			//return $query;

			// $query = Inscricao::with('evento');
			// $query->select(
   //              'evento.nome as evento_nome',
   //              'porte.nome as porte_nome',
   //              'inscricao.id as inscricao_id',
   //              'inscricao.cpf as inscricao_cpf',
   //              'inscricao.nome as inscricao_nome',
   //              'inscricao.endereco as inscricao_endereco',
   //              'inscricao.email as inscricao_email',
   //              'inscricao.email_alternativo as inscricao_email_alternativo',
   //              'inscricao.telefone as inscricao_telefone',
   //              'inscricao.celular as inscricao_celular',
   //              'inscricao.cnpj as inscricao_cnpj',
   //              'inscricao.nome_fantasia as inscricao_nome_fantasia',
   //              'inscricao.razao_social as inscricao_razao_social',
   //              'inscricao.representante as inscricao_representante',
   //              'inscricao.representante_cargo as inscricao_representante_cargo',
   //              'inscricao.cep_empresa as inscricao_cep_empresa',
   //              'inscricao.endereco_empresa as inscricao_endereco_empresa',
   //              'inscricao.estado_empresa as inscricao_estado_empresa',
   //              'inscricao.municipio_empresa as inscricao_municipio_empresa',
   //              'inscricao.bairro_empresa as inscricao_bairro_empresa',
   //              'inscricao.complemento_empresa as inscricao_complemento_empresa'
   //          );
			// $query->join('evento','evento.id','=','inscricao.evento_id');
			// $query->join('porte','porte.id','=','inscricao.porte_id');
			// //$query->join('inscricao_modalidade','inscricao_modalidade.inscricao_id','=','inscricao.id');
			// $query->where('inscricao.evento_id',$data['evento_id']);
			// $query->orderBy('porte.nome');

			// return $query->get();
		} catch (Exception $e) {
			return FALSE;
		}
	}

	public function relatorioModelo2($data)
	{
		try {
			return [];
		} catch (Exception $e) {
			echo $e->getMessage();
			exit();
			return [];
		}
	}

	public function relatorioModelo3($data)
	{
		try {
			$query = $this->projeto_model->query();
			$query->select(
				'projeto.id as projeto_id',
				'projeto.inscricao_id as projeto_inscricao_id',
				'projeto.nome as projeto_nome',
				'projeto.modalidade_id as projeto_modalidade_id',
				'projeto.responsavel_nome as projeto_responsavel_nome',
				'projeto.responsavel_telefone as projeto_responsavel_telefone',
				'projeto.responsavel_email as projeto_responsavel_email',
				'modalidade.nome as modalidade_nome',
				'evento.nome as evento_nome',
				'evento.data_inicio as evento_data_inicio',
				'evento.data_fim as evento_data_fim',
				'avaliador.nome as avaliador_nome',
				'avaliador_projeto.nota as avaliador_projeto_nota',
				'avaliador_projeto.comentario as avaliador_projeto_comentario',
				'inscricao.id as inscricao_id',
				'inscricao.nome as inscricao_nome',
				'inscricao.nome_fantasia as inscricao_nome_fantasia',
				'porte.nome as porte_nome'

			);
			$query->join('modalidade','modalidade.id','=','projeto.modalidade_id');
			$query->join('evento','evento.id','=','projeto.evento_id');
			$query->join('avaliador_projeto','avaliador_projeto.projeto_id','=','projeto.id');
			$query->join('avaliador','avaliador.id','=','avaliador_projeto.avaliador_id');
			$query->join('inscricao','inscricao.id','=','projeto.inscricao_id');
			$query->join('porte','porte.id','=','inscricao.porte_id');
			$query->where('inscricao.evento_id',$data['evento_id']);
			
			// return $this->avaliador_projeto_model->with(['projeto','avaliador'])
   //         	->select(
   //         		'projeto.nome as projeto_nome'
   //         	)
			// ->join('porte','porte.id','=','inscricao.porte_id')
			// ->where('inscricao.evento_id',$data['evento_id'])
			$query->orderBy('modalidade.nome','ASC');
			$query->orderBy('porte.nome','ASC'); 
			$query->orderBy('projeto.nome','ASC');
			$query->orderBy('inscricao.nome_fantasia','ASC');
			$query->orderBy('avaliador.nome','ASC'); 
			//avaliador.nome','ASC');
			// ->get();

			// $r = $query->toSql();
			// echo $r;
			$resultados = $query->get();

			$array = [];
			if (count($resultados) > 0) {
				foreach ($resultados as $resultado) {
					// echo"<pre>";
					// print_r($resultado);
					$array[$resultado->modalidade_nome][$resultado->porte_nome][$resultado->projeto_nome][] = $resultado;
				}
			}

			// echo"<pre>";
			// print_r($array);

			// exit();
			return $array;

			return $query->get();
		} catch (Exception $e) {
			echo $e->getMessage();
			exit();
			return [];
		}
	}
}